import torch
import torch.nn as nn
import torch.nn.functional as f
from coverage_environment.coverage_environment.models import (
    LGTCRaExtractor,
    LGTCResoursesAllocation,
    LGTCRaNetwork,
)
import math


class GNNRegularization(object):
    def __init__(self, model, weight_list=[], regularization=[], C=1.0):

        assert isinstance(model, LGTCRaNetwork)

        self.model = model
        self.weights = weight_list
        self.regularization = regularization
        self.C = C

        self.GNNlayers = []

        self.hard_tanh = nn.Hardtanh(max_val=10)
        self.softplus = nn.Softplus(beta=10)
        self.L = torch.eye(2).to(device=self.model.device)
        self.data = {"L_norm": 2}

    @staticmethod
    def smooth_norm_inf(x, matrix=True):

        if matrix:
            tensor = torch.abs(x)
            tensor = torch.sum(tensor, dim=len(tensor.shape) - 1)
        else:
            tensor = torch.abs(x)

        c = torch.max(tensor)

        if c > 1e-3:
            k = 81 / (c.detach())

            tensor = torch.log(
                torch.sum(torch.exp(k * tensor), dim=len(tensor.shape) - 1)
            ) / (k + 1e-12)
        else:
            tensor = c

        return tensor

    @staticmethod
    def smooth_norm_inf_bs(x, sequenced=True, batched=True, matrix=True):
        if sequenced & len(x.shape) > 3:
            raise RuntimeError(f"Tensor dimension {x.shape} but not sequenced")
        result = []

        if sequenced:
            for xx in x:
                result.append(GNNRegularization.smooth_norm_inf(xx, matrix))

            result = torch.cat(result, dim=0)
            return GNNRegularization.smooth_norm_inf(result, matrix=False)

        else:
            if batched:
                for xx in x:
                    result.append(GNNRegularization.smooth_norm_inf(xx, matrix))
                result = torch.Tensor(result)
            else:
                result = GNNRegularization.smooth_norm_inf(x, matrix)

            return GNNRegularization.smooth_norm_inf(result, matrix=False)

    def smooth_lognorm_inf(self, x, matrix=True):

        if matrix:
            diagonal = torch.diagonal(x, 0)
            x_w_d = x - torch.diag(diagonal)
            x_w_d = torch.abs(x_w_d)
            tensor = torch.sum(torch.diag(diagonal) + x_w_d, dim=len(x.shape) - 1)
        else:
            tensor = x

        c = torch.max(tensor)
        if c > 1e-3:
            k = 81 / (c.detach())

            tensor = torch.log(
                torch.sum(torch.exp(k * tensor), dim=len(tensor.shape) - 1)
            ) / (k + 1e-12)
        else:
            tensor = c

        return tensor

    def update_L(self, L: torch.Tensor) -> None:
        self.L = L

    def module_LGTC(self, lgtc_layer):
        lgtc_layer: LGTCResoursesAllocation.LGTCRAimp = lgtc_layer.gnn

        H = lgtc_layer.H
        module_l = 0  # norm Laplacian

        module_gss = []  # module state-state gate
        module_s = []  # module state transiction

        for j in range(H):
            # compute norm for the gates
            if j < H - 1:
                module_gss.append(lgtc_layer.weight_Ax[:, :, j].mT)

            # compute norm state-state transiction
            module_s.append(lgtc_layer.weight_A[:, :, j])

            # compute laplacian norm
            if j == 0:
                module_l = torch.Tensor([1]).to(device=self.model.device)
            else:
                L = torch.pow(self.L, j)
                module_l = module_l + torch.linalg.matrix_norm(L, ord=float("inf"))

        module_l = torch.max(module_l)
        if self.data["L_norm"] < module_l:
            self.data["L_norm"] = module_l
        module_l = self.data["L_norm"]

        module_s = torch.cat(module_s, dim=0)
        module_gs_sum = torch.stack(module_gss, dim=0)
        module_gss = torch.cat(module_gss, dim=0)

        module_gs_sum = torch.sum(module_gs_sum, dim=0)
        module_gs_sum = self.smooth_lognorm_inf(module_gs_sum, matrix=True)
        module_s = self.smooth_norm_inf_bs(
            module_s, sequenced=False, batched=False, matrix=True
        )
        module_gss = self.smooth_norm_inf_bs(
            module_gss, sequenced=False, batched=False, matrix=True
        )

        b = lgtc_layer.bias_f
        bx = lgtc_layer.bias_fx
        module_b = self.smooth_norm_inf_bs(
            b, sequenced=False, batched=False, matrix=False
        )
        module_b_h = self.smooth_norm_inf_bs(
            bx, sequenced=False, batched=False, matrix=False
        )

        b = self.smooth_lognorm_inf(b, matrix=False)
        c = -module_b - module_b_h - module_s + module_gss * module_l
        loss = self.softplus(c)

        return loss

    def cLGTC(self):
        """incremental ISS for LGTC

        Returns:
            torch.Tensor: regularization loss
        """
        loss = 0.0
        lgtc = []
        for a in dir(self.model):
            lgtc_layer = getattr(self.model, a)
            if isinstance(lgtc_layer, LGTCResoursesAllocation):
                lgtc.append(lgtc_layer)

        for i in range(len(lgtc)):
            lgtc_layer = lgtc[i]

            module_Ad = self.module_LGTC(lgtc_layer)

            loss = loss + module_Ad

        return loss

    def __call__(self):
        assert len(self.regularization) == len(self.weights)

        loss = torch.Tensor([0.0]).to(self.model.device)
        for i, reg in enumerate(self.regularization):
            loss = loss + self.weights[i] * reg()

        return loss
