from coverage_environment.coverage_environment.multi_resources_ppo.multi_resources_ppo import (
    MultiResourcesMAPPO,
    MultiResourcesIPPO,
)


__all__ = ["MultiResourcesMAPPO", "MultiResourcesIPPO"]
