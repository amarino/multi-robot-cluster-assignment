from typing import Dict, List, Optional, Tuple, Type, Union

import gymnasium as gym
import torch as th
from gymnasium import spaces
from torch import nn

from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.preprocessing import get_flattened_obs_dim, is_image_space
from stable_baselines3.common.type_aliases import TensorDict
from stable_baselines3.common.utils import get_device
from multi_agent_stable_baselines3.GNNlib import *
from multi_agent_stable_baselines3.common.torch_layers import *
import torch_geometric
from torch_geometric.utils import to_dense_adj

import torch.nn.functional as Functional
from copy import deepcopy


def create_multi_agent_mlp_network(
    layers_dims: list,
    last_layer_dim: int,
    agents: list,
    activation_fn: Type[nn.Module],
    shared_parameters: bool,
    add_final_linear: bool = False,
):
    if shared_parameters:
        net: List[nn.Module] = []
        # Iterate through the policy layers and build the policy net
        for curr_layer_dim in layers_dims:
            net.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net.append(activation_fn())
            last_layer_dim = curr_layer_dim

        if add_final_linear:
            net.append(nn.Linear(last_layer_dim, 1))
            last_layer_dim = 1
        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        return (
            nn.Sequential(*net),
            last_layer_dim,
        )
    else:
        net: Dict[str, nn.Sequential] = {agent: [] for agent in agents}
        first_layer_dim = last_layer_dim
        for agent in agents:
            last_layer_dim = first_layer_dim

            # Iterate through the policy layers and build the policy net
            for curr_layer_dim in layers_dims:
                net[agent].append(nn.Linear(last_layer_dim, curr_layer_dim))
                net[agent].append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net[agent].append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

        # aggregate in the dict all the networks
        return (
            nn.ModuleDict({agent: nn.Sequential(*net[agent]) for agent in agents}),
            last_layer_dim,
        )


def create_graph_nn(
    feature_dims_pre: list,
    feature_dims_post: list,
    last_layer_dim: int,
    activation_fn: Type[nn.Module],
    gnn_feature_dim: int = 128,
    add_final_linear: bool = False,
):
    net_pre: List[nn.Module] = []
    for curr_layer_dim in feature_dims_pre:
        net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
        net_pre.append(activation_fn())
        last_layer_dim = curr_layer_dim

    gnn1 = GraphLayer(
        2,
        last_layer_dim,
        gnn_feature_dim,
        bias=True,
        direct_input=True,
        attention=False,
        autoencoder=False,
    )
    gnn2 = GraphLayer(
        2,
        gnn_feature_dim,
        gnn_feature_dim,
        bias=True,
        direct_input=True,
        attention=False,
        autoencoder=False,
    )
    last_layer_dim = gnn_feature_dim

    net_post: List[nn.Module] = []

    for curr_layer_dim in feature_dims_post:
        net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
        net_post.append(activation_fn())
        last_layer_dim = curr_layer_dim

    if add_final_linear:
        net_post.append(nn.Linear(last_layer_dim, 1))
        last_layer_dim = 1
    # Create policy network
    # If the list of layers is empty, the network will just act as an Identity module
    net_pre = nn.Sequential(*net_pre)
    net_post = nn.Sequential(*net_post)

    return net_pre, gnn1, net_post, last_layer_dim


class MultiAgentRaNetwork(nn.Module):

    type = ["mlp"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        net_arch: Union[List[int], Dict[str, List[int]]],
        activation_fn: Type[nn.Module],
        shared_parameters: bool = False,
        device: Union[th.device, str] = "auto",
        destinations: int = 4,
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameters = shared_parameters

        self.pre_net = nn.Sequential(nn.Linear(4, 128), nn.ReLU(), nn.Linear(128, 1))
        self.dest_size = destinations

        self.net, last_layer_dim = create_multi_agent_mlp_network(
            net_arch,
            feature_dim - 4 * (destinations - 1),
            agents,
            activation_fn,
            shared_parameters,
            add_final_linear,
        )
        self.net = self.net.to(device)

        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        centralized: bool = False,
    ) -> Union[TensorDict, th.Tensor]:
        if centralized:
            fxx = th.cat(list(features.values()), dim=1)
            fx = {key: fxx for key in self.agents}
        else:
            fx = features

        fxx = th.stack(list(fx.values()), dim=-2)
        it = fxx[:, :, : -(self.dest_size * 4)]
        itx = th.stack(th.split(fxx[:, :, -(self.dest_size * 4) :], 4, dim=-1), dim=-2)
        itx_A = th.softmax(self.pre_net(itx), dim=-2)
        A_value = torch.max(itx_A, dim=-2)[0]
        mask = th.ones_like(itx_A)
        mask[itx_A < A_value.unsqueeze(-2)] = 0
        A = itx_A * mask
        itxA = th.sum(A * itx, dim=-2)
        fx = self.net(th.cat([it, itxA], dim=-1))
        fx = {agent: fx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx


class MultiAgentRaExtractor(nn.Module):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["mlp"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        net_arch: Union[List[int], Dict[str, List[int]]],
        activation_fn: Type[nn.Module],
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        destinations: int = 4,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameter_actor = shared_parameter_actor
        self.shared_parameter_critic = shared_parameter_critic
        self.centralized_critics = centralized_critics

        last_layer_dim_pi = feature_dim
        if self.centralized_critics:
            last_layer_dim_vf = len(agents) * feature_dim
        else:
            last_layer_dim_vf = feature_dim

        # save dimensions of layers in policy and value nets
        if isinstance(net_arch, dict):
            # Note: if key is not specified, assume linear network
            pi_layers_dims = net_arch.get("pi", [])  # Layer sizes of the policy network
            vf_layers_dims = net_arch.get("vf", [])  # Layer sizes of the value network
        else:
            pi_layers_dims = vf_layers_dims = net_arch

        # create policy network
        self.policy_net = MultiAgentRaNetwork(
            feature_dim=last_layer_dim_pi,
            agents=agents,
            net_arch=pi_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_actor,
            destinations=destinations,
            device=device,
        )

        # create policy network
        self.value_net = MultiAgentRaNetwork(
            feature_dim=last_layer_dim_vf,
            agents=agents,
            net_arch=vf_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_critic,
            destinations=destinations,
            device=device,
            centralized=centralized_critics,
        )

        # Save dim, used to create the distributions
        self.latent_dim_pi = self.policy_net.last_layer_dim
        self.latent_dim_vf = self.value_net.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy_net.agents = agents
        self.value_net.agents = agents

    def forward(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(features), self.forward_critic(features)

    def forward_actor(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        return self.policy_net(features)

    def forward_critic(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        return self.value_net(features, self.centralized_critics)


class MLPMORaNetwork(nn.Module):

    type = ["mlp", "multi-objective"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        shared_parameters: bool = False,
        device: Union[th.device, str] = "auto",
        destinations: int = 4,
        resources: int = 2,
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super().__init__()
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.agents = agents

        pre_dims = [64]
        post_dims = [256, 256]
        self.agent_features = 2
        self.resources_features = 2
        self.resources = resources

        self.centralized = centralized

        self.dest_size = destinations

        last_layer_dim = self.agent_features + self.resources_features
        last_layer_dim_dist = self.agent_features + self.resources_features

        net_pre_dist: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist.append(nn.Linear(last_layer_dim_dist, curr_layer_dim))
            net_pre_dist.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre.append(activation_fn())
            last_layer_dim = curr_layer_dim

        net_pre_dist_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist_resources.append(
                nn.Linear(last_layer_dim_dist, curr_layer_dim)
            )
            net_pre_dist_resources.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_resources.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre_resources.append(activation_fn())
            last_layer_dim = curr_layer_dim

        last_layer_dim = 2 * 64

        self.attention = nn.Sequential(
            nn.Linear(last_layer_dim, 128), nn.ReLU(), nn.Linear(128, 1)
        )

        net_post: List[nn.Module] = []

        if self.centralized:
            for curr_layer_dim in post_dims:
                net_post.append(
                    nn.Linear(last_layer_dim * len(self.agents), curr_layer_dim)
                )
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        else:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        self.net_pre = nn.Sequential(*net_pre)
        self.net_pre_dist = nn.Sequential(*net_pre_dist)
        self.net_pre_resources = nn.Sequential(*net_pre_resources)
        self.net_pre_dist_resources = nn.Sequential(*net_pre_dist_resources)
        self.net_post = nn.Sequential(*net_post)

        self.net_pre = self.net_pre.to(self.device)
        self.net_pre_dist = self.net_pre_dist.to(self.device)
        self.net_pre_resources = self.net_pre_resources.to(self.device)
        self.net_pre_dist_resources = self.net_pre_dist_resources.to(self.device)
        self.net_post = self.net_post.to(self.device)

        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        centralized: bool = False,
    ) -> Union[TensorDict, th.Tensor]:
        fxx = th.stack(list(features.values()), dim=-2)
        batch_size = fxx.shape[0]

        feature_slice = self.agent_features + self.resources * self.resources_features

        it = fxx[:, :, : -(self.dest_size * feature_slice)]
        it_f_split = th.split(
            it[:, :, self.agent_features :], self.resources_features, dim=-1
        )
        it_f_split = [
            th.cat([it[:, :, : self.agent_features], it_f], dim=-1)
            for it_f in it_f_split
        ]

        itx = th.split(
            fxx[:, :, -(self.dest_size * feature_slice) :],
            feature_slice,
            dim=-1,
        )
        itx_dist = th.stack(itx, dim=-2)
        itx_dist_c = th.split(
            itx_dist[:, :, :, self.agent_features :], self.resources_features, dim=-1
        )
        itx_dist_c = [
            th.cat([itx_dist[:, :, :, : self.agent_features], tdc], dim=-1)
            for tdc in itx_dist_c
        ]

        itx_dist_c = sum(self.net_pre_dist(idc) for idc in itx_dist_c) / len(itx_dist_c)
        itx_dist_c = self.net_pre_dist_resources(itx_dist_c)
        itx_f_split = sum(self.net_pre(it_f) for it_f in it_f_split) / len(it_f_split)
        itx_f_split = self.net_pre_resources(itx_f_split)

        itx_f_split = itx_f_split.unsqueeze(-2)
        itx_f_split = itx_f_split.repeat(1, 1, self.dest_size, 1)

        fxx = th.cat([itx_f_split, itx_dist_c], dim=-1)
        Ax = th.softmax(self.attention(fxx), dim=-2)
        fxx = th.sum(Ax * fxx, dim=-2)

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fxx = fxx.reshape(batch_size, len(self.agents), -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx


class GNNMORaExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["mlp", "multi-objective", "gnn"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        net_arch: Union[List[int], Dict[str, List[int]]],
        activation_fn: Type[nn.Module],
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        destinations: int = 4,
        resources: int = 2,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = GNNRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            resources=resources,
        )
        self.value = MLPMORaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            resources=resources,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(dict(features)), self.forward_critic(dict(features))

    def forward_actor(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        fx = self.policy(features)
        return fx

    def forward_critic(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        del features[self.edge_variable]
        fx = self.value(features, centralized=self.centralized_critics)
        return fx


class GNNRaNetwork(nn.Module):

    type = ["gnn"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        add_final_linear: bool = False,
        destinations: int = 4,
        resources: int = 2,
        centralized: bool = False,
    ) -> None:
        super(GNNRaNetwork, self).__init__()

        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents

        pre_dims = [64]
        post_dims = [256, 256]
        self.agent_features = 2
        self.resources_features = 2
        self.resources = resources

        self.dest_size = destinations
        self.attention_net = nn.Sequential(
            nn.Linear(128, 128),
            nn.ReLU(),
            nn.Linear(128, 1),
        )

        gnn_feature_dim = 64
        last_layer_dim = self.agent_features + self.resources_features
        last_layer_dim_dist = self.agent_features + self.resources_features

        net_pre_dist: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist.append(nn.Linear(last_layer_dim_dist, curr_layer_dim))
            net_pre_dist.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre.append(activation_fn())
            last_layer_dim = curr_layer_dim

        net_pre_dist_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist_resources.append(
                nn.Linear(last_layer_dim_dist, curr_layer_dim)
            )
            net_pre_dist_resources.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_resources.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre_resources.append(activation_fn())
            last_layer_dim = curr_layer_dim

        self.gnn = GraphLayer(
            2,
            last_layer_dim,
            gnn_feature_dim,
            bias=True,
            direct_input=True,
            attention=False,
            autoencoder=False,
        )

        last_layer_dim = 2 * 64 + self.agent_features

        net_post: List[nn.Module] = []

        if centralized:
            for curr_layer_dim in post_dims:
                net_post.append(
                    nn.Linear(last_layer_dim * len(self.agents), curr_layer_dim)
                )
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        else:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        self.net_pre = nn.Sequential(*net_pre)
        self.net_pre_dist = nn.Sequential(*net_pre_dist)
        self.net_pre_resources = nn.Sequential(*net_pre_resources)
        self.net_pre_dist_resources = nn.Sequential(*net_pre_dist_resources)
        self.net_post = nn.Sequential(*net_post)

        self.net_pre = self.net_pre.to(self.device)
        self.net_pre_dist = self.net_pre_dist.to(self.device)
        self.net_pre_resources = self.net_pre_resources.to(self.device)
        self.net_pre_dist_resources = self.net_pre_dist_resources.to(self.device)
        self.net_post = self.net_post.to(self.device)
        self.gnn = self.gnn.to(self.device)

        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def make_graph(
        self,
        pos: th.Tensor,
    ):
        batch_size = pos.shape[0]
        b = torch.arange(batch_size, device=self.device)
        pos = pos.reshape(-1, pos.shape[-1])
        graphs = torch_geometric.data.Batch()
        graphs.ptr = torch.arange(
            0, (batch_size + 1) * len(self.agents), len(self.agents)
        )
        graphs.batch = torch.repeat_interleave(b, len(self.agents))
        graphs.pos = pos
        graphs.vel = None
        graphs.edge_attr = None

        graphs.edge_index = torch_geometric.nn.pool.radius_graph(
            graphs.pos, batch=graphs.batch, r=self.edge_radius, loop=False
        )

        graphs = graphs.to(self.device)

        A = to_dense_adj(graphs.edge_index, graphs.batch)
        D = torch.sum(A, axis=-1)
        A = A / (D.unsqueeze(-1) + 1e-4)

        return A

    def forward(self, features: th.Tensor, centralized: bool = False) -> th.Tensor:

        pos = features.pop(self.edge_variable)

        # build Graph
        A = self.make_graph(pos)

        # dimensions batch x n_agents x feature_dim
        fxx = th.stack(list(features.values()), dim=-2)
        batch_size = fxx.shape[0]

        feature_slice = self.agent_features + self.resources * self.resources_features

        it = fxx[:, :, : -(self.dest_size * feature_slice)]
        it_f_split = th.split(
            it[:, :, self.agent_features :], self.resources_features, dim=-1
        )
        it_f_split = [
            th.cat([it[:, :, : self.agent_features], it_f], dim=-1)
            for it_f in it_f_split
        ]

        itx = th.split(
            fxx[:, :, -(self.dest_size * feature_slice) :],
            feature_slice,
            dim=-1,
        )
        itx_dist = th.stack(itx, dim=-2)
        itx_dist_c = th.split(
            itx_dist[:, :, :, self.agent_features :], self.resources_features, dim=-1
        )
        itx_dist_c = [
            th.cat([itx_dist[:, :, :, : self.agent_features], tdc], dim=-1)
            for tdc in itx_dist_c
        ]

        itx_dist_c = sum(self.net_pre_dist(idc) for idc in itx_dist_c) / len(itx_dist_c)
        itx_dist_c = self.net_pre_dist_resources(itx_dist_c)
        itx_f_split = sum(self.net_pre(it_f) for it_f in it_f_split) / len(it_f_split)
        itx_f_split = self.net_pre_resources(itx_f_split)

        fx = itx_f_split + th.mean(itx_dist_c, dim=-2)
        fx = torch.tanh(self.gnn(fx, A))
        fx = fx.unsqueeze(-2)
        fx = fx.repeat(1, 1, self.dest_size, 1)

        itx_A = self.attention_net(th.cat([itx_dist_c, fx], dim=-1))
        itx_A = th.softmax(itx_A, dim=-2)
        A_value = th.max(itx_A, dim=-2, keepdim=True)[0]  # - 1 / self.dest_size
        mask = th.ones_like(itx_A)
        mask[itx_A < A_value] = 0

        # mask = mask / (A_value.unsqueeze(-2) + 1e-5)
        itx_A = itx_A * mask
        # itx_A = itx_A / th.sum(itx_A, dim=-2, keepdim=True)
        it_fx = th.sum(itx_A * itx_dist_c, dim=-2)
        # fx = th.sum(Aidx * fx, dim=-2)

        fxx = th.cat([it_fx, it[:, :, : self.agent_features], fx[:, :, 0, :]], dim=-1)

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fxx = fxx.reshape(batch_size, len(self.agents), -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx


class GNNRaExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["gnn"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        destinations: int = 4,
        resources: int = 2,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = GNNRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            resources=resources,
        )
        self.value = GNNRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            resources=resources,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.value.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius
        self.value.edge_radius = self.edge_radius

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(deepcopy(features)), self.forward_critic(
            deepcopy(features)
        )

    def forward_actor(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        fx = self.policy(features)
        return fx

    def forward_critic(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        fx = self.value(features, centralized=self.centralized_critics)
        return fx


class GGNNRaNetwork(nn.Module):

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        add_final_linear: bool = False,
        destinations: int = 4,
        resources: int = 2,
        centralized: bool = False,
    ) -> None:
        super(GGNNRaNetwork, self).__init__()

        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents

        pre_dims = [64, 128]
        post_dims = [128, 64]
        gnn_feature_dim = 64
        self.agent_feature = 3

        self.dest_size = destinations
        self.attention_net = nn.Sequential(
            nn.Linear(
                self.agent_feature + feature_dim - self.agent_feature * (destinations),
                128,
            ),
            nn.ReLU(),
            nn.Linear(128, 1),
        )

        last_layer_dim = feature_dim - self.agent_feature * (destinations - 1)
        net_pre: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre.append(activation_fn())
            last_layer_dim = curr_layer_dim

        self.gnn = GGL(
            H=2,
            G=last_layer_dim,
            F=gnn_feature_dim,
            attention=False,
            autoencoder=False,
        )
        last_layer_dim = gnn_feature_dim

        net_post: List[nn.Module] = []

        if centralized:
            for curr_layer_dim in post_dims:
                net_post.append(
                    nn.Linear(last_layer_dim * len(self.agents), curr_layer_dim)
                )
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        else:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        self.net_pre = nn.Sequential(*net_pre)
        self.net_post = nn.Sequential(*net_post)

        self.net_pre = self.net_pre.to(self.device)
        self.net_post = self.net_post.to(self.device)
        self.gnn = self.gnn.to(self.device)

        self.recurrent_layers = tuple([(gnn_feature_dim,)])
        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def make_graph(
        self,
        pos: th.Tensor,
    ):
        batch_size = pos.shape[0]
        b = torch.arange(batch_size, device=self.device)
        pos = pos.reshape(-1, pos.shape[-1])
        graphs = torch_geometric.data.Batch()
        graphs.ptr = torch.arange(
            0, (batch_size + 1) * len(self.agents), len(self.agents)
        )
        graphs.batch = torch.repeat_interleave(b, len(self.agents))
        graphs.pos = pos
        graphs.vel = None
        graphs.edge_attr = None

        graphs.edge_index = torch_geometric.nn.pool.radius_graph(
            graphs.pos, batch=graphs.batch, r=self.edge_radius, loop=True
        )

        graphs = graphs.to(self.device)

        A = to_dense_adj(graphs.edge_index, graphs.batch)
        D = torch.sum(A, axis=-1)
        A = A / (D.unsqueeze(-1) + 1e-4)

        return A

    def forward(
        self,
        features: th.Tensor,
        state: Tuple = None,
        episode_start: th.Tensor = None,
        centralized: bool = False,
    ) -> th.Tensor:

        pos = features.pop(self.edge_variable)

        # reshape to stack pos sequences in batch
        pos = pos.reshape(-1, pos.shape[-2], pos.shape[-1])

        # build Graph
        A: th.Tensor = self.make_graph(pos)

        # dimensions batch x n_agents x feature_dim
        fxx: th.Tensor = th.stack(list(features.values()), dim=-2)
        N = fxx.size(2)
        batch_size = fxx.size(1)
        sequence_size = fxx.size(0)

        it = fxx[:, :, :, : -(self.dest_size * self.agent_feature)]
        itx = th.split(
            fxx[:, :, :, -(self.dest_size * self.agent_feature) :],
            self.agent_feature,
            dim=-1,
        )
        itx_dist = th.stack(itx, dim=-2)
        itx = th.stack([th.cat([i, it], dim=-1) for i in itx], dim=-2)
        itx_A = th.softmax(self.attention_net(itx), dim=-2)
        A_value = torch.max(itx_A, dim=-2)[0]
        mask = th.ones_like(itx_A)
        mask[itx_A < A_value.unsqueeze(-2)] = 0
        # mask = mask / (A_value.unsqueeze(-2) + 1e-5)
        Aidx = itx_A * mask
        it_fx = th.sum(Aidx * itx_dist, dim=-2)

        fxx = self.net_pre(th.cat([it, it_fx], dim=-1))

        hidden = []

        fxx = fxx.transpose(0, 1)
        A = A.reshape(sequence_size, batch_size, N, N)
        A = A.transpose(0, 1)
        if episode_start is None:
            episode_start = th.zeros(sequence_size, 1, 1, device=fxx.device)

        if len(episode_start.shape) < 3:
            episode_start = episode_start.unsqueeze(0)

        for j in range(len(self.recurrent_layers)):
            if not state:
                hd = torch.zeros((batch_size, N, self.gnn.F), device=fxx.device)
            else:
                hd = state[j]
            hs = fxx = self.gnn(fxx, A, hd, episode_start)
            hidden.append(hs.squeeze(0))

        fxx = fxx.squeeze(0)

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fxx = fxx.reshape(batch_size * sequence_size, N, -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx, tuple(hidden)


class GGNNRaExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
        resources: int = 2,
        destinations: int = 4,
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = GGNNRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            destinations=destinations,
            resources=resources,
        )
        self.value = GGNNRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            destinations=destinations,
            resources=resources,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.value.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius
        self.value.edge_radius = self.edge_radius

        # save dim of recurrent layers
        self.recurrent_pi_layers = self.policy.recurrent_layers
        self.recurrent_vf_layers = self.value.recurrent_layers

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return (
            self.forward_actor(
                dict(features),
                state[0],
                episode_start,
            ),
            self.forward_critic(
                dict(features),
                state[1],
                episode_start,
            ),
        )

    def forward_actor(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.policy(features, state, episode_start)

    def forward_critic(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.value(
            features,
            state=state,
            episode_start=episode_start,
            centralized=self.centralized_critics,
        )


class LGTCRaNetwork(nn.Module):

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        add_final_linear: bool = False,
        destinations: int = 4,
        resources: int = 2,
        centralized: bool = False,
    ) -> None:
        super(LGTCRaNetwork, self).__init__()

        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents

        pre_dims = [64]
        post_dims = [256, 256]
        gnn_feature_dim = 64
        self.agent_feature = 2
        self.resources_features = 2
        self.prediction_time = 0.05

        self.resources = resources
        self.dest_size = destinations
        self.attention_net = nn.Sequential(
            nn.Linear(128, 128),
            nn.ReLU(),
            nn.Linear(128, 1),
        )

        feature_slice = self.agent_feature + self.resources * self.resources_features

        last_layer_dim = self.agent_feature + self.resources_features
        last_layer_dim_dist = self.agent_feature + self.resources_features

        net_pre_dist: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist.append(nn.Linear(last_layer_dim_dist, curr_layer_dim))
            net_pre_dist.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre.append(activation_fn())
            last_layer_dim = curr_layer_dim

        net_pre_dist_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_dist_resources.append(
                nn.Linear(last_layer_dim_dist, curr_layer_dim)
            )
            net_pre_dist_resources.append(activation_fn())
            last_layer_dim_dist = curr_layer_dim

        net_pre_resources: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre_resources.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre_resources.append(activation_fn())
            last_layer_dim = curr_layer_dim

        self.gnn = GraphLayer(
            2,
            last_layer_dim,
            gnn_feature_dim,
            bias=True,
            direct_input=True,
            attention=False,
            autoencoder=False,
        )

        self.gnn_state = LGTCResoursesAllocation(
            H=2,
            agent_feature=self.agent_feature,
            destinations=destinations,
            G=64,
            F=gnn_feature_dim,
            attention=False,
            resources=resources,
            resources_features=self.resources_features,
            use_odeint=False,
        )

        last_layer_dim = 3 * 64 + self.agent_feature

        net_post: List[nn.Module] = []

        if centralized:
            for curr_layer_dim in post_dims:
                net_post.append(
                    nn.Linear(last_layer_dim * len(self.agents), curr_layer_dim)
                )
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        else:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        self.net_pre = nn.Sequential(*net_pre)
        self.net_pre_dist = nn.Sequential(*net_pre_dist)
        self.net_pre_resources = nn.Sequential(*net_pre_resources)
        self.net_pre_dist_resources = nn.Sequential(*net_pre_dist_resources)
        self.net_post = nn.Sequential(*net_post)

        self.net_pre = self.net_pre.to(self.device)
        self.net_pre_dist = self.net_pre_dist.to(self.device)
        self.net_pre_resources = self.net_pre_resources.to(self.device)
        self.net_pre_dist_resources = self.net_pre_dist_resources.to(self.device)
        self.net_post = self.net_post.to(self.device)
        self.gnn = self.gnn.to(self.device)
        self.gnn_state = self.gnn_state.to(self.device)

        self.recurrent_layers = tuple([(gnn_feature_dim,)])
        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def make_graph(
        self,
        pos: th.Tensor,
    ):
        batch_size = pos.shape[0]
        b = torch.arange(batch_size, device=self.device)
        pos = pos.reshape(-1, pos.shape[-1])
        graphs = torch_geometric.data.Batch()
        graphs.ptr = torch.arange(
            0, (batch_size + 1) * len(self.agents), len(self.agents)
        )
        graphs.batch = torch.repeat_interleave(b, len(self.agents))
        graphs.pos = pos
        graphs.vel = None
        graphs.edge_attr = None

        graphs.edge_index = torch_geometric.nn.pool.radius_graph(
            graphs.pos, batch=graphs.batch, r=self.edge_radius, loop=True
        )

        graphs = graphs.to(self.device)

        A = to_dense_adj(graphs.edge_index, graphs.batch)
        D = torch.sum(A, axis=-1)
        A = A / (D.unsqueeze(-1) + 1e-4)

        return A

    def forward(
        self,
        features: th.Tensor,
        state: Tuple = None,
        episode_start: th.Tensor = None,
        centralized: bool = False,
    ) -> th.Tensor:

        pos = features.pop(self.edge_variable)

        # build Graph
        pos = pos.reshape(-1, pos.shape[-2], pos.shape[-1])
        A = self.make_graph(pos)

        # dimensions batch x n_agents x feature_dim
        fxx = th.stack(list(features.values()), dim=-2)
        batch_size = fxx.shape[1]
        sequence_size = fxx.shape[0]
        N = fxx.size(2)

        A = A.reshape(sequence_size, batch_size, N, N)

        resources = fxx.shape[-1] / (self.dest_size + 1)
        resources = (resources - self.agent_feature) / self.resources_features
        resources = int(resources)

        feature_slice = self.agent_feature + resources * self.resources_features

        it = fxx[:, :, :, : -(self.dest_size * feature_slice)]
        it_f_split = th.split(
            it[:, :, :, self.agent_feature :], self.resources_features, dim=-1
        )
        it_f_split = [
            th.cat([it[:, :, :, : self.agent_feature], it_f], dim=-1)
            for it_f in it_f_split
        ]

        itx = th.split(
            fxx[:, :, :, -(self.dest_size * feature_slice) :],
            feature_slice,
            dim=-1,
        )
        itx_dist = th.stack(itx, dim=-2)
        itx_dist_c = th.split(
            itx_dist[:, :, :, :, self.agent_feature :], self.resources_features, dim=-1
        )
        itx_dist_c = [
            th.cat([itx_dist[:, :, :, :, : self.agent_feature], tdc], dim=-1)
            for tdc in itx_dist_c
        ]

        itx_dist_c = sum(self.net_pre_dist(idc) for idc in itx_dist_c) / len(itx_dist_c)
        itx_dist_c = self.net_pre_dist_resources(itx_dist_c)
        itx_f_split = sum(self.net_pre(it_f) for it_f in it_f_split) / len(it_f_split)
        itx_f_split = self.net_pre_resources(itx_f_split)

        fx = itx_f_split + th.mean(itx_dist_c, dim=-2)
        fx = [torch.tanh(self.gnn(fx[i], A[i])) for i in range(sequence_size)]
        fx = th.stack(fx, dim=0)
        fx = fx.unsqueeze(-2)
        fx = fx.repeat(1, 1, 1, self.dest_size, 1)

        itx_A = self.attention_net(th.cat([itx_dist_c, fx], dim=-1))
        itx_A = th.softmax(itx_A, dim=-2)
        A_value = th.max(itx_A, dim=-2, keepdim=True)[0]  # - 1 / self.dest_size
        mask = th.ones_like(itx_A)
        mask[itx_A < A_value] = 0

        # mask = mask / (A_value.unsqueeze(-2) + 1e-5)
        itx_A = itx_A * mask
        # itx_A = itx_A / th.sum(itx_A, dim=-2, keepdim=True)
        it_fx = th.sum(itx_A * itx_dist_c, dim=-2)
        # fx = th.sum(Aidx * fx, dim=-2)

        itx_A = itx_A / th.sum(itx_A, dim=-2, keepdim=True)
        it_fxn = th.sum(itx_A * itx_dist_c, dim=-2)

        self.gnn_state.f = it_fxn
        hidden = []

        if episode_start is None:
            episode_start = th.zeros(sequence_size, 1, 1, device=fxx.device)

        if len(episode_start.shape) < 3:
            episode_start = episode_start.unsqueeze(0)

        for j in range(len(self.recurrent_layers)):
            if not state:
                hd = torch.zeros((batch_size, N, self.gnn.F), device=fxx.device)
            else:
                hd = state[j]
            hs = fx_hs = self.gnn_state(self.prediction_time, fxx, A, hd, episode_start)
            hidden.append(hs.squeeze(0))

        fxx = th.cat(
            [it_fx, fx_hs, it[:, :, :, : self.agent_feature], fx[:, :, :, 0, :]], dim=-1
        )
        # fxx = self.layer_norm(fxx)

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fxx = fxx.transpose(1, 0).reshape(batch_size * sequence_size, N, -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx, tuple(hidden)


class LGTCRaExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
        destinations: int = 4,
        resources: int = 2,
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = LGTCRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            destinations=destinations,
            resources=resources,
        )
        self.value = LGTCRaNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            destinations=destinations,
            resources=resources,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.value.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius
        self.value.edge_radius = self.edge_radius

        # save dim of recurrent layers
        self.recurrent_pi_layers = self.policy.recurrent_layers
        self.recurrent_vf_layers = self.value.recurrent_layers

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return (
            self.forward_actor(
                dict(features),
                state[0],
                episode_start,
            ),
            self.forward_critic(
                dict(features),
                state[1],
                episode_start,
            ),
        )

    def forward_actor(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.policy(features, state, episode_start)

    def forward_critic(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.value(
            features,
            state=state,
            episode_start=episode_start,
            centralized=self.centralized_critics,
        )


class LGTCResoursesAllocation(nn.Module):
    """
    LGTC Layer
    To use the LGTC layer, there must be an implementation counterpart
    passing only t (time) and x (state) to the forward method to apply
    odeint to the state x
    """

    class LGTCRAimp(nn.Module):
        def __init__(
            self,
            H: int,
            G: int,
            F: int,
            agent_feature: int,
            destinations: int,
            resources: int,
            resources_features: int,
            attention: bool,
            use_odeint: bool,
        ) -> None:
            super(LGTCResoursesAllocation.LGTCRAimp, self).__init__()

            self.softplus = nn.Softplus(beta=1e4)

            self.H = H
            self.F = F
            self.G = G
            self.attention = attention
            self.use_odeint = use_odeint
            self.agent_feature = agent_feature
            self.resources = resources
            self.resources_features = resources_features
            self.dest_size = destinations

            self.I_com_x = 0  # torch.randint(4, 8, (1,))
            self.I_com_u = 0  # torch.randint(4, 8, (1,))
            self.weight_A = nn.parameter.Parameter(Tensor(F, F, H))
            self.weight_Ax = nn.parameter.Parameter(Tensor(F, F, H - 1))

            self.bias_f = nn.parameter.Parameter(torch.rand(F))

            self.bias_fx = nn.parameter.Parameter(torch.rand(F))
            self.bias_fu = nn.parameter.Parameter(torch.rand(F))

            self.bias_fB = nn.parameter.Parameter(2 * torch.rand(F) - 1)

            self.transition_net = nn.Sequential(nn.Linear(G, F), self.softplus)

            self.f = None
            self.leaky_relu = LeakyReLU()
            self.softmax = Softmax(dim=2)
            self.sigmoid = nn.Sigmoid()

            # init parameters
            self.initialize_parameters()

        def initialize_parameters(self) -> None:
            """initialize_parameters

            Init layers weights and biases
            """

            for name, param in self.named_parameters():
                if param.requires_grad:
                    if len(param.shape) != 1:
                        torch.nn.init.orthogonal_(param, gain=0.1)

            for layer in self.modules():
                if isinstance(layer, nn.Linear):
                    init.xavier_normal_(layer.weight.data)
                    if layer.bias is not None:
                        init.normal_(layer.bias.data)

        def generate_weight(self, F, G):
            I = []
            weight = []
            for _ in range(F):
                # bin = Binomial(G-1, torch.rand(1)*torch.ones(G-1))
                # I = I + [torch.unique(bin.sample(),dim=0).int()]
                I = I + [torch.arange(0, G)]
                w = torch.nn.parameter.Parameter(Tensor(I[-1].size()))
                torch.nn.init.constant_(w, val=0.1)
                weight = weight + [w]
            return (I, torch.nn.ParameterList(weight))

        def graph_filter_step(
            self,
            hidden: Tuple[Tensor, Tensor],
            L: Tensor,
            i: int,
            gate: Tuple[Tensor, Tensor],
        ) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
            """This method computes the communicated data and build up the filters components i
                of length H. The components are summed directly to the previous components 0...i-1
                the output of the graph filter is of dimension  [batch,F,Nr]

            Args:
                x Tuple(Tensor, Tensor) : input data, hidden state ([batch, Nr, G],[batch, Nr ,F])
                L (torch.Tensor): Laplacian, [batch, Nr , Nr]
                A (torch.Tensor): Attention resource matrix, [batch, Nr , Nr]
                i (int): filter tap
                gate Tuple(Tensor,Tensor)): computed gates in transition dynamics

            Returns:
                Tuple(torch.Tensor, torch.Tensor) : data, hidden after communication step; i=0 they are equal to the input
            """

            fx, fsx = gate

            if i > 0:

                hidden = torch.matmul(L, hidden)

                WAX = th.matmul(
                    self.weight_Ax[:, :, i - 1], self.weight_Ax[:, :, i - 1].mT
                )
                fsx = fsx + torch.matmul(hidden, WAX)

            fx = fx + torch.matmul(hidden, self.weight_A[:, :, i].mT)

            if i == 0:
                return hidden, (fx, fsx), L
            else:
                return hidden, (fx, fsx), L

        def output_cell(self, hidden: Tensor, f: Tensor) -> Tensor:
            fx, fsx = self.gate

            bias_f = torch.relu(self.bias_f)
            fx = self.softplus(fx + self.bias_fx)
            f = self.transition_net(f)
            if self.use_odeint:
                out = (
                    -bias_f * hidden
                    - (f + fx) * hidden
                    - fsx
                    + (f + fx) * th.tanh(self.bias_fB)
                )
            else:
                numerator = -fsx + (f + fx) * th.tanh(self.bias_fB)
                denominator = bias_f + (f + fx)

                out = (numerator, denominator)

            return out

        def cell(self, x: Tensor, L: Tensor, hidden: Optional[Tensor]) -> Tensor:
            """
            if hidden is None, it must be initialized with zeros before calling the forward method
            """

            self.gate = (0.0, 0.0)

            state = hidden

            for i in range(self.H):
                state, self.gate, L = self.graph_filter_step(state, L, i, self.gate)

            # self.f = self.attention_mask(self.gate[0], self.fu, self.process_input_2)
            out = self.output_cell(hidden, self.f)

            return out

        def reset_parameters(self, device):
            with torch.no_grad():
                self.weight_A = torch.zeros_like(self.weight_A).to(device)
                self.weight_B = torch.zeros_like(self.weight_B).to(device)

            for i in range(self.H):
                for j in range(self.F):
                    self.weight_A[j : j + 1, self.I_x[i][j], i] = getattr(
                        self, f"weight_x_{i}"
                    )[j]
                    self.weight_B[j : j + 1, self.I_u[i][j], i] = getattr(
                        self, f"weight_u_{i}"
                    )[j]

        def forward(self, x, L, hidden) -> Tuple[Tensor, Tensor]:
            state = hidden.detach() if hidden is not None else None
            return self.cell(x, L, state)

    def __init__(
        self,
        H: int,
        G: int,
        F: int,
        agent_feature: int,
        destinations: int,
        resources: int,
        resources_features: int,
        attention: bool,
        use_odeint: bool,
    ) -> None:
        super().__init__()
        self.gnn = self.LGTCRAimp(
            H,
            G,
            F,
            agent_feature,
            destinations,
            resources,
            resources_features,
            attention,
            use_odeint,
        )
        self.L = None
        self.data = None
        self.delta_time = 6
        self.odeint = use_odeint
        self.f = None

    def reset_parameters(self, device):
        self.gnn.reset_parameters(device)

    def set_L_data(self, L: Tensor, data: Tensor) -> None:
        self.L = L
        self.data = data

    def forward(
        self,
        t: Tensor,
        data: Tensor,
        L: Tensor,
        x: Tensor,
        episode_start: Tensor,
    ) -> Tensor:

        if len(data.shape) < 4:
            data = torch.unsqueeze(data, dim=0)
            L = torch.unsqueeze(L, dim=0)

        if episode_start is not None:
            episode_start = torch.zeros(
                (data.shape[0], data.shape[1], 1, 1), device=data.device
            )

        self.fu = []

        hidden = []
        out = 0
        elapsed_time = t / self.delta_time
        for i, inp in enumerate(data):
            self.gnn.f = self.f[i]
            x = x.detach() if hidden is not None else None

            for _ in range(self.delta_time):
                numerator, denominator = self.gnn.forward(
                    inp, L[i, :, :, :], (1 - episode_start[i]).reshape(-1, 1, 1) * x
                )
                x = (x + elapsed_time * numerator) / (1 + elapsed_time * denominator)
            hidden.append(x)

        out = torch.stack(hidden, 0)
        return out
