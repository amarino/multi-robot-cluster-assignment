import functools
import random
from copy import copy

import pygame

from shapely.geometry import Polygon, Point
from scipy.stats import multivariate_normal

import numpy as np
from numpy.linalg import norm as norm
import gymnasium as gym
from gymnasium.spaces import Box
from scipy.spatial import Voronoi

from pettingzoo import ParallelEnv
import colorsys

import cvxpy as cp

# import matplotlib.pyplot as plt


class Env(ParallelEnv):

    metadata = {
        "name": "coverage_environment_v0",
        "render_modes": ["human", "rgb_array"],
        "render_fps": 30,
    }

    def __init__(
        self,
        max_cycles=250,
        na=4,
        destinations=4,
        resources=1,
        render_mode="rgb_array",
        continuous_actions=False,
    ):
        super().__init__()

        self.render_mode = render_mode
        self.viewer = None
        self.width = 700
        self.height = 700
        self.max_size = 1
        # Set up the drawing window

        self.renderOn = False
        self.max_cycles = max_cycles
        self.continuous_actions = continuous_actions

        self.polygon = Polygon([(-1, -1), (1, -1), (1, 1), (-1, 1), (-1, -1)])
        num_samples = 100
        points = np.random.rand(
            num_samples, 2
        )  # Generate random points in [0, 1] x [0, 1]
        points_in_polygon = [
            Point(p).within(self.polygon) for p in points
        ]  # Check if points are in polygon
        self.valid_points = points[points_in_polygon]
        # self.valid_points = np.array([[-1, -1], [1, -1], [1, 1], [-1, 1]])

        self.previous_loc = []

        self.Na = na  # number of agents
        self.dest = destinations  # number of destinations
        self.agents_loc = np.zeros((na, 2), dtype=np.float32)
        self.dest_loc = np.random.rand(self.dest, 2).astype(np.float32) * 2 - 1

        # resources level
        self.resources = resources
        self.resource_type = np.random.randint(0, 1, size=(self.resources))
        self.d_std_l = 0.33
        self.r_std_l = 0.33
        self.deplation_threshold = 0.6
        self.agents_level = np.zeros((na, self.resources), dtype=np.float32)
        self.dests_level = np.zeros((self.dest, self.resources), dtype=np.float32)
        self.dests_level_init = np.zeros((self.dest, self.resources), dtype=np.float32)
        # demanding resources area
        self.dests_std_level = np.ones(self.dest).astype(np.float32) * self.d_std_l
        # robot resources area
        self.robots_std_level = (
            np.random.rand(self.Na).astype(np.float32) * self.r_std_l
        )

        self.eps = 1e-4

        self.possible_agents = ["a" + str(i) for i in range(na)]

        self.agent_name_mapping = dict(
            zip(self.possible_agents, list(range(len(self.possible_agents))))
        )

        self.idx_instant = np.array([], dtype=int)
        self.state_space = Box(
            -1,
            1,
            (
                (2 + 2 * self.resources) * self.Na
                + (2 + 2 * self.resources) * self.dest,
            ),
        )

        self.reset_d = True

        """
        If the objective is reached, we can reset the environment destination 
        locations and stds. This to achieve a form of curriculum learning.
        """
        self.objective_reached = True

        """
        If human-rendering is used, `self.window` will be a reference
        to the window that we draw to. `self.clock` will be a clock that is used
        to ensure that the environment is rendered at the correct framerate in
        human-mode. They will remain `None` until human-mode is used for the
        first time.
        """
        self.timestep = None
        self.canvas = None
        self.render_mode = render_mode
        self.window_size = 512  # The size of the PyGame window
        self.size = 700
        self.window = None
        self.clock = None

    @functools.lru_cache(maxsize=None)
    def observation_space(self, agent):
        # gymnasium spaces are defined and documented here: https://gymnasium.farama.org/api/spaces/
        return Box(
            -1, 1, ((2 * self.resources + 2) * self.dest + (2 * self.resources + 2),)
        )

    # Action space should be defined here.
    # If your spaces change over time, remove this line (disable caching).
    @functools.lru_cache(maxsize=None)
    def action_space(self, agent):
        return Box(-1, 1, (2,))

    def __get_obs(self):
        obs = {}
        dests_level = self.dests_level / (
            np.sum(self.dests_level, axis=0, keepdims=True) + 1e-6
        )
        resource_type = self.resource_type.reshape(1, -1).repeat(self.dest, axis=0)
        dests_level = np.concatenate(
            [
                dests_level.reshape(self.dest, -1, 1),
                resource_type.reshape(self.dest, -1, 1),
            ],
            axis=2,
        ).reshape(self.dest, -1)
        for i in range(self.Na):

            obs[self.possible_agents[i]] = np.concatenate(
                [
                    self.agents_loc[i, :].reshape(-1),
                    np.concatenate(
                        [
                            self.agents_level[i, :].reshape(-1, 1),
                            self.resource_type.reshape(-1, 1),
                        ],
                        axis=1,
                    ).reshape(-1),
                    np.concatenate(
                        [self.dest_loc, dests_level],
                        axis=1,
                    ).reshape(-1),
                ],
            ).astype(np.float32)

        return obs

    def __get_info(self):
        info = {agent: {} for agent in self.possible_agents}
        info["pos"] = self.agents_loc

        for i in range(self.dest):
            info["dest" + str(i)] = self.dests_level[i, :]

        return info

    def get_rewards(self):
        pass

    def reset(self, seed=None, options=None):
        """
        Reset needs to initialize the following attributes
        - agents
        - rewards
        - _cumulative_rewards
        - terminations
        - truncations
        - infos
        - agent_selection
        And must set up the environment so that render(), step(), and observe()
        can be called without issues.
        Here it sets up the state dictionary which is used by step() and the observations dictionary which is used by step() and observe()
        """

        self.agents_loc = np.random.rand(self.Na, 2).astype(np.float32) * 2 - 1
        self.old_loc = self.agents_loc

        same_resources = np.random.choice([0, 1], p=[0.7, 0.3])

        # 0 instant, 1 static
        self.resource_type = np.random.choice(
            [0, 1], size=(self.resources), p=[0.5, 0.5]
        )
        if same_resources:
            self.resource_type[:] = self.resource_type[:1]

        resource_module = np.ones(self.resource_type.shape)

        random_mult = (
            0.5 * np.random.rand(np.sum(self.resource_type)).astype(np.float32) + 0.5
        )

        resource_module[self.resource_type == 1] = random_mult

        self.agents_level = np.random.rand(self.Na, self.resources).astype(np.float32)
        sum_agents_level = np.sum(self.agents_level, axis=0, keepdims=True)
        self.agents_level = resource_module * self.agents_level / sum_agents_level
        self.agents_level[self.agents_level < self.eps] = 0

        if same_resources:
            self.agents_level[:, :] = self.agents_level[:, :1]

        self.idx_instant = [
            [[] for _ in range(self.resources)] for _ in range(self.dest)
        ]

        # robot resources area
        self.robots_std_level = (
            np.random.rand(self.Na).astype(np.float32) * self.r_std_l
        )

        self.success = np.array(np.zeros((self.dest, self.resources)), dtype=bool)

        self.dest_loc = np.array(self.RandX(self.dest, 0.5))

        self.dests_std_level = np.ones(self.dest).astype(np.float32) * 0.2

        self.dests_level = np.random.rand(self.dest, self.resources).astype(np.float32)
        sum_dests_level = np.sum(self.dests_level, axis=0, keepdims=True)
        self.dests_level = self.dests_level / sum_dests_level
        self.dests_level[self.dests_level < self.eps] = 0

        if same_resources:
            self.dests_level[:, :] = self.dests_level[:, :1]

        self.old_dl = copy(self.dests_level)

        self.agents = self.possible_agents[:]
        infos = self.__get_info()
        observations = self.__get_obs()

        self.timestep = 0

        return observations, infos

    def pos(self):
        return self.agents_loc

    def state(self):

        return np.concatenate(
            [
                self.agents_loc.reshape(-1),
                self.dest_loc.reshape(-1),
                self.agents_level.reshape(-1),
                self.dests_level.reshape(-1),
                # self.dests_std_level.reshape(-1),
            ],
            axis=0,
        )

    def evaluate_density(self, loc, dest_loc, level, std):
        dist = norm(dest_loc - loc, axis=-1) ** 2
        return level * np.exp(-(dist) / std)

    def calculate_cost(self, al, dl, dist):
        # Cost is inversely proportional to the number of resources satisfied

        dl = np.sum(dl * (al > self.eps).astype(np.float32))
        al = np.sum(al)

        resources_satisfied = (al) / (dl + self.eps) + dist * al
        if resources_satisfied < self.eps:
            return 1000
        return resources_satisfied

    def RandX(self, N, r_threshold):
        points = []
        scope = np.arange(-1, 1, 0.05)
        while len(points) < N:
            new_point = np.random.choice(scope, 2)
            if np.all(
                np.array([norm(new_point - points[i]) for i in range(len(points))])
                > r_threshold
            ):
                points.append(new_point)
        return points

    def step(self, actions):
        """
        step(action) takes in an action for each agent and should return the
          - observations
          - rewards
          - terminations
          - truncations
          - infos
         dicts where each dict looks like {agent_1: item_1, agent_2: item_2}
        """

        self.old_loc = copy(self.agents_loc)
        # update agents location with actions
        for agent in self.agents:
            id = self.agent_name_mapping[agent]
            self.agents_loc[id, :] = np.clip(
                self.agents_loc[id, :] + 0.05 * actions[agent], -1, 1
            )

        # rewards for all agents are placed in the rewards dictionary
        rewards_inst = {agent: 0 for agent in self.agents}
        rewards_static = {agent: 0 for agent in self.agents}
        env_truncation = False

        dl = copy(self.dests_level)
        al = copy(self.agents_level)

        dist = (
            norm(
                self.dest_loc.reshape(-1, 1, 2) - self.agents_loc.reshape(1, -1, 2),
                axis=-1,
            )
            ** 2
        )
        dest_dist = np.exp(-(dist) / self.dests_std_level.reshape(-1, 1))
        dist_old = (
            norm(
                self.dest_loc.reshape(-1, 1, 2) - self.old_loc.reshape(1, -1, 2),
                axis=-1,
            )
            ** 2
        )

        idx = [[] for _ in range(self.dest)]

        # update resources level
        for k, agent in enumerate(self.agents):

            dist_agent = norm(
                self.agents_loc[k : k + 1, :] - self.agents_loc[:, :], axis=1
            )
            for j in range(dist_agent.shape[0]):
                if j != k:
                    if dist_agent[j] < 0.1:
                        rewards_static[agent] += -0.1 * (0.1 - dist_agent[j])

            borders = np.array(
                [
                    [self.agents_loc[k, 0], 1],
                    [self.agents_loc[k, 0], -1],
                    [1, self.agents_loc[k, 1]],
                    [-1, self.agents_loc[k, 1]],
                ]
            )
            dist_borders = norm(self.agents_loc[k : k + 1, :] - borders, axis=1)
            for j in range(dist_borders.shape[0]):
                if dist_borders[j] < 0.01:
                    rewards_static[agent] += -1

            if np.sum(self.agents_level[k, :]) < self.eps:
                if norm(actions[agent]) > self.eps:
                    rewards_static[agent] += -0.00 * norm(actions[agent])

        for i in range(self.dest):
            dist_max = (-dest_dist[i, :]).argsort()
            for k in range(self.Na):
                kl = dist_max[k]
                agent = self.agents[kl]
                if np.sum(al[kl, :]) >= self.eps:
                    if dest_dist[i, kl] >= self.deplation_threshold:
                        # discourage remaining at a destination already fulfilled
                        if np.any(np.sum(dl[i, :]) > self.eps):
                            min_level = np.min([dl[i, :], al[kl, :]], axis=0)

                            for id_type, r_type in enumerate(self.resource_type):
                                if r_type:
                                    dl[i, id_type] -= min_level[id_type]
                                    al[kl, id_type] -= min_level[id_type]
                                    if min_level[id_type] > self.eps:
                                        idx[i].append(agent)
                                        rewards_static[agent] += (
                                            0.5
                                            * self.dests_level[i, id_type]
                                            / self.resources
                                        )
                                        # 0.5 * self.dests_level[i, id_type]
                                else:
                                    if self.dests_level[i, id_type] >= self.eps:
                                        self.dests_level[i, id_type] -= min_level[
                                            id_type
                                        ]
                                        self.agents_level[kl, id_type] -= min_level[
                                            id_type
                                        ]
                                        al[kl, id_type] -= min_level[id_type]
                                        dl[i, id_type] -= min_level[id_type]
                                        if min_level[id_type] > self.eps:
                                            # compute reward for the density function
                                            rewards_inst[agent] += 0.5
                                            self.idx_instant[i][id_type].append(kl)
                                        if self.dests_level[i, id_type] < self.eps:
                                            self.success[i, id_type] = True

        idx_r = []

        cost_matrix = np.full((self.Na, self.dest), np.inf)

        for k, agent in enumerate(self.agents):
            for i in range(self.dest):
                cost_matrix[k, i] = self.calculate_cost(
                    self.agents_level[k, :] * (1 - self.resource_type),
                    self.dests_level[i, :] * (1 - self.resource_type),
                    dist[i, k],
                )

        num_rows, num_cols = cost_matrix.shape

        # Decision variables: binary matrix of size (num_rows, num_cols)
        x = cp.Variable((num_rows, num_cols), boolean=True)

        al_sum = np.sum(self.agents_level[:, :], axis=1)
        dl_sum = np.sum(self.dests_level[:, :], axis=1)

        # Objective: Minimize the total cost
        if np.sum(self.agents_level[:, self.resource_type == 0]) > self.eps:
            objective = cp.Minimize(cp.sum(cp.multiply(cost_matrix, x)))
        else:
            objective = cp.Minimize(cp.norm(dl_sum - x.T @ al_sum, 1))

        # Constraints: Each row is assigned to exactly one column
        row_constraints = [cp.sum(x[i, :]) == 1 for i in range(num_rows)]

        if np.sum(self.agents_level[:, self.resource_type == 0]) < self.eps:
            # Constraints: Each column is assigned to exactly one row
            col_constraints = [cp.sum(x[:, j]) >= 1 for j in range(num_cols)]
        else:
            col_constraints = []
        # Combine all constraints
        constraints = row_constraints + col_constraints

        # Solve the problem
        problem = cp.Problem(objective, constraints)
        problem.solve()

        x = np.array(x.value)
        assignment = [
            (i, j) for i in range(num_rows) for j in range(num_cols) if x[i, j] > 0.5
        ]

        for k, i in assignment:
            idx_r.append(i)
            if dest_dist[i, k] < self.deplation_threshold:
                rewards_static[self.agents[k]] += 10 * (dist_old[i, k] - dist[i, k]) / 8
            else:
                rewards_static[self.agents[k]] += 0.1 * np.sum(
                    self.dests_level[i, self.resource_type == 1]
                )
        if np.any(self.success):
            for i, s in enumerate(self.success):
                for j in range(self.resources):
                    if s[j]:
                        for k in self.idx_instant[i][j]:
                            rewards_static[self.agents[k]] += 0.3 / self.resources

        # if np.sum(self.dests_level) < self.eps:
        #    env_truncation = True

        global_rewards = 0

        # there must be an agent for each destinaton
        if all(idx):
            global_rewards += 0.2 * np.any(self.resource_type).astype(int)

        for k, agent in enumerate(self.agents):
            rewards_static[agent] += 0.05 if np.sum(al[k, :]) < self.eps else 0

        # account for the fulfilment of the overall objective
        global_rewards += 10 * (np.sum(self.old_dl) - np.sum(dl)) / self.resources

        rewards = {
            agent: rewards_inst[agent] + (rewards_static[agent] + global_rewards) * 0.1
            for agent in self.agents
        }

        self.reset_d = True
        self.timestep += 1

        # The truncations dictionary must be updated for all players.
        if self.timestep >= self.max_cycles:
            env_truncation = True

        self.old_dl = dl
        truncations = {agent: False for agent in self.agents}
        terminations = {agent: env_truncation for agent in self.agents}

        # observe the current state
        observations = self.__get_obs()

        self.observations = observations

        # typically there won't be any information in the infos, but there must
        # still be an entry for each agent
        infos = self.__get_info()

        return observations, rewards, terminations, truncations, infos

    def show_text(self, text, x, y, color=(0, 0, 0)):
        text = pygame.font.Font(None, 19).render(text, True, color)
        return (text, (x, y))

    def convert_xy_to_screen(self, xy):
        return xy * np.array([(self.width - 3) / 2, -(self.height - 3) / 2]) + np.array(
            [self.width / 2, self.height / 2]
        )

    def voronoi_finite_polygons_2d(self, vor, radius=None):
        """
        Reconstruct infinite voronoi regions in a 2D diagram to finite
        regions.
        """
        if vor.points.shape[1] != 2:
            raise ValueError("Requires 2D input")

        new_regions = []
        new_vertices = vor.vertices.tolist()

        center = vor.points.mean(axis=0)
        if radius is None:
            radius = vor.points.ptp().max() * 2

        # Construct a map containing all ridges for a given point
        all_ridges = {}
        for (p1, p2), (v1, v2) in zip(vor.ridge_points, vor.ridge_vertices):
            all_ridges.setdefault(p1, []).append((p2, v1, v2))
            all_ridges.setdefault(p2, []).append((p1, v1, v2))

        # Reconstruct infinite regions
        for p1, region in enumerate(vor.point_region):
            vertices = vor.regions[region]

            if all(v >= 0 for v in vertices):
                # finite region
                new_regions.append(vertices)
                continue

            # reconstruct a non-finite region
            ridges = all_ridges[p1]
            new_region = [v for v in vertices if v >= 0]

            for p2, v1, v2 in ridges:
                if v2 < 0:
                    v1, v2 = v2, v1
                if v1 >= 0:
                    # finite ridge: already in the region
                    continue

                # Compute the missing endpoint of an infinite ridge
                t = vor.points[p2] - vor.points[p1]  # tangent
                t /= np.linalg.norm(t)
                n = np.array([-t[1], t[0]])  # normal

                midpoint = vor.points[[p1, p2]].mean(axis=0)
                direction = np.sign(np.dot(midpoint - center, n)) * n
                far_point = vor.vertices[v2] + direction * radius

                far_point = np.clip(far_point, -1, 1)
                new_region.append(len(new_vertices))
                new_vertices.append(far_point.tolist())

            if len(new_region) < 4:
                vs = np.asarray([new_vertices[v] for v in new_region])
                borders = np.array([[-1, -1], [1, -1], [1, 1], [-1, 1]])
                mv = np.mean(
                    np.linalg.norm(
                        vs.reshape(1, -1, 2) - borders.reshape(-1, 1, 2), axis=-1
                    ),
                    axis=-1,
                )
                idx = np.argmin(mv)
                new_region.append(len(new_vertices))
                new_vertices.append(borders[idx].tolist())

            # sort region counterclockwise
            vs = np.asarray([new_vertices[v] for v in new_region])
            c = vs.mean(axis=0)
            angles = np.arctan2(vs[:, 1] - c[1], vs[:, 0] - c[0])
            new_region = np.array(new_region)[np.argsort(angles)]

            # finish
            new_regions.append(new_region.tolist())
        return new_regions, np.asarray(new_vertices)

    def draw_voronoi(self, canvas):
        # generate Voronoi diagram
        try:
            vor = Voronoi(
                self.agents_loc
                + np.random.rand(*self.agents_loc.shape) * 0.0001
                - 0.0001
            )
        except:
            print("Voronoi failed")
            print(f"loc {self.agents_loc}")
            return

        center = vor.points.mean(axis=0)
        # draw all the edges
        for pointidx, simplex in zip(vor.ridge_points, vor.ridge_vertices):
            simplex = np.asarray(simplex)
            if np.all(simplex >= 0):
                points = vor.vertices[simplex]
                start = self.convert_xy_to_screen(points[0])
                end = self.convert_xy_to_screen(points[1])
                pygame.draw.line(canvas, (0, 0, 0), start, end)
            else:
                i = simplex[simplex >= 0][0]  # finite end Voronoi vertex

                t = vor.points[pointidx[1]] - vor.points[pointidx[0]]  # tangent
                t /= np.linalg.norm(t)
                n = np.array([-t[1], t[0]])  # normal

                midpoint = vor.points[pointidx].mean(axis=0)
                direction = np.sign(np.dot(midpoint - center, n)) * n
                if vor.furthest_site:
                    direction = -direction
                aspect_factor = 10.0
                far_point = vor.vertices[i] + direction * aspect_factor

                start = self.convert_xy_to_screen(vor.vertices[i])
                end = self.convert_xy_to_screen(far_point)
                pygame.draw.line(canvas, (0, 0, 0), start, end)

    def render(self):

        if self.render_mode is None:
            gym.logger.warn(
                "You are calling render method without specifying any render mode."
            )
            return
        else:
            if self.canvas is None:
                pygame.init()
                self.canvas = pygame.Surface((self.width, self.height), pygame.SRCALPHA)

            if self.window is None and self.render_mode == "human":
                pygame.display.init()
                self.window = pygame.display.set_mode((self.width, self.height))

            if self.clock is None and self.render_mode == "human":
                self.clock = pygame.time.Clock()

            self.canvas.fill(
                tuple(round(255 * i) for i in colorsys.hsv_to_rgb(0.666666, 1, 1))
                + (200,)
            )

            # Draw density function
            """
            xv, yv = np.meshgrid(np.linspace(-1, 1, self.width), np.linspace(-1, 1, self.height))
            dens = [
                sum(self.dests_level[i, :])
                * np.exp(
                    (-((self.dest_loc[i, 0] - xv) ** 2) - (self.dest_loc[i, 1] - yv) ** 2)
                    / self.dests_std_level[i]
                )
                for i in range(self.dest)
            ]
            dens = sum(dens)
            c = plt.imshow(dens, cmap="gist_heat", origin="lower")
            img = c.make_image(renderer=None, unsampled=True)[0]
            surf = pygame.surfarray.make_surface(img[:, :, :3])
            self.canvas.blit(surf, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)
            
            """

            for i in range(self.dest):
                surf = pygame.Surface((self.width, self.height), pygame.SRCALPHA)
                dest_loc = self.dest_loc[i, :]
                for k in reversed(range(10, 500, 10)):
                    loc_range = np.array([k, k]) / np.array(
                        [(self.width) / 2, self.height / 2]
                    )

                    dist = sum(self.dests_level[i, :]) * np.exp(
                        -norm(loc_range, axis=0) ** 2 / self.dests_std_level[i]
                    )

                    color = tuple(
                        round(255 * i)
                        for i in colorsys.hsv_to_rgb(0.666666 + dist * 0.33, 1, 1)
                    )
                    pygame.draw.circle(
                        surf,
                        color=pygame.Color(*color),
                        center=self.convert_xy_to_screen(dest_loc),
                        radius=k,
                    )

                self.canvas.blit(surf, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)

            text = []
            for i in range(self.Na):
                agent = self.convert_xy_to_screen(self.agents_loc[i, :])
                x = agent[0] + 5
                text.append(
                    self.show_text(
                        "[",
                        x,
                        agent[1] + 5,
                        color=(0, 0, 0),
                    )
                )
                font = pygame.freetype.Font(None, 19)
                x += font.get_metrics("[")[0][4]
                for j in range(self.resources):
                    text.append(
                        self.show_text(
                            str(round(self.agents_level[i, j], 2)),
                            x,
                            agent[1] + 7,
                            color=(
                                (255, 165, 20)
                                if self.resource_type[j]
                                else (100, 255, 50)
                            ),
                        )
                    )
                    metrics = font.get_metrics(str(round(self.agents_level[i, j], 2)))
                    x += sum(m[4] for m in metrics) - 8

                text.append(
                    self.show_text(
                        "]",
                        x,
                        agent[1] + 5,
                        color=(0, 0, 0),
                    )
                )

                # Now we draw the agent
                pygame.draw.circle(self.canvas, (0, 255, 0), agent, 5)

            for i in range(self.dest):
                destination = self.convert_xy_to_screen(self.dest_loc[i, :])
                x = destination[0] + 5

                text.append(
                    self.show_text(
                        "[",
                        x,
                        destination[1] + 5,
                        color=(0, 0, 0),
                    )
                )
                font = pygame.freetype.Font(None, 19)
                x += font.get_metrics("[")[0][4]

                for j in range(self.resources):
                    text.append(
                        self.show_text(
                            str(round(self.dests_level[i, j], 2)),
                            x,
                            destination[1] + 7,
                            color=(
                                (255, 165, 20)
                                if self.resource_type[j]
                                else (100, 255, 50)
                            ),
                        )
                    )
                    metrics = font.get_metrics(str(round(self.dests_level[i, j], 2)))
                    x += sum(m[4] for m in metrics) - 8

                text.append(
                    self.show_text(
                        "]",
                        x,
                        destination[1] + 5,
                        color=(0, 0, 0),
                    )
                )
                pygame.draw.circle(self.canvas, (255, 0, 0), destination, 5)

            # self.draw_voronoi(self.canvas)
            for t in text:
                self.canvas.blit(*t)

            if self.render_mode == "human":
                # The following line copies our drawings from `canvas` to the visible window
                self.window.blit(self.canvas, self.canvas.get_rect())
                pygame.event.pump()
                pygame.display.update()

                # We need to ensure that human-rendering occurs at the predefined framerate.
                # The following line will automatically add a delay to keep the framerate stable.
                self.clock.tick(self.metadata["render_fps"])
            else:  # rgb_array
                return np.transpose(
                    np.array(pygame.surfarray.pixels3d(self.canvas)), axes=(1, 0, 2)
                )

    def close(self):
        """
        Close should release any graphical displays, subprocesses, network connections
        or any other environment data which should not be kept around after the
        user is no longer using the environment.
        """
        if self.window is not None:
            pygame.display.quit()
            pygame.quit()
