import functools
import random
from copy import copy

import pygame

import numpy as np
from numpy.linalg import norm as norm
import gymnasium as gym
from gymnasium.spaces import Box
from scipy.spatial import Voronoi

from pettingzoo import ParallelEnv
import colorsys

# import matplotlib.pyplot as plt


class Env(gym.Env):

    metadata = {
        "name": "coverage_environment_v0",
        "render_modes": ["human", "rgb_array"],
        "render_fps": 30,
    }

    def __init__(
        self,
        max_cycles=250,
        na=4,
        destinations=4,
        render_mode="rgb_array",
        continuous_actions=False,
    ):
        super().__init__()

        self.render_mode = render_mode
        self.viewer = None
        self.width = 700
        self.height = 700
        self.max_size = 1
        # Set up the drawing window

        self.renderOn = False
        self.max_cycles = max_cycles
        self.continuous_actions = continuous_actions

        self.previous_loc = []

        self.Na = na  # number of agents
        self.dest = destinations  # number of destinations
        self.agents_loc = np.zeros((na, 2), dtype=np.float32)
        self.old_loc = self.agents_loc  # old position of the agents
        self.dest_loc = np.random.rand(self.dest, 2).astype(np.float32) * 2 - 1

        # resources level
        self.resources = 1
        self.d_std_l = 0.33
        self.r_std_l = 0.33
        self.deplation_threshold = 0.6
        self.agents_level = np.zeros((na, self.resources), dtype=np.float32)
        self.dests_level = np.zeros((self.dest, self.resources), dtype=np.float32)
        self.dests_level_init = np.zeros((self.dest, self.resources), dtype=np.float32)
        # demanding resources area
        self.dests_std_level = np.ones(self.dest).astype(np.float32) * self.d_std_l
        # robot resources area
        self.robots_std_level = (
            np.random.rand(self.Na).astype(np.float32) * self.r_std_l
        )

        self.possible_agents = ["a" + str(i) for i in range(na)]

        self.agent_name_mapping = dict(
            zip(self.possible_agents, list(range(len(self.possible_agents))))
        )

        self.state_space = Box(
            -1,
            1,
            ((2 + self.resources) * self.Na + (2 + self.resources) * self.dest,),
        )

        self.observation_space = gym.spaces.Dict(
            {
                agent: Box(
                    -1,
                    1,
                    ((self.resources * self.dest + 2 * self.dest + 3),),
                )
                for agent in self.possible_agents
            }
        )
        self.action_space = Box(-1, 1, (na, 2))

        self.reset_d = True

        """
        If the objective is reached, we can reset the environment destination 
        locations and stds. This to achieve a form of curriculum learning.
        """
        self.objective_reached = True

        """
        If human-rendering is used, `self.window` will be a reference
        to the window that we draw to. `self.clock` will be a clock that is used
        to ensure that the environment is rendered at the correct framerate in
        human-mode. They will remain `None` until human-mode is used for the
        first time.
        """
        self.timestep = None
        self.canvas = None
        self.render_mode = render_mode
        self.window_size = 512  # The size of the PyGame window
        self.size = 700
        self.window = None
        self.clock = None

    def __get_obs(self):
        obs = {}

        for i in range(self.Na):
            dist = norm(self.dest_loc - self.agents_loc[i, :], axis=1) ** 2
            dest_dist = np.exp(-(dist) / self.dests_std_level)

            obs[self.possible_agents[i]] = np.concatenate(
                [
                    self.agents_loc[i, :].reshape(-1),
                    self.dest_loc.reshape(-1),
                    (self.dests_level.reshape(-1)),
                    # dest_dist.reshape(-1),
                    self.agents_level[i, :].reshape(-1),
                ],
            ).astype(np.float32)

        return obs

    def __get_info(self):
        info = {
            agent: {"pos": self.agents_loc[i, :]} for i, agent in enumerate(self.agents)
        }

        for agent in self.agents:
            for i in range(self.dest):
                info[agent]["dest" + str(i)] = self.dests_level[i, :]
        return info

    def get_rewards(self):
        pass

    def reset(self, seed=None, options=None):
        """
        Reset needs to initialize the following attributes
        - agents
        - rewards
        - _cumulative_rewards
        - terminations
        - truncations
        - infos
        - agent_selection
        And must set up the environment so that render(), step(), and observe()
        can be called without issues.
        Here it sets up the state dictionary which is used by step() and the observations dictionary which is used by step() and observe()
        """

        self.agents_loc = np.random.rand(self.Na, 2).astype(np.float32) * 2 - 1
        self.old_loc = self.agents_loc
        self.agents_level = np.random.rand(self.Na, 1).astype(np.float32)
        sum_agents_level = np.sum(self.agents_level)
        self.agents_level = self.agents_level / sum_agents_level

        # robot resources area
        self.robots_std_level = (
            np.random.rand(self.Na).astype(np.float32) * self.r_std_l
        )

        if self.reset_d:
            self.dest_loc = np.clip(
                np.random.normal(0, 1, size=(self.dest, 2)), -1, 1
            ).astype(np.float32)

            # demanding resources area
            # self.dests_std_level = (
            #    np.random.rand(self.dest).astype(np.float32) * (self.d_std_l - 0.1) + 0.1
            # )
            self.dests_std_level = np.ones(self.dest).astype(np.float32) * 0.3

            self.dests_level_init = np.random.rand(self.dest, 1).astype(np.float32)
            sum_dests_level = np.sum(self.dests_level_init)
            self.dests_level_init = self.dests_level_init / sum_dests_level
            self.reset_d = True

        self.dests_level = copy(self.dests_level_init)

        self.agents = self.possible_agents[:]
        infos = self.__get_info()
        observations = self.__get_obs()

        self.timestep = 0

        return observations, infos

    def pos(self):
        return self.agents_loc

    def state(self):

        return np.concatenate(
            [
                self.agents_loc.reshape(-1),
                self.dest_loc.reshape(-1),
                self.agents_level.reshape(-1),
                self.dests_level.reshape(-1),
                # self.dests_std_level.reshape(-1),
            ],
            axis=0,
        )

    def step(self, actions):

        self.old_loc = self.agents_loc

        # update agents location with actions
        for agent in self.agents:
            id = self.agent_name_mapping[agent]
            self.agents_loc[id, :] = np.clip(
                self.agents_loc[id, :] + 0.05 * actions[id, :], -1, 1
            )

        # rewards for all agents are placed in the rewards dictionary
        rewards = np.zeros((self.Na, 1))
        env_truncation = False

        # update resources level
        for k, agent in enumerate(self.agents):

            dist = norm(self.agents_loc[k : k + 1, :] - self.agents_loc[:, :], axis=1)
            for j in range(dist.shape[0]):
                # rewards[self.possible_agents[i]] += -dist[k]
                if j != k:
                    if dist[j] < 0.01:
                        rewards[k, :] += -0.0

            borders = np.array(
                [
                    [self.agents_loc[k, 0], 1],
                    [self.agents_loc[k, 0], -1],
                    [1, self.agents_loc[k, 1]],
                    [-1, self.agents_loc[k, 1]],
                ]
            )
            dist = norm(self.agents_loc[k : k + 1, :] - borders, axis=1)
            for j in range(dist.shape[0]):
                if dist[j] < 0.01:
                    rewards[k, :] += -2 / self.Na
                    env_truncation = True

            if self.agents_level[k, 0] > 1e-4:

                for i in range(self.dest):

                    if self.dests_level[i, 0] > 1e-4:
                        dist = norm(self.dest_loc[i, :] - self.agents_loc[k, :]) ** 2
                        dist_old = norm(self.dest_loc[i, :] - self.old_loc[k, :]) ** 2
                        dest_dist = np.exp(-(dist) / self.dests_std_level[i])
                        dest_dist_old = np.exp(-dist_old / self.dests_std_level[i])
                        if dest_dist > self.deplation_threshold:
                            min_level = np.min(
                                [self.dests_level[i, 0], self.agents_level[k, 0]]
                            )
                            # compute reward for the density function
                            rewards[k, :] += self.dests_level[i, 0]
                            self.dests_level[i, :] -= min_level
                            self.agents_level[k, :] -= min_level

        # rewards = sum(rewards.values())

        if sum(self.dests_level) < 1e-4:
            rewards += 2 / self.Na
            env_truncation = True

        rewards = np.sum(rewards)

        self.reset_d = True
        self.timestep += 1

        # The truncations dictionary must be updated for all players.
        if self.timestep >= self.max_cycles:
            env_truncation = True

        truncations = False
        terminations = env_truncation

        # observe the current state
        observations = self.__get_obs()

        self.observations = observations

        # typically there won't be any information in the infos, but there must
        # still be an entry for each agent
        infos = self.__get_info()

        return observations, rewards, terminations, truncations, infos

    def show_text(self, text, x, y):
        text = pygame.font.Font(None, 18).render(text, True, (0, 0, 0))
        return (text, (x, y))

    def convert_xy_to_screen(self, xy):
        return xy * np.array([(self.width - 3) / 2, -(self.height - 3) / 2]) + np.array(
            [self.width / 2, self.height / 2]
        )

    def draw_voronoi(self, canvas):
        # generate Voronoi diagram
        try:
            vor = Voronoi(
                self.agents_loc
                + np.random.rand(*self.agents_loc.shape) * 0.0001
                - 0.0001
            )
        except:
            print("Voronoi failed")
            print(f"loc {self.agents_loc}")
            return

        center = vor.points.mean(axis=0)
        # draw all the edges
        for pointidx, simplex in zip(vor.ridge_points, vor.ridge_vertices):
            simplex = np.asarray(simplex)
            if np.all(simplex >= 0):
                points = vor.vertices[simplex]
                start = self.convert_xy_to_screen(points[0])
                end = self.convert_xy_to_screen(points[1])
                pygame.draw.line(canvas, (0, 0, 0), start, end)
            else:
                i = simplex[simplex >= 0][0]  # finite end Voronoi vertex

                t = vor.points[pointidx[1]] - vor.points[pointidx[0]]  # tangent
                t /= np.linalg.norm(t)
                n = np.array([-t[1], t[0]])  # normal

                midpoint = vor.points[pointidx].mean(axis=0)
                direction = np.sign(np.dot(midpoint - center, n)) * n
                if vor.furthest_site:
                    direction = -direction
                aspect_factor = 10.0
                far_point = vor.vertices[i] + direction * aspect_factor

                start = self.convert_xy_to_screen(vor.vertices[i])
                end = self.convert_xy_to_screen(far_point)
                pygame.draw.line(canvas, (0, 0, 0), start, end)

    def render(self):

        if self.render_mode is None:
            gym.logger.warn(
                "You are calling render method without specifying any render mode."
            )
            return
        else:
            if self.canvas is None:
                pygame.init()
                self.canvas = pygame.Surface((self.width, self.height), pygame.SRCALPHA)

            if self.window is None and self.render_mode == "human":
                pygame.display.init()
                self.window = pygame.display.set_mode((self.width, self.height))

            if self.clock is None and self.render_mode == "human":
                self.clock = pygame.time.Clock()

            self.canvas.fill(
                tuple(round(255 * i) for i in colorsys.hsv_to_rgb(0.666666, 1, 1))
                + (200,)
            )

            # Draw density function
            """
            xv, yv = np.meshgrid(np.linspace(-1, 1, self.width), np.linspace(-1, 1, self.height))
            dens = [
                sum(self.dests_level[i, :])
                * np.exp(
                    (-((self.dest_loc[i, 0] - xv) ** 2) - (self.dest_loc[i, 1] - yv) ** 2)
                    / self.dests_std_level[i]
                )
                for i in range(self.dest)
            ]
            dens = sum(dens)
            c = plt.imshow(dens, cmap="gist_heat", origin="lower")
            img = c.make_image(renderer=None, unsampled=True)[0]
            surf = pygame.surfarray.make_surface(img[:, :, :3])
            self.canvas.blit(surf, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)
            
            """

            for i in range(self.dest):
                surf = pygame.Surface((self.width, self.height), pygame.SRCALPHA)
                dest_loc = self.dest_loc[i, :]
                for k in reversed(range(10, 500, 10)):
                    loc_range = np.array([k, k]) / np.array(
                        [(self.width) / 2, self.height / 2]
                    )

                    dist = sum(self.dests_level[i, :]) * np.exp(
                        -norm(loc_range, axis=0) ** 2 / self.dests_std_level[i] / 2
                    )
                    dist = np.clip(dist, 0, 1)
                    color = tuple(
                        round(255 * i)
                        for i in colorsys.hsv_to_rgb(0.666666 + dist * 0.33, 1, 1)
                    )
                    pygame.draw.circle(
                        surf,
                        color=pygame.Color(*color),
                        center=self.convert_xy_to_screen(dest_loc),
                        radius=k,
                    )

                self.canvas.blit(surf, (0, 0), special_flags=pygame.BLEND_RGBA_ADD)

            text = []
            for i in range(self.Na):
                agent = self.convert_xy_to_screen(self.agents_loc[i, :])

                text.append(
                    self.show_text(
                        str(round(self.agents_level[i, 0], 2)),
                        agent[0] + 5,
                        agent[1] + 5,
                    )
                )
                # Now we draw the agent
                pygame.draw.circle(self.canvas, (0, 255, 0), agent, 5)

            for i in range(self.dest):
                destination = self.convert_xy_to_screen(self.dest_loc[i, :])
                text.append(
                    self.show_text(
                        str(round(self.dests_level[i, 0], 2)),
                        destination[0] + 5,
                        destination[1] + 5,
                    )
                )
                pygame.draw.circle(self.canvas, (255, 0, 0), destination, 5)

            # self.draw_voronoi(self.canvas)
            for t in text:
                self.canvas.blit(*t)

            if self.render_mode == "human":
                # The following line copies our drawings from `canvas` to the visible window
                self.window.blit(self.canvas, self.canvas.get_rect())
                pygame.event.pump()
                pygame.display.update()

                # We need to ensure that human-rendering occurs at the predefined framerate.
                # The following line will automatically add a delay to keep the framerate stable.
                self.clock.tick(self.metadata["render_fps"])
            else:  # rgb_array
                return np.transpose(
                    np.array(pygame.surfarray.pixels3d(self.canvas)), axes=(1, 0, 2)
                )

    def close(self):
        """
        Close should release any graphical displays, subprocesses, network connections
        or any other environment data which should not be kept around after the
        user is no longer using the environment.
        """
        if self.window is not None:
            pygame.display.quit()
            pygame.quit()
