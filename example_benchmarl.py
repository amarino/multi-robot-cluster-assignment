from BenchMARL.benchmarl.algorithms import (
    MappoConfig,
    MasacConfig,
    MaddpgConfig,
    IppoConfig,
    IddpgConfig,
)
from BenchMARL.benchmarl.environments import VmasTask
from BenchMARL.benchmarl.experiment import Experiment, ExperimentConfig
from BenchMARL.benchmarl.models.mlp import MlpConfig
from BenchMARL.benchmarl.models.gru import GruConfig
from BenchMARL.benchmarl.models.gnn import GnnConfig
from coverage_environment.coverage_environment.models.gnnc import GnnCConfig
from BenchMARL.benchmarl.models.common import SequenceModelConfig
from BenchMARL.benchmarl.benchmark import Benchmark
from pathlib import Path
from coverage_environment.coverage_environment.custom_experiment import (
    CustomExperimentConfig,
    CustomExperiment,
)
import coverage_environment.coverage_environment as ce
from coverage_environment.coverage_environment import CoverageEnvTask
import os
import torch.nn as nn

restore = False
seeds = {0}
experiment_folder_name = "mappo_simple_ce_mlp__430fb6a5_24_07_26-19_00_48"
checkpoint = "checkpoint_2700000.pt"

experimenal_folder = "config/experimental_config"
task_folder = "config/task_config"
model_folder = "config/model_config"
algorithms_folder = "config/algorithms_config"


if __name__ == "__main__":

    file_path_ce = Path(ce.__file__).parent

    # Loads from "benchmarl/conf/task/vmas/balance.yaml"
    task = CoverageEnvTask.SIMPLE_CE.get_from_yaml(
        file_path_ce / task_folder / "simple_ce_task.yaml"
    )
    # task = VmasTask.BALANCE.get_from_yaml()

    # Loads from "benchmarl/conf/algorithm/mappo.yaml"
    algorithm_config = MappoConfig.get_from_yaml(
        file_path_ce / algorithms_folder / "mappo_config.yaml"
    )
    # algorithm_config = IppoConfig.get_from_yaml()
    # algorithm_config = MasacConfig.get_from_yaml(
    #    file_path_ce / algorithms_folder / "masac_config.yaml"
    # )

    # Loads from "benchmarl/conf/model/layers/mlp.yaml"
    model_config = MlpConfig.get_from_yaml(file_path_ce / model_folder / "mlp_config.yaml")
    # model_config = GruConfig.get_from_yaml(file_path_ce / model_folder / "gru_config.yaml")

    """
    model_config = SequenceModelConfig(
        model_configs=[
            MlpConfig(num_cells=[128, 128], activation_class=nn.Tanh, layer_class=nn.Linear),
            GnnCConfig.get_from_yaml(
                file_path_ce / model_folder / "gnnc_config.yaml"
            ),  # Loads from "benchmarl/conf/model/layers/mlp.yaml"
            MlpConfig.get_from_yaml(file_path_ce / model_folder / "mlp_config.yaml"),
        ],
        intermediate_sizes=[64, 24],  # Nuber of intermediate outputs. List of size n_layers - 1
    )
    """

    critic_model_config = MlpConfig.get_from_yaml(file_path_ce / model_folder / "mlp_config.yaml")
    # critic_model_config = GruConfig.get_from_yaml(file_path_ce / model_folder / "gru_config.yaml")

    # Loads from "benchmarl/conf/experiment/base_experiment.yaml"
    experiment_config = ExperimentConfig.get_from_yaml(
        file_path_ce / experimenal_folder / "experimental_config.yaml"
    )

    experiment_config.checkpoint_interval = experiment_config.on_policy_collected_frames_per_batch

    os.makedirs(experiment_config.save_folder, exist_ok=True)
    
    algorithm_config = MappoConfig.get_from_yaml()

    # Loads from "benchmarl/conf/model/layers/mlp.yaml"
    model_config = MlpConfig.get_from_yaml()
    critic_model_config = MlpConfig.get_from_yaml()

    experiment_config = ExperimentConfig.get_from_yaml()
    experiment_config.loggers = []
    if restore:

        # Now we tell it where to restore  from
        experiment_config.restore_file = (
            Path(__file__).parent
            / experiment_config.save_folder
            / experiment_folder_name
            / "checkpoints"
            / "checkpoint_2700000.pt"
        )
        # The experiment will be saved in the ame folder as the one it is restoring from
        experiment_config.save_folder = None

        experiment = Experiment(
            algorithm_config=algorithm_config,
            model_config=model_config,
            seed=1,
            config=experiment_config,
            task=task,
        )
        experiment.run()

    else:

        experiment = Experiment(
            task=task,
            algorithm_config=algorithm_config,
            model_config=model_config,
            critic_model_config=critic_model_config,
            seed=1,
            config=experiment_config,
        )
        experiment.run()
