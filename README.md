# Multi-Robot-Cluster-Assignment

# Installation

Install requirements

```shell

sudo apt install python3-venv wandb swig

```

create python3 venv

```shell
git clone --recursive git@gitlab.inria.fr:amarino/multi-robot-cluster-assignment.git
python3 -m venv .env
source .env/bin/activate
python3 -m pip install -r requirements.txt
```

install benchmarl 
```shell
python3 -m pip install -e . 
```