from tensorboard.backend.event_processing.event_accumulator import EventAccumulator
import pandas as pd
import matplotlib.pyplot as plt
import os
import numpy as np

pwd = os.getcwd()
log_dir = os.path.join(pwd, "resources_assignment_logs", "10_4")

name = ["IPPO", "VDN", "MultiResourcesIPPO"]

list_dir = os.listdir(log_dir)

for i in range(len(name)):
    ld = []
    for d in list_dir:
        if name[i] == d[: len(name[i])]:
            ld.append(d)

    if name[i] == "IPPO" or name[i] == "MultiResourcesIPPO":
        time = np.linspace(24000, 1500000, 35)
    else:
        time = np.linspace(1000, 1500000, 1500)

    c = []
    for d in ld:
        event_acc = EventAccumulator(os.path.join(log_dir, d)).Reload()
        tags = event_acc.Tags()["scalars"]
        if tags:
            df = pd.DataFrame(event_acc.Scalars("rollout/ep_rew_mean"))
            if name[i] == "IPPO" or name[i] == "MultiResourcesIPPO":
                df = df["value"][:35].to_numpy()
                if len(df) == 35:
                    c.append(df)
            else:
                df = df["value"][:1500].to_numpy()
                if len(df) == 1500:
                    c.append(df)

    mean_c = np.array(c).mean(axis=0)
    std_c = np.array(c).std(axis=0)

    plt.plot(time, mean_c, label=name[i])
    plt.fill_between(time, mean_c - std_c / 3, mean_c + std_c / 3, alpha=0.3)
plt.grid("True")
plt.savefig("comarison_plot.png")
