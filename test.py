import numpy as np

import coverage_environment.coverage_environment as ce

import torch as th
import os
import random
from PIL import Image

import cvxpy as cp

seed = 2

random.seed(seed)
os.environ["PYTHONHASHSEED"] = str(seed)
np.random.seed(seed)
th.manual_seed(seed)
th.cuda.manual_seed(seed)


eval_env = ce.SimpleCEEnv(
    na=10, destinations=4, resources=2, max_cycles=250, render_mode="rgb_array"
)
eval_env.reset()
rewards = np.zeros(eval_env.Na)
while eval_env.timestep != eval_env.max_cycles:

    cost_matrix = np.full((eval_env.Na, eval_env.dest), np.inf)
    dist = (
        np.linalg.norm(
            eval_env.dest_loc.reshape(-1, 1, 2) - eval_env.agents_loc.reshape(1, -1, 2),
            axis=-1,
        )
        ** 2
    )

    for k, agent in enumerate(eval_env.agents):
        for i in range(eval_env.dest):
            cost_matrix[k, i] = eval_env.calculate_cost(
                np.sum(eval_env.agents_level[k, :] * (1 - eval_env.resource_type)),
                np.sum(eval_env.dests_level[i, :] * (1 - eval_env.resource_type)),
                dist[i, k],
            )

    num_rows, num_cols = cost_matrix.shape

    # Decision variables: binary matrix of size (num_rows, num_cols)
    x = cp.Variable((num_rows, num_cols), boolean=True)

    al_sum = np.sum(eval_env.agents_level[:, :], axis=1)
    dl_sum = np.sum(eval_env.dests_level[:, :], axis=1)

    # Objective: Minimize the total cost
    if np.sum(eval_env.agents_level[:, eval_env.resource_type == 0]) > eval_env.eps:
        objective = cp.Minimize(cp.sum(cp.multiply(cost_matrix, x)))
    else:
        objective = cp.Minimize(cp.norm(dl_sum - x.T @ al_sum, 1))

    # Constraints: Each row is assigned to exactly one column
    row_constraints = [cp.sum(x[i, :]) == 1 for i in range(num_rows)]

    # Constraints: Each column is assigned to exactly one row
    col_constraints = [cp.sum(x[:, j]) >= 1 for j in range(num_cols)]

    # Combine all constraints
    constraints = row_constraints + col_constraints

    # Solve the problem
    problem = cp.Problem(objective, constraints)
    problem.solve()

    x = np.array(x.value)
    assignment = [
        (i, j) for i in range(num_rows) for j in range(num_cols) if x[i, j] > 0.5
    ]

    actions = {
        agent: eval_env.action_space(agent).sample() for agent in eval_env.agents
    }

    for k, i in assignment:
        actions[eval_env.agents[k]] = np.clip(
            1 * (eval_env.dest_loc[i] - eval_env.agents_loc[k]), -1, 1
        )

    observation, reward, termination, truncation, info = eval_env.step(actions)

    rewards += np.array(list(reward.values()))

    img = eval_env.render()
    im = Image.fromarray(img)
    im.save("test.jpeg")
img = eval_env.render()
im = Image.fromarray(img)
im.save("test.jpeg")
eval_env.close()
