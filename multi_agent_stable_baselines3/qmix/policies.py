from typing import Any, Dict, List, Optional, Tuple, Type, Union

import torch as th
from gymnasium import spaces
from torch import nn
import numpy as np

from copy import deepcopy

from stable_baselines3.common.distributions import (
    SquashedDiagGaussianDistribution,
    StateDependentNoiseDistribution,
)
from stable_baselines3.common.policies import BasePolicy
from multi_agent_stable_baselines3.common.policies import ContinuousCritic
from stable_baselines3.common.preprocessing import get_obs_shape
from stable_baselines3.common.torch_layers import (
    get_actor_critic_arch,
)


from multi_agent_stable_baselines3.common.torch_layers import (
    MultiAgentFeaturesExtractor,
    MultiAgentMlpExtractor,
    MultiAgentMLPNetwork,
    GNNNetwork,
)
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.type_aliases import PyTorchObs, Schedule
from multi_agent_stable_baselines3.common.utils import (
    check_graph_nn,
    check_recurrent_nn,
)

# CAP the standard deviation of the actor
LOG_STD_MAX = 2
LOG_STD_MIN = -20


class Actor(BasePolicy):
    """
    Actor network (policy) for SAC.

    :param observation_space: Observation space
    :param action_space: Action space
    :param net_arch: Network architecture
    :param features_extractor: Network to extract features
        (a CNN when using images, a nn.Flatten() layer otherwise)
    :param features_dim: Number of features
    :param activation_fn: Activation function
    :param use_sde: Whether to use State Dependent Exploration or not
    :param log_std_init: Initial value for the log standard deviation
    :param full_std: Whether to use (n_features x n_actions) parameters
        for the std instead of only (n_features,) when using gSDE.
    :param use_expln: Use ``expln()`` function instead of ``exp()`` when using gSDE to ensure
        a positive standard deviation (cf paper). It allows to keep variance
        above zero and prevent it from growing too fast. In practice, ``exp()`` is usually enough.
    :param clip_mean: Clip the mean output when using gSDE to avoid numerical instability.
    :param normalize_images: Whether to normalize images or not,
         dividing by 255.0 (True by default)
    """

    action_space: spaces.Dict

    def __init__(
        self,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        net_model_kwargs: Optional[Dict[str, Any]],
        net_model: Type[nn.Module],
        features_extractor: nn.Module,
        features_dim: int,
        shared_parameters: bool = False,
        activation_fn: Type[nn.Module] = nn.ReLU,
        use_sde: bool = False,
        log_std_init: float = -3,
        full_std: bool = True,
        use_expln: bool = False,
        clip_mean: float = 2.0,
        normalize_images: bool = True,
    ):
        self.shared_parameters = shared_parameters
        self.agents = list(action_space.keys())
        super().__init__(
            observation_space,
            action_space[self.agents[0]],
            features_extractor=features_extractor,
            normalize_images=normalize_images,
            squash_output=True,
        )

        # Save arguments to re-create object at loading

        self.use_sde = use_sde
        self.sde_features_extractor = None
        self.net_model_kwargs = net_model_kwargs
        self.features_dim = features_dim
        self.activation_fn = activation_fn
        self.log_std_init = log_std_init
        self.use_expln = use_expln
        self.full_std = full_std
        self.clip_mean = clip_mean
        self.net_model = net_model
        net_model_kwargs["activation_fn"] = activation_fn
        net_model_kwargs["agents"] = self.agents
        net_model_kwargs["shared_parameters"] = shared_parameters
        net_model_kwargs["feature_dim"] = features_dim

        action_dim = get_obs_shape(action_space)[self.agents[0]][0]
        self.latent_pi = net_model(**net_model_kwargs)
        last_layer_dim = self.latent_pi.last_layer_dim

        if self.use_sde:
            self.action_dist = StateDependentNoiseDistribution(
                action_dim,
                full_std=full_std,
                use_expln=use_expln,
                learn_features=True,
                squash_output=True,
            )
            self.mu, self.log_std = self.action_dist.proba_distribution_net(
                latent_dim=last_layer_dim,
                latent_sde_dim=last_layer_dim,
                log_std_init=log_std_init,
            )
            # Avoid numerical issues by limiting the mean of the Gaussian
            # to be in [-clip_mean, clip_mean]
            if clip_mean > 0.0:
                self.mu = nn.Sequential(
                    self.mu, nn.Hardtanh(min_val=-clip_mean, max_val=clip_mean)
                )
        else:
            self.action_dist = SquashedDiagGaussianDistribution(action_dim)  # type: ignore[assignment]
            if self.shared_parameters:
                self.mu = nn.Linear(last_layer_dim, action_dim)
                self.log_std = nn.Linear(last_layer_dim, action_dim)
            else:
                self.mu = nn.ModuleDict(
                    {
                        agent: nn.Linear(last_layer_dim, action_dim)
                        for agent in self.agents
                    }
                )
                self.log_std = nn.ModuleDict({agent: nn.Linear(last_layer_dim, action_dim) for agent in self.agents})  # type: ignore[assignment]

    def _get_constructor_parameters(self) -> Dict[str, Any]:
        data = super()._get_constructor_parameters()

        data.update(
            dict(
                net_arch=self.net_arch,
                features_dim=self.features_dim,
                activation_fn=self.activation_fn,
                use_sde=self.use_sde,
                log_std_init=self.log_std_init,
                full_std=self.full_std,
                use_expln=self.use_expln,
                features_extractor=self.features_extractor,
                clip_mean=self.clip_mean,
            )
        )
        return data

    def get_std(self) -> th.Tensor:
        """
        Retrieve the standard deviation of the action distribution.
        Only useful when using gSDE.
        It corresponds to ``th.exp(log_std)`` in the normal case,
        but is slightly different when using ``expln`` function
        (cf StateDependentNoiseDistribution doc).

        :return:
        """
        msg = "get_std() is only available when using gSDE"
        assert isinstance(self.action_dist, StateDependentNoiseDistribution), msg
        return self.action_dist.get_std(self.log_std)

    def reset_noise(self, batch_size: int = 1) -> None:
        """
        Sample new weights for the exploration matrix, when using gSDE.

        :param batch_size:
        """
        msg = "reset_noise() is only available when using gSDE"
        assert isinstance(self.action_dist, StateDependentNoiseDistribution), msg
        self.action_dist.sample_weights(self.log_std, batch_size=batch_size)

    def extract_features(  # type: ignore[override]
        self,
        obs: PyTorchObs,
        features_extractor: Optional[BaseFeaturesExtractor] = None,
    ) -> Union[th.Tensor, Tuple[th.Tensor, th.Tensor]]:
        """
        Preprocess the observation if needed and extract features.

        :param obs: Observation
        :param features_extractor: The features extractor to use. If None, then ``self.features_extractor`` is used.
        :return: The extracted features. If features extractor is not shared, returns a tuple with the
            features for the actor and the features for the critic.
        """
        return features_extractor(obs)

    def get_action_dist_params(
        self,
        obs: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
    ) -> Tuple[th.Tensor, th.Tensor, Dict[str, th.Tensor]]:
        """
        Get the parameters for the action distribution.

        :param obs:
        :return:
            Mean, standard deviation and optional keyword arguments.
        """
        features = self.extract_features(deepcopy(obs), self.features_extractor)

        if check_recurrent_nn(self.net_model):
            latent_pi, state = self.latent_pi(
                features,
                state,
                episode_start=episode_starts,
            )
        else:
            latent_pi = self.latent_pi(features)

        if self.shared_parameters:
            mean_actions = {agent: self.mu(latent_pi[agent]) for agent in self.agents}
        else:
            mean_actions = {
                agent: self.mu[agent](latent_pi[agent]) for agent in self.agents
            }

        if self.use_sde:
            return mean_actions, self.log_std, dict(latent_sde=latent_pi), state
        # Unstructured exploration (Original implementation)
        if self.shared_parameters:
            # Original Implementation to cap the standard deviation
            log_std = {
                agent: th.clamp(
                    self.log_std(latent_pi[agent]), LOG_STD_MIN, LOG_STD_MAX
                )
                for agent in self.agents
            }
        else:
            # Original Implementation to cap the standard deviation
            log_std = {
                agent: th.clamp(
                    self.log_std[agent](latent_pi[agent]), LOG_STD_MIN, LOG_STD_MAX
                )
                for agent in self.agents
            }
        return mean_actions, log_std, {}, state

    def forward(
        self,
        obs: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
        deterministic: bool = False,
    ) -> th.Tensor:

        mean_actions, log_std, kwargs, state = self.get_action_dist_params(
            obs, state, episode_starts
        )
        # Note: the action is squashed
        return {
            agent: self.action_dist.actions_from_params(
                mean_actions[agent],
                log_std[agent],
                deterministic=deterministic,
                **kwargs,
            )
            for agent in self.agents
        }, state

    def action_log_prob(
        self,
        obs: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
    ) -> Tuple[th.Tensor, th.Tensor]:
        mean_actions, log_std, kwargs, state = self.get_action_dist_params(
            obs, state, episode_starts
        )
        # return action and associated log prob

        action_plus_log_prob = {
            agent: self.action_dist.log_prob_from_params(
                mean_actions[agent], log_std[agent], **kwargs
            )
            for agent in self.agents
        }
        actions = th.stack(
            [action_plus_log_prob[agent][0] for agent in self.agents], dim=1
        )
        log_prob = th.stack(
            [action_plus_log_prob[agent][1] for agent in self.agents], dim=1
        )
        return actions, log_prob, state

    def _predict(
        self,
        observation: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
        deterministic: bool = False,
    ) -> th.Tensor:
        return self(observation, state, episode_starts, deterministic)

    def predict(
        self,
        observation: Union[np.ndarray, Dict[str, np.ndarray]],
        state: Optional[Tuple[np.ndarray, ...]] = None,
        episode_start: Optional[np.ndarray] = None,
        deterministic: bool = False,
        infos: Optional[List[Dict[str, Any]]] = None,
    ) -> Tuple[Dict[str, np.ndarray], Optional[Tuple[np.ndarray, ...]]]:
        """
        Get the policy action from an observation (and optional hidden state).
        Includes sugar-coating to handle different observations (e.g. normalizing images).

        :param observation: the input observation
        :param state: The last hidden states (can be None, used in recurrent policies)
        :param episode_start: The last masks (can be None, used in recurrent policies)
            this correspond to beginning of episodes,
            where the hidden states of the RNN must be reset.
        :param deterministic: Whether or not to return deterministic actions.
        :return: the model's action and the next hidden state
            (used in recurrent policies)
        """
        # Switch to eval mode (this affects batch norm / dropout)
        self.set_training_mode(False)

        # Check for common mistake that the user does not mix Gym/VecEnv API
        # Tuple obs are not supported by SB3, so we can safely do that check
        if not isinstance(observation, dict):
            raise ValueError(
                "You have passed a tuple to the predict() function instead of a Numpy array or a Dict. "
                "You are probably mixing Gym API with SB3 VecEnv API: `obs, info = env.reset()` (Gym) "
                "vs `obs = vec_env.reset()` (SB3 VecEnv). "
                "See related issue https://github.com/DLR-RM/stable-baselines3/issues/1694 "
                "and documentation for more information: https://stable-baselines3.readthedocs.io/en/master/guide/vec_envs.html#vecenv-api-vs-gym-api"
            )

        obs_tensor, vectorized_env = self.obs_to_tensor(observation)
        if check_graph_nn(self.net_model):
            # Graph NNs need to add position information to form the graph
            if (
                infos is None
                or not infos
                or any([info.get("pos") is None for info in infos])
            ):
                raise ValueError(
                    "Graph Neural Networks need to receive the position information in the info dict. "
                )
            pos = []
            for info in infos:
                pos = pos + [info["pos"]]

            pos = np.stack(pos, axis=0)
            obs_tensor["pos"] = th.from_numpy(pos).to(obs_tensor[self.agents[0]].device)

        with th.no_grad():
            actions, states = self._predict(
                obs_tensor, state, episode_start, deterministic=deterministic
            )
        # Convert to numpy, and reshape to the original action shape
        actions = actions.cpu().numpy().reshape((-1, *self.action_space.shape))  # type: ignore[misc, assignment]

        if isinstance(self.action_space, spaces.Box):
            if self.squash_output:
                # Rescale to proper domain when using squashing
                actions = self.unscale_action(actions)  # type: ignore[assignment, arg-type]
            else:
                # Actions could be on arbitrary scale, so clip the actions to avoid
                # out of bound error (e.g. if sampling from a Gaussian distribution)
                actions = np.clip(actions, self.action_space.low, self.action_space.high)  # type: ignore[assignment, arg-type]

        actions = actions.reshape(-1, len(self.agents), *self.action_space.shape)
        actions = {agent: actions[:, i, :] for i, agent in enumerate(self.agents)}
        # Remove batch dimension if needed
        if not vectorized_env:
            for action in actions:
                assert isinstance(actions[action], np.ndarray)
                actions[action] = actions[action].squeeze(axis=0)

        return actions, states  # type: ignore[return-value]


class QMIXCritic(ContinuousCritic):

    def __init__(
        self,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        net_model: Type[nn.Module],
        net_model_kwargs: Optional[Dict[str, Any]],
        features_extractor: MultiAgentFeaturesExtractor,
        features_dim: int,
        activation_fn: Type[nn.Module] = nn.ReLU,
        normalize_images: bool = True,
        n_critics: int = 2,
        share_features_extractor: bool = True,
        shared_parameters: bool = False,
        centralized_critics: bool = False,
    ):

        super().__init__(
            observation_space=observation_space,
            action_space=action_space,
            net_model=net_model,
            net_model_kwargs=net_model_kwargs,
            features_extractor=features_extractor,
            features_dim=features_dim,
            activation_fn=activation_fn,
            normalize_images=normalize_images,
            n_critics=n_critics,
            share_features_extractor=share_features_extractor,
            shared_parameters=shared_parameters,
            centralized_critics=centralized_critics,
        )

        self.mix_q = nn.Sequential(nn.Linear(self.na, 32), nn.ReLU(), nn.Linear(32, 1))
        self.add_module(f"q_mix", self.mix_q)

    def mix(self, agent_qs: th.Tensor) -> th.Tensor:
        """
        Compute the mixing of the Q-values.

        :param agent_qs: The Q-values of each agent
        :param obs: The observation
        :return: The global Q-value
        """

        qs = agent_qs.squeeze(-1)
        q = self.mix_q(qs)

        return q


class QMIXPolicy(BasePolicy):
    """
    Policy class (with both actor and critic) for QMIX.

    :param observation_space: Observation space
    :param action_space: Action space
    :param lr_schedule: Learning rate schedule (could be constant)
    :param net_arch: The specification of the policy and value networks.
    :param activation_fn: Activation function
    :param use_sde: Whether to use State Dependent Exploration or not
    :param log_std_init: Initial value for the log standard deviation
    :param use_expln: Use ``expln()`` function instead of ``exp()`` when using gSDE to ensure
        a positive standard deviation (cf paper). It allows to keep variance
        above zero and prevent it from growing too fast. In practice, ``exp()`` is usually enough.
    :param clip_mean: Clip the mean output when using gSDE to avoid numerical instability.
    :param features_extractor_class: Features extractor to use.
    :param features_extractor_kwargs: Keyword arguments
        to pass to the features extractor.
    :param normalize_images: Whether to normalize images or not,
         dividing by 255.0 (True by default)
    :param optimizer_class: The optimizer to use,
        ``th.optim.Adam`` by default
    :param optimizer_kwargs: Additional keyword arguments,
        excluding the learning rate, to pass to the optimizer
    :param n_critics: Number of critic networks to create.
    :param share_features_extractor: Whether to share or not the features extractor
        between the actor and the critic (this saves computation time)
    """

    actor: Actor
    actor_target: Actor
    critic: QMIXCritic
    critic_target: QMIXCritic

    def __init__(
        self,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        lr_schedule: Schedule,
        net_model: Optional[Dict[str, Type[nn.Module]]] = MultiAgentMLPNetwork,
        net_arch: Optional[Union[List[int], Dict[str, List[int]]]] = None,
        centralized_critics: bool = False,
        share_parameters_actor: bool = False,
        share_parameters_critic: bool = False,
        activation_fn: Type[nn.Module] = nn.ReLU,
        use_sde: bool = False,
        log_std_init: float = -3,
        use_expln: bool = False,
        clip_mean: float = 2.0,
        features_extractor_class: Type[
            BaseFeaturesExtractor
        ] = MultiAgentFeaturesExtractor,
        features_extractor_kwargs: Optional[Dict[str, Any]] = None,
        normalize_images: bool = True,
        optimizer_class: Type[th.optim.Optimizer] = th.optim.Adam,
        optimizer_kwargs: Optional[Dict[str, Any]] = None,
        share_features_extractor: bool = False,
        net_model_kwargs: Optional[Dict[str, Any]] = {},
    ):
        features_extractor_class = MultiAgentFeaturesExtractor
        features_extractor_kwargs = {
            "keep_dict": True,
            "use_mlp": True,
            "use_sequence": check_recurrent_nn(net_model),
        }
        self.na = len(action_space.keys())
        self.agents = list(action_space.keys())
        self.share_parameters_actor = share_parameters_actor
        self.share_parameters_critic = share_parameters_critic
        self.centralized_critics = centralized_critics
        n_critics = 2

        super().__init__(
            observation_space,
            action_space[self.agents[0]],
            features_extractor_class,
            features_extractor_kwargs,
            optimizer_class=optimizer_class,
            optimizer_kwargs=optimizer_kwargs,
            squash_output=True,
            normalize_images=normalize_images,
        )

        net_model_arch_actor = dict(net_model_kwargs)
        net_model_arch_critic = dict(net_model_kwargs)

        if net_arch is None:
            critic_arch = None
            actor_arch = None
        else:
            actor_arch, critic_arch = get_actor_critic_arch(net_arch)
            net_model_arch_actor["net_arch"] = actor_arch
            net_model_arch_critic["net_arch"] = critic_arch

        self.net_model = net_model

        self.net_arch = net_arch
        self.activation_fn = activation_fn
        self.net_args = {
            "observation_space": self.observation_space,
            "action_space": action_space,
            "net_model": self.net_model,
            "net_model_kwargs": net_model_arch_actor,
            "activation_fn": self.activation_fn,
            "shared_parameters": self.share_parameters_actor,
            "normalize_images": normalize_images,
        }
        self.actor_kwargs = self.net_args.copy()

        sde_kwargs = {
            "use_sde": use_sde,
            "log_std_init": log_std_init,
            "use_expln": use_expln,
            "clip_mean": clip_mean,
        }
        self.actor_kwargs.update(sde_kwargs)
        self.critic_kwargs = self.net_args.copy()
        self.critic_kwargs.update(
            {
                "n_critics": n_critics,
                "share_features_extractor": share_features_extractor,
                "shared_parameters": self.share_parameters_critic,
                "centralized_critics": centralized_critics,
                "net_model_kwargs": net_model_arch_critic,
            }
        )

        self.share_features_extractor = share_features_extractor

        self._build(lr_schedule)

    def _build(self, lr_schedule: Schedule) -> None:
        self.actor = self.make_actor()
        self.actor_target = self.make_actor(features_extractor=None)
        # Initialize the target to have the same weights as the actor
        self.actor_target.load_state_dict(self.actor.state_dict())
        self.actor.optimizer = self.optimizer_class(
            self.actor.parameters(),
            lr=lr_schedule(1),  # type: ignore[call-arg]
            **self.optimizer_kwargs,
        )

        if self.share_features_extractor:
            self.critic = self.make_critic(
                features_extractor=self.actor.features_extractor
            )
            # Do not optimize the shared features extractor with the critic loss
            # otherwise, there are gradient computation issues
            critic_parameters = [
                param
                for name, param in self.critic.named_parameters()
                if "features_extractor" not in name
            ]
        else:
            # Create a separate features extractor for the critic
            # this requires more memory and computation
            self.critic = self.make_critic(features_extractor=None)
            critic_parameters = list(self.critic.parameters())

        # Critic target should not share the features extractor with critic
        self.critic_target = self.make_critic(features_extractor=None)
        self.critic_target.load_state_dict(self.critic.state_dict())

        self.critic.optimizer = self.optimizer_class(
            critic_parameters,
            lr=lr_schedule(1),  # type: ignore[call-arg]
            **self.optimizer_kwargs,
        )

        # Target networks should always be in eval mode
        self.critic_target.set_training_mode(False)
        self.actor_target.set_training_mode(False)

    def _get_constructor_parameters(self) -> Dict[str, Any]:
        data = super()._get_constructor_parameters()

        data.update(
            dict(
                net_arch=self.net_arch,
                activation_fn=self.net_args["activation_fn"],
                use_sde=self.actor_kwargs["use_sde"],
                log_std_init=self.actor_kwargs["log_std_init"],
                use_expln=self.actor_kwargs["use_expln"],
                clip_mean=self.actor_kwargs["clip_mean"],
                n_critics=self.critic_kwargs["n_critics"],
                lr_schedule=self._dummy_schedule,  # dummy lr schedule, not needed for loading policy alone
                optimizer_class=self.optimizer_class,
                optimizer_kwargs=self.optimizer_kwargs,
                features_extractor_class=self.features_extractor_class,
                features_extractor_kwargs=self.features_extractor_kwargs,
            )
        )
        return data

    def reset_noise(self, batch_size: int = 1) -> None:
        """
        Sample new weights for the exploration matrix, when using gSDE.

        :param batch_size:
        """
        pass

    def get_initial_states(self, n_envs: int) -> List[Tuple[np.ndarray, ...]]:

        return tuple(
            np.zeros((n_envs, self.na, *pi_r), dtype=np.float32)
            for pi_r in self.actor.latent_pi.recurrent_layers
        )

    def make_actor(
        self, features_extractor: Optional[MultiAgentFeaturesExtractor] = None
    ) -> Actor:
        if features_extractor is None:
            features_extractor = self.make_features_extractor()
        features_dim = features_extractor.features_dim
        self.actor_kwargs.update(
            {"features_extractor": features_extractor, "features_dim": features_dim}
        )
        return Actor(**self.actor_kwargs).to(self.device)

    def make_critic(
        self, features_extractor: Optional[MultiAgentFeaturesExtractor] = None
    ) -> QMIXCritic:
        if features_extractor is None:
            features_extractor = self.make_features_extractor()
        features_dim = features_extractor.features_dim
        self.critic_kwargs.update(
            {"features_extractor": features_extractor, "features_dim": features_dim}
        )
        return QMIXCritic(**self.critic_kwargs).to(self.device)

    def forward(self, obs: PyTorchObs, deterministic: bool = False) -> th.Tensor:
        return self._predict(obs, deterministic=deterministic)

    def _predict(
        self,
        observation: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
        deterministic: bool = False,
    ) -> th.Tensor:
        return self.actor(observation, state, episode_starts, deterministic)

    def predict(
        self,
        observation: Union[np.ndarray, Dict[str, np.ndarray]],
        state: Optional[Tuple[np.ndarray, ...]] = None,
        episode_start: Optional[np.ndarray] = None,
        deterministic: bool = False,
        infos: Optional[List[Dict[str, Any]]] = None,
    ) -> Tuple[Dict[str, np.ndarray], Optional[Tuple[np.ndarray, ...]]]:
        """
        Get the policy action from an observation (and optional hidden state).
        Includes sugar-coating to handle different observations (e.g. normalizing images).

        :param observation: the input observation
        :param state: The last hidden states (can be None, used in recurrent policies)
        :param episode_start: The last masks (can be None, used in recurrent policies)
            this correspond to beginning of episodes,
            where the hidden states of the RNN must be reset.
        :param deterministic: Whether or not to return deterministic actions.
        :return: the model's action and the next hidden state
            (used in recurrent policies)
        """
        # Switch to eval mode (this affects batch norm / dropout)
        self.set_training_mode(False)

        # Check for common mistake that the user does not mix Gym/VecEnv API
        # Tuple obs are not supported by SB3, so we can safely do that check
        if not isinstance(observation, dict):
            raise ValueError(
                "You have passed a tuple to the predict() function instead of a Numpy array or a Dict. "
                "You are probably mixing Gym API with SB3 VecEnv API: `obs, info = env.reset()` (Gym) "
                "vs `obs = vec_env.reset()` (SB3 VecEnv). "
                "See related issue https://github.com/DLR-RM/stable-baselines3/issues/1694 "
                "and documentation for more information: https://stable-baselines3.readthedocs.io/en/master/guide/vec_envs.html#vecenv-api-vs-gym-api"
            )

        obs_tensor, vectorized_env = self.obs_to_tensor(observation)
        if check_graph_nn(self.net_model):
            # Graph NNs need to add position information to form the graph
            if (
                infos is None
                or not infos
                or any([info.get("pos") is None for info in infos])
            ):
                raise ValueError(
                    "Graph Neural Networks need to receive the position information in the info dict. "
                )
            pos = []
            for info in infos:
                pos = pos + [info["pos"]]

            pos = np.stack(pos, axis=0)
            obs_tensor["pos"] = th.from_numpy(pos).to(obs_tensor[self.agents[0]].device)

        with th.no_grad():
            states = None
            if check_recurrent_nn(self.net_model):
                if state is None:
                    # Initialize hidden states to zeros
                    state = self.get_initial_states(obs_tensor[self.agents[0]].shape[0])
                states = tuple(
                    (
                        th.tensor(s, dtype=th.float32, device=self.device)
                        if isinstance(s, np.ndarray)
                        else s
                    )
                    for s in state
                )
            episode_starts = (
                th.tensor(episode_start, dtype=th.float32, device=self.device)
                if isinstance(episode_start, np.ndarray)
                else episode_start
            )
            actions, states = self._predict(
                obs_tensor, states, episode_starts, deterministic=deterministic
            )
            if check_recurrent_nn(self.net_model):
                states = tuple(s.cpu().numpy() for s in states)

        # Convert to numpy, and reshape to the original action shape
        actions = {agent: actions[agent].cpu().numpy().reshape((-1, *self.action_space.shape)) for agent in self.agents}  # type: ignore[misc, assignment]

        if isinstance(self.action_space, spaces.Box):
            if self.squash_output:
                # Rescale to proper domain when using squashing
                actions = {agent: self.unscale_action(actions[agent]) for agent in self.agents}  # type: ignore[assignment, arg-type]
            else:
                # Actions could be on arbitrary scale, so clip the actions to avoid
                # out of bound error (e.g. if sampling from a Gaussian distribution)
                actions = {agent: np.clip(actions[agent], self.action_space.low, self.action_space.high) for agent in self.agents}  # type: ignore[assignment, arg-type]

        # Remove batch dimension if needed
        if not vectorized_env:
            for action in actions:
                assert isinstance(actions[action], np.ndarray)
                actions[action] = actions[action].squeeze(axis=0)

        return actions, states  # type: ignore[return-value]

    def set_training_mode(self, mode: bool) -> None:
        """
        Put the policy in either training or evaluation mode.

        This affects certain modules, such as batch normalisation and dropout.

        :param mode: if true, set to training mode, else set to evaluation mode
        """
        self.actor.set_training_mode(mode)
        self.critic.set_training_mode(mode)
        self.training = mode


MlpPolicy = QMIXPolicy
