import torch
import torch.nn as nn
from typing import Optional, Tuple

# import torchvision.transforms as transforms

import torch.nn.init as init
from torch import Tensor
import torch.nn.functional as F
import math

from collections import OrderedDict

from torch.nn import Sequential, Linear, ReLU, LeakyReLU, Softmax


class LGTC(nn.Module):
    """
    LGTC Layer
    To use the LGTC layer, there must be an implementation counterpart
    passing only t (time) and x (state) to the forward method to apply
    odeint to the state x
    """

    class LGTCimp(nn.Module):
        def __init__(
            self, H: int, G: int, F: int, attention: bool, use_odeint: bool
        ) -> None:
            super(LGTC.LGTCimp, self).__init__()

            self.H = H
            self.F = F
            self.G = G
            self.attention = attention
            self.use_odeint = use_odeint

            self.I_com_x = 0  # torch.randint(4, 8, (1,))
            self.I_com_u = 0  # torch.randint(4, 8, (1,))
            self.weight_A = nn.parameter.Parameter(Tensor(F, F, H))
            self.weight_Ax = nn.parameter.Parameter(Tensor(F, F, H - 1))
            self.weight_B = nn.parameter.Parameter(Tensor(F, G, H))
            self.weight_Bu = nn.parameter.Parameter(Tensor(F, G, H))

            self.bias_f = nn.parameter.Parameter(torch.rand(F))

            self.bias_fx = nn.parameter.Parameter(torch.rand(F))
            self.bias_fu = nn.parameter.Parameter(torch.rand(F))

            # attention
            self.a_l1 = Linear(G * 2, 50)
            self.a_l2 = Linear(50, 50)
            self.a_l3 = Linear(50, 1)

            self.leaky_relu = LeakyReLU()
            self.softmax = Softmax(dim=2)
            self.sigmoid = nn.Sigmoid()
            self.softplus = nn.Softplus(beta=1e4)

            # init parameters
            self.initialize_parameters()

        def initialize_parameters(self) -> None:
            """initialize_parameters

            Init layers weights and biases
            """

            for name, param in self.named_parameters():
                if param.requires_grad:
                    if len(param.shape) != 1:
                        torch.nn.init.orthogonal_(param, gain=0.1)

            for layer in self.modules():
                if isinstance(layer, nn.Linear):
                    init.xavier_normal_(layer.weight.data)
                    if layer.bias is not None:
                        init.normal_(layer.bias.data)

        def generate_weight(self, F, G):
            I = []
            weight = []
            for _ in range(F):
                # bin = Binomial(G-1, torch.rand(1)*torch.ones(G-1))
                # I = I + [torch.unique(bin.sample(),dim=0).int()]
                I = I + [torch.arange(0, G)]
                w = torch.nn.parameter.Parameter(Tensor(I[-1].size()))
                torch.nn.init.constant_(w, val=0.1)
                weight = weight + [w]
            return (I, torch.nn.ParameterList(weight))

        def attention_weight(self, data: Tensor, L: Tensor):
            batch = L.shape[0]
            N = L.shape[1]

            Lt = (L == 0).int()
            Ln = (L != 0).int()
            Ln = Ln.reshape(batch, N, N, 1)
            L = L.reshape(batch, N, N, 1)

            data_N = data.reshape(batch, 1, N, data.shape[2])
            data_N = data_N.repeat(1, N, 1, 1)

            data = data.reshape(batch, N, 1, data.shape[2])
            data = data.repeat(1, 1, N, 1)

            data_N = Ln * data_N
            data = Ln * data

            dl = torch.cat([data_N, data], dim=3)
            dl = torch.flatten(dl, start_dim=0, end_dim=2)

            dl = self.leaky_relu(self.a_l1(dl))
            dl = self.leaky_relu(self.a_l2(dl))
            dl = self.a_l3(dl)

            dl = torch.unflatten(dl, 0, (batch, N, N))
            dl = dl * L
            dl = dl.reshape(batch, N, N)

            dl = torch.exp(dl)
            dl = dl - Lt
            dl = dl / torch.sum(dl, dim=2, keepdim=True)
            L = dl

            return L

        def graph_filter_step(
            self,
            x: Tuple[Tensor, Tensor],
            L: Tensor,
            i: int,
            gate: Tuple[Tensor, Tensor],
        ) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
            """This method computes the communicated data and build up the filters components i
                of length H. The components are summed directly to the previous components 0...i-1
                the output of the graph filter is of dimension  [batch,F,Nr]

            Args:
                x Tuple(Tensor, Tensor) : input data, hidden state ([batch, Nr, G],[batch, Nr ,F])
                L (torch.Tensor): Laplacian, [batch, Nr , Nr]
                i (int): filter tap
                data_com Optional(Tuple(Tensor,Tensor)): communicated data and hidden state

            Returns:
                Tuple(torch.Tensor, torch.Tensor) : data, hidden after communication step; i=0 they are equal to the input
            """

            data, hidden = x
            fx, fu, fsx, fsu = gate

            if i > 0:
                data_com, hidden_com = (
                    data[:, :, -self.I_com_u :],
                    hidden[:, :, -self.I_com_x :],
                )

                if i == 1 and self.attention:
                    L = self.attention_weight(data, L)
                data_com = torch.matmul(L, data_com)
                hidden_com = torch.matmul(L, hidden_com)

                data = torch.concat([data[:, :, : -self.I_com_u], data_com], dim=2)
                hidden = torch.concat(
                    [hidden[:, :, : -self.I_com_x], hidden_com], dim=2
                )

                WAX = torch.relu(self.weight_Ax[:, :, i - 1].mT)
                fsx = fsx + torch.matmul(hidden, WAX)

            fx = fx + torch.matmul(hidden, self.weight_A[:, :, i].mT)
            fu = fu + torch.matmul(data, self.weight_B[:, :, i].mT)
            fsu = fsu + torch.matmul(data, self.weight_Bu[:, :, i].mT)

            if i == 0:
                return (data, hidden), (fx, fu, fsx, fsu), L
            else:
                return (data, hidden), (fx, fu, fsx, fsu), L

        def output_cell(self, hidden: Tensor) -> Tensor:
            fx, fu, fsx, fsu = self.gate

            fx = self.softplus(fx + self.bias_fx)
            fu = self.softplus(fu + self.bias_fu)
            bias_f = torch.relu(self.bias_f)
            if self.use_odeint:
                out = (
                    -bias_f * hidden
                    - (fx + fu) * hidden
                    - fsx
                    + (fx + fu) * torch.tanh(fsu)
                )
            else:
                numerator = -fsx + (fx + fu) * torch.tanh(fsu)
                denominator = bias_f + (fx + fu)

                out = (numerator, denominator)

            return out

        def cell(self, x: Tensor, L: Tensor, hidden: Optional[Tensor] = None) -> Tensor:
            """
            if hidden is None, it must be initialized with zeros before calling the forward method
            """

            self.gate = (0.0, 0.0, 0.0, 0.0)

            data = x
            state = hidden

            for i in range(self.H):
                (data, state), self.gate, L = self.graph_filter_step(
                    (data, state), L, i, self.gate
                )

            out = self.output_cell(hidden)

            return out

        def reset_parameters(self, device):
            with torch.no_grad():
                self.weight_A = torch.zeros_like(self.weight_A).to(device)
                self.weight_B = torch.zeros_like(self.weight_B).to(device)

            for i in range(self.H):
                for j in range(self.F):
                    self.weight_A[j : j + 1, self.I_x[i][j], i] = getattr(
                        self, f"weight_x_{i}"
                    )[j]
                    self.weight_B[j : j + 1, self.I_u[i][j], i] = getattr(
                        self, f"weight_u_{i}"
                    )[j]

        def forward(self, x, L, hidden):
            return self.cell(x, L, hidden)

    def __init__(
        self, H: int, G: int, F: int, attention: bool = False, use_odeint: bool = False
    ) -> None:
        super().__init__()
        self.gnn = LGTC.LGTCimp(H, G, F, attention, use_odeint)
        self.L = None
        self.data = None
        self.delta_time = 6
        self.odeint = use_odeint

    def reset_parameters(self, device):
        self.gnn.reset_parameters(device)

    def set_L_data(self, L: Tensor, data: Tensor) -> None:
        self.L = L
        self.data = data

    def forward(self, t: Tensor, x: Tensor):
        if self.odeint:
            x = self.gnn.forward(self.data, self.L, x)  # using odeint releasing dx

        else:
            elapsed_time = t / self.delta_time
            for _ in range(self.delta_time):

                numerator, denominator = self.gnn.forward(self.data, self.L, x)
                x = (x + elapsed_time * numerator) / (1 + elapsed_time * denominator)

        return x


class CFGC(nn.Module):
    def __init__(self, H: int, G: int, F: int, attention: bool = False) -> None:
        super(CFGC, self).__init__()
        self.H = H
        self.F = F
        self.G = G
        self.attention = attention

        self.I_com_x = torch.randint(4, 8, (1,))
        self.I_com_u = torch.randint(4, 8, (1,))
        self.weight_A = nn.parameter.Parameter(Tensor(F, F, H))
        self.weight_Ax = nn.parameter.Parameter(Tensor(F, F, H - 1))
        self.weight_B = nn.parameter.Parameter(Tensor(F, G, H))
        self.weight_Bx = nn.parameter.Parameter(Tensor(F, G, H))

        self.bias_f = nn.parameter.Parameter(torch.rand(F))

        self.bias_fx = nn.parameter.Parameter(torch.rand(F))
        self.bias_fu = nn.parameter.Parameter(torch.rand(F))

        # attention
        self.a_l1 = Linear(G * 2, 50)
        self.a_l2 = Linear(50, 50)
        self.a_l3 = Linear(50, 1)

        self.leaky_relu = LeakyReLU()
        self.softmax = Softmax(dim=2)
        self.sigmoid = nn.Sigmoid()
        self.softplus = nn.Softplus()
        self.hard_tanh = nn.Hardtanh()

        # init parameters
        self.initialize_parameters()

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=0.1)

        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                init.xavier_normal_(layer.weight.data)
                if layer.bias is not None:
                    init.normal_(layer.bias.data)

    def attention_weight(self, data: Tensor, L: Tensor):
        batch = L.shape[0]
        N = L.shape[1]

        Lt = (L == 0).int()
        Ln = (L != 0).int()
        Ln = Ln.reshape(batch, N, N, 1)
        L = L.reshape(batch, N, N, 1)

        data_N = data.reshape(batch, 1, N, data.shape[2])
        data_N = data_N.repeat(1, N, 1, 1)

        data = data.reshape(batch, N, 1, data.shape[2])
        data = data.repeat(1, 1, N, 1)

        data_N = Ln * data_N
        data = Ln * data

        dl = torch.cat([data_N, data], dim=3)
        dl = torch.flatten(dl, start_dim=0, end_dim=2)

        dl = self.leaky_relu(self.a_l1(dl))
        dl = self.leaky_relu(self.a_l2(dl))
        dl = self.a_l3(dl)

        dl = torch.unflatten(dl, 0, (batch, N, N))
        dl = dl * L
        dl = dl.reshape(batch, N, N)

        dl = torch.exp(dl)
        dl = dl - Lt
        dl = dl / torch.sum(dl, dim=2, keepdim=True)
        L = dl

        return L

    def graph_filter_step(
        self,
        x: Tuple[Tensor, Tensor],
        L: Tensor,
        i: int,
        gate: Tuple[Tensor, Tensor],
    ) -> Tuple[Tuple[Tensor, Tensor], Tuple[Tensor, Tensor]]:
        """This method computes the communicated data and build up the filters components i
            of length H. The components are summed directly to the previous components 0...i-1
            the output of the graph filter is of dimension  [batch,F,Nr]

        Args:
            x Tuple(Tensor, Tensor) : input data, hidden state ([batch, Nr, G],[batch, Nr ,F])
            L (torch.Tensor): Laplacian, [batch, Nr , Nr]
            i (int): filter tap
            data_com Optional(Tuple(Tensor,Tensor)): communicated data and hidden state

        Returns:
            Tuple(torch.Tensor, torch.Tensor) : data, hidden after communication step; i=0 they are equal to the input
        """

        data, hidden = x
        fx, fu, fu_hat, fs = gate

        if i > 0:
            data_com, hidden_com = (
                data[:, :, -self.I_com_u :],
                hidden[:, :, -self.I_com_x :],
            )

            if i == 1 and self.attention:
                L = self.attention_weight(data, L)
            data_com = torch.matmul(L, data_com)
            hidden_com = torch.matmul(L, hidden_com)

            data = torch.concat([data[:, :, : -self.I_com_u], data_com], dim=2)
            hidden = torch.concat([hidden[:, :, : -self.I_com_x], hidden_com], dim=2)

            WAX = torch.relu(self.weight_Ax[:, :, i - 1].mT)
            fs = fs + torch.matmul(hidden, WAX)

        fx = fx + torch.matmul(hidden, self.weight_A[:, :, i].mT)
        fu = fu + torch.matmul(data, self.weight_B[:, :, i].mT)
        fu_hat = fu_hat + torch.matmul(data, self.weight_Bx[:, :, i].mT)

        if i == 0:
            return (data, hidden), (fx, fu, fu_hat, fs), L
        else:
            return (data, hidden), (fx, fu, fu_hat, fs), L

    def output_cell(self, t: float, hidden: Tensor) -> Tensor:
        fx, fu, fu_hat, fs = self.gate

        fxs = torch.relu(fx + self.bias_fx)
        fus = torch.relu(fu + self.bias_fu)
        bias_f = torch.relu(self.bias_f)
        fu_hat = torch.tanh(fu_hat)

        Dfxs = fxs / (fx + self.bias_fx + 1e-9)
        fs = fs / (hidden + 1e-9)
        int_fb = (fx * fu_hat) / (hidden + 1e-9)

        if torch.any(torch.isnan(int_fb)):
            print("porco")

        q_hat = torch.sigmoid(-(bias_f + fxs + fs + Dfxs * fx) * t + 3.14)
        # out =  torch.mul(hidden,q_hat) +(torch.sigmoid(-(fus+torch.relu(self.bias_fx))*t)-1)*fu_hat
        sigma = torch.sigmoid(fus + fxs)
        out = torch.mul(hidden, q_hat) * sigma + (1 - sigma) * fu_hat

        return out

    def cell(
        self, t: float, x: Tensor, L: Tensor, hidden: Optional[Tensor] = None
    ) -> Tensor:
        """
        if hidden is None, it must be initialized with zeros before calling the forward method
        """

        h = None
        self.gate = (0.0, 0.0, 0.0, 0.0)

        state = hidden
        data = x

        for i in range(self.H):
            (data, state), self.gate, L = self.graph_filter_step(
                (data, state), L, i, self.gate
            )

        out = self.output_cell(t, hidden)

        return out

    def forward(self, t: float, x: Tensor, L: Tensor, hidden: Tensor):
        return self.cell(t, x, L, hidden)


class GraphLinear(nn.Module):
    def __init__(self, G: int, F: int, bias: bool = True) -> None:
        super(GraphLinear, self).__init__()
        self.use_bias = bias
        self.linear = nn.parameter.Parameter(torch.Tensor(G, F))
        if self.use_bias:
            self.bias = nn.parameter.Parameter(torch.Tensor(F))

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """
        stdv = 1.0 / math.sqrt(self.H)
        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    def forward(self, x) -> Tensor:
        x = torch.matmul(x, self.linear) + self.bias
        return x


class GraphLayer(nn.Module):
    def __init__(
        self, H, G, F, bias=True, direct_input=False, attention=False, autoencoder=False
    ) -> None:
        super(GraphLayer, self).__init__()

        self.G = G
        self.F = F
        self.H = H
        self.direct_input = direct_input
        self.attention = attention
        self.autoencoder = autoencoder
        self.use_bias = bias
        self.weight = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.encoding_size = 5

        # attention
        if self.autoencoder:
            self.a_l1 = Linear(2 * self.encoding_size, 50)
        else:
            self.a_l1 = Linear(G * 2, 50)
        self.a_l2 = Linear(50, 50)
        self.a_l3 = Linear(50, 1)

        # autoencoder
        self.ae_depth = 4
        self.e_input = nn.ModuleList(
            [
                GraphLinear(G, 128),
                GraphLinear(128, 64),
                GraphLinear(64, 32),
                GraphLinear(32, self.encoding_size),
            ]
        )
        self.d_input = nn.ModuleList(
            [
                GraphLinear(self.encoding_size, 32),
                GraphLinear(32, 64),
                GraphLinear(64, 128),
                GraphLinear(128, G),
            ]
        )

        self.softmax = Softmax(dim=2)
        self.leaky_relu = LeakyReLU()
        self.bias = nn.parameter.Parameter(torch.Tensor(F))

        self.initialize_parameters()

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """
        stdv = 1.0 / math.sqrt(self.H)
        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=1)
            if "bias" in name:
                param.data.fill_(0)

    @staticmethod
    def reshape(x, x_com, w):
        """reshape

        reshape method to shape data before aggregation step

        Args:
            x (torch.Tensor): robot data size [batch x k x 1]
            x_com (torch.Tensor): neighbots data size [batch x k x N]

        Returns:
            torch.Tensor: tensor of size [batch x k x N]
        """
        return w(x - x_com)

    @staticmethod
    def combine(x):
        """combine function

        Args:
            x (torch.Tensor): data to combine size batch_size x k x N

        Returns:
            torch.Tensor: tensor of size batch_size x k x 1
        """
        return torch.sum(x, 2)

    def consensus(self, x: Tensor, x_com: Tensor):
        """consensus function

        Args:
            x (torch.Tensor): local data (batch_size x k)
            x_com (torch.Tensor): communicated data (batch_size x Ni x k)

        Returns:
            torch.Tensor: combined data (batch_size x k)
        """
        x = x.unsqueeze(dim=1)
        x = x.repeat(1, x_com.shape[1], 1)
        w = torch.ones_like(x) / x_com.shape[1]
        if self.attention:
            dl = torch.cat([x, x_com], dim=2)
            dl = torch.flatten(dl, start_dim=0, end_dim=1)
            dl = self.leaky_relu(self.a_l1(dl))
            dl = self.leaky_relu(self.a_l2(dl))
            dl = self.a_l3(dl)
            dl = torch.unflatten(dl, 0, (x.shape[0], x.shape[1]))
            w = torch.softmax(dl, dim=dl.shape[-1])
        x_share = GraphLayer.reshape(x, x_com, w)
        data = GraphLayer.combine(x_share)
        return data

    def attention_weight(self, data: Tensor, L: Tensor):
        batch = L.shape[0]
        N = L.shape[1]
        Lt = (L == 0).int()
        Ln = (L != 0).int()
        Ln = Ln.reshape(batch, N, N, 1)
        L = L.reshape(batch, N, N, 1)

        data_N = data.reshape(batch, 1, N, data.shape[2])
        data_N = data_N.repeat(1, N, 1, 1)

        data = data.reshape(batch, N, 1, data.shape[2])
        data = data.repeat(1, 1, N, 1)

        data_N = Ln * data_N
        data = Ln * data

        dl = torch.cat([data_N, data], dim=3)

        dl = torch.flatten(dl, start_dim=0, end_dim=2)

        dl = self.leaky_relu(self.a_l1(dl))
        dl = self.leaky_relu(self.a_l2(dl))
        dl = self.a_l3(dl)

        dl = torch.unflatten(dl, 0, (batch, N, N))
        dl = dl * L
        dl = dl.reshape(batch, N, N)

        dl = torch.exp(dl)
        dl = dl - Lt
        dl = dl / (torch.sum(dl, dim=2, keepdim=True) + 1e-5)
        L = dl

        return L

    def encoder(self, data):
        for i, ei in enumerate(self.e_input):
            if i == self.ae_depth - 1:
                data = ei(data)
            else:
                data = torch.relu(ei(data))
        return data

    def decoder(self, data):
        for i, di in enumerate(self.d_input):
            if i == self.ae_depth - 1:
                data = di(data)
            else:
                data = torch.relu(di(data))
        return data

    def graph_filter_step(
        self,
        data: Tensor,
        L: Tensor,
        i: int,
        out: Tensor,
        data_com: Optional[Tensor] = None,
    ) -> Tuple[Tensor, Tensor]:
        if i > 0:
            if self.autoencoder:
                data = self.encoder(data)
            if data.size(1) > 1 or L.size(1) == 1:
                if i == 1 and self.attention:
                    L = self.attention_weight(data, L)
                data = torch.matmul(L, data)
            else:
                if data_com is not None:
                    data = GraphLayer.consensus(data, data_com)
                else:
                    raise RuntimeError(f"No communicated data for tap {i}")

            if self.autoencoder:
                data = self.decoder(data)

        out = out + torch.matmul(data, self.weight[:, :, i].mT)

        return data, out, L

    def forward(self, x: Tensor, L: Tensor) -> Tensor:
        out = torch.zeros((x.shape[0], x.shape[1], self.F), device=x.device)

        if self.direct_input:
            start = 0
        else:
            start = 1

        for i in range(start, self.H):
            x, out, L = self.graph_filter_step(x, L, i, out)

        if self.use_bias:
            out = out + self.bias.reshape(1, 1, self.F)

        return out


class GGL(nn.Module):
    def __init__(
        self,
        H: int = 0,
        G: int = 1,
        F: int = 1,
        attention=False,
        a_mlps: int = 3,
        a_mlps_dim: int = 50,
        autoencoder=False,
    ):
        """Graph Gated Layer

        Args:
            H (int): number of aggregaton steps. Defaults to 0.
            F (int): number of output features. Defaults to 1
            G (int): number of input features. Defaults to 1
        """
        super(GGL, self).__init__()

        self.H = H
        self.F = F
        self.G = G
        self.attention = attention
        self.autoencoder = autoencoder

        self.weight_A = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.weight_A_tilde = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B_tilde = nn.parameter.Parameter(torch.Tensor(F, G, H))
        self.weight_A_hat = nn.parameter.Parameter(torch.Tensor(F, F, H))
        self.weight_B_hat = nn.parameter.Parameter(torch.Tensor(F, G, H))

        self.bias_hat = nn.parameter.Parameter(torch.Tensor(F))
        self.bias_tilde = nn.parameter.Parameter(torch.Tensor(F))
        self.bias = nn.parameter.Parameter(torch.Tensor(F))

        # attention
        if self.autoencoder:
            self.weight_att = nn.parameter.Parameter(torch.Tensor(2, 2))
            self.weight_att_h = nn.parameter.Parameter(torch.Tensor(2, 2))
        else:
            self.weight_att = nn.parameter.Parameter(torch.Tensor(G, G))
            self.weight_att_h = nn.parameter.Parameter(torch.Tensor(F, F))

        a_mlp_list = []
        for i in range(a_mlps):
            a_mlp_list.append(
                (
                    "a_l" + str(i),
                    (
                        Linear(a_mlps_dim, 1)
                        if i == (a_mlps - 1)
                        else (
                            Linear(a_mlps_dim, a_mlps_dim)
                            if i != 0
                            else (
                                Linear(4, a_mlps_dim)
                                if self.autoencoder
                                else Linear(G * 2, a_mlps_dim)
                            )
                        )
                    ),
                )
            )
            if i != (a_mlps - 1):
                a_mlp_list.append(("a_relu" + str(i), LeakyReLU()))

        self.attention_mlp = nn.Sequential(OrderedDict(a_mlp_list))

        # autoencoder
        self.ae_depth = 4
        self.e_input = nn.ModuleList(
            [
                GraphLinear(G, 512),
                GraphLinear(512, 128),
                GraphLinear(128, 32),
                GraphLinear(32, 2),
            ]
        )
        self.d_input = nn.ModuleList(
            [
                GraphLinear(2, 32),
                GraphLinear(32, 128),
                GraphLinear(128, 512),
                GraphLinear(512, G),
            ]
        )
        self.e_state = nn.ModuleList(
            [
                GraphLinear(F, 512),
                GraphLinear(512, 128),
                GraphLinear(128, 32),
                GraphLinear(32, 2),
            ]
        )
        self.d_state = nn.ModuleList(
            [
                GraphLinear(2, 32),
                GraphLinear(32, 128),
                GraphLinear(128, 512),
                GraphLinear(512, F),
            ]
        )

        self.leaky_relu = LeakyReLU()
        self.softmax = Softmax(dim=2)

        self.gates = (
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
            torch.zeros((1, 1, self.F)),
        )

        # init parameters
        self.initialize_parameters()

    def initialize_parameters(self) -> None:
        """initialize_parameters

        Init layers weights and biases
        """

        for name, param in self.named_parameters():
            if param.requires_grad:
                if len(param.shape) != 1:
                    torch.nn.init.orthogonal_(param, gain=0.1)
            if "bias" in name:
                param.data.fill_(0)

        for layer in self.modules():
            if isinstance(layer, nn.Linear):
                init.xavier_normal_(layer.weight.data)
                if layer.bias is not None:
                    init.normal_(layer.bias.data)

    def attention_weight(self, data: Tensor, L: Tensor):
        batch = L.shape[0]
        N = L.shape[1]
        G = data.shape[2]
        Lt = (L == 0).int()
        Ln = (L != 0).int()
        Ln = Ln.reshape(batch, N, N, 1)
        L = L.reshape(batch, N, N, 1)

        data_N = data.reshape(batch, 1, N, data.shape[2])
        data_N = data_N.repeat(1, N, 1, 1)

        data = data.reshape(batch, N, 1, data.shape[2])
        data = data.repeat(1, 1, N, 1)

        data_N = Ln * data_N
        data = Ln * data

        dl = torch.cat([data_N, data], dim=3)

        dl = torch.flatten(dl, start_dim=0, end_dim=2)

        dl = self.attention_mlp(dl)

        dl = torch.unflatten(dl, 0, (batch, N, N))
        dl = dl * L
        dl = dl.reshape(batch, N, N)

        dl = torch.exp(dl)
        dl = dl - Lt
        dl = dl / torch.sum(dl, dim=2, keepdim=True)
        L = dl

        return L

    def encoder(self, data, hidden):
        for i in range(self.ae_depth - 1):
            data = torch.relu(self.e_input[i](data))
            hidden = torch.relu(self.e_state[i](hidden))
        return self.e_input[self.ae_depth](data), self.e_state[self.ae_depth](hidden)

    def decoder(self, data, hidden):
        for i in range(self.ae_depth - 1):
            data = torch.relu(self.d_input[i](data))
            hidden = torch.relu(self.d_state[i](hidden))
        return self.d_input[self.ae_depth](data), self.d_state[self.ae_depth](hidden)

    def graph_filter_step(
        self,
        x: Tuple[Tensor, Tensor],
        L: Tensor,
        Lh: Tensor,
        i: int,
        gates: Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor],
        data_com: Optional[Tuple[Tensor, Tensor]] = None,
    ) -> Tuple[
        Tuple[Tensor, Tensor], Tuple[Tensor, Tensor, Tensor, Tensor, Tensor, Tensor]
    ]:
        """This method computes the communicated data and build up the filters components i
            of length H. The components are summed directly to the previous components 0...i-1
            the output of the graph filter is of dimension  [batch,F,Nr]

        Args:
            x Tuple(Tensor, Tensor) : input data, hidden state ([batch, G ,Nr],[batch, G ,Nr])
            L (torch.Tensor): Laplacian, [batch, Nr , Nr]
            i (int): filter tap
            gates Tuple(Tensor,Tensor,Tensor,Tensor,Tensor,Tensor) : gates of dimension [batch, F, Nr]
            data_com Optional(Tuple(Tensor,Tensor)): communicated data and hidden state

        Returns:
            Tuple(torch.Tensor, torch.Tensor) : data, hidden after communication step; i=0 they are equal to the input
            Tuple(Tensor, Tensor, Tensor, Tensor, Tensor, Tensor) : gates filters updates
        """

        data, hidden = x
        qth, qtu, qhh, qhu, hx, hu = gates

        if i > 0:
            if data.size(1) > 1 or L.size(1) == 1:
                if self.autoencoder:
                    data, hidden = self.encoder(data, hidden)
                if i == 1 and self.attention:
                    L = self.attention_weight(data, L)
                    Lh = self.attention_weight(hidden, Lh)
                data = torch.matmul(L, data)
                hidden = torch.matmul(Lh, hidden)
                if self.autoencoder:
                    data, hidden = self.decoder(data, hidden)
            else:
                if (data_com is not None) and data_com[0] and data_com[1]:
                    data = GraphLayer.consensus(data, data_com[0])
                    hidden = GraphLayer.consensus(hidden, data_com[1])
                else:
                    raise RuntimeError(f"No communicated data for tap {i}")

        qth = qth + torch.matmul(hidden, self.weight_A_tilde[:, :, i].mT)
        qtu = qtu + torch.matmul(data, self.weight_B_tilde[:, :, i].mT)
        qhh = qhh + torch.matmul(hidden, self.weight_A_hat[:, :, i].mT)
        qhu = qhu + torch.matmul(data, self.weight_B_hat[:, :, i].mT)
        hx = hx + torch.matmul(hidden, self.weight_A[:, :, i].mT)
        hu = hu + torch.matmul(data, self.weight_B[:, :, i].mT)

        return (data, hidden), (qth, qtu, qhh, qhu, hx, hu), L, Lh

    def input_cell(self, x: Tensor, hidden: Optional[Tensor] = None):
        if hidden is None:
            hidden = torch.zeros((x.shape[0], x.shape[1], self.F), device=x.device)

        self.gates = (
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
            torch.zeros(hidden.size(), device=x.device),
        )

        return hidden

    def output_cell(self):
        qth, qtu, qhh, qhu, hx, hu = self.gates

        q_tilde = torch.sigmoid(qth + qtu + self.bias_tilde.reshape(1, 1, self.F))
        q_hat = torch.sigmoid(qhh + qhu + self.bias_hat.reshape(1, 1, self.F))

        hx = torch.mul(q_hat, hx)
        hu = torch.mul(q_tilde, hu)

        out = torch.tanh(hx + hu + self.bias.reshape(1, 1, self.F))

        return out

    def cell(self, x: Tensor, L: Tensor, hidden: Optional[Tensor] = None) -> Tensor:
        hidden = self.input_cell(x, hidden)
        Lh = L
        for i in range(self.H):
            (x, hidden), self.gates, L, Lh = self.graph_filter_step(
                (x, hidden), L, Lh, i, self.gates
            )

        out = self.output_cell()

        return out

    def forward(
        self,
        x: Tensor,
        L: Tensor,
        hidden: Optional[Tensor] = None,
        episode_start: Optional[Tensor] = None,
    ) -> Tensor:
        """forward

        Args:
            x (torch.Tensor): output of previous aggregation layers

            hidden (torch.Tensor): estimation average

        Returns:
           torch.Tensor: tensor of size batch_size x 1
        """

        if len(x.shape) < 4:
            x = torch.unsqueeze(x, dim=0)
            L = torch.unsqueeze(L, dim=0)
        else:
            x = x.transpose(0, 1)
            L = L.transpose(0, 1)

        if episode_start is not None:
            episode_start = torch.zeros((x.shape[0], x.shape[1], 1, 1), device=x.device)

        results = []
        out = 0.0
        for i, xx in enumerate(x):
            hidden = self.cell(
                x=xx,
                L=L[i, :, :, :],
                hidden=(1 - episode_start[i]).reshape(-1, 1, 1) * hidden,
            )
            results.append(hidden)

        out = torch.stack(results, 0)

        return out
