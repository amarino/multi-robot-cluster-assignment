from typing import Dict, List, Optional, Tuple, Type, Union

import gymnasium as gym
import torch as th
from gymnasium import spaces
from torch import nn

from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.preprocessing import get_flattened_obs_dim, is_image_space
from stable_baselines3.common.type_aliases import TensorDict
from stable_baselines3.common.utils import get_device
from multi_agent_stable_baselines3.GNNlib import *
import torch_geometric
from torch_geometric.utils import to_dense_adj

from copy import deepcopy


class GraphExtractor(nn.Module):

    type = ["gnn"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        centralized_critics: bool = False,
        edge_variable: str = "pos",
    ):
        super(GraphExtractor, self).__init__()
        self.agents = agents
        self.centralized_critics = centralized_critics
        self.feature_dim = feature_dim
        self.edge_variable = edge_variable


class MultiAgentFeaturesExtractor(BaseFeaturesExtractor):
    def __init__(
        self,
        observation_space: spaces.Dict,
        keep_dict: bool = False,
        use_mlp: bool = False,
        use_sequence: bool = False,
    ):
        self.use_mlp = use_mlp
        if use_mlp:
            key = list(observation_space.keys())[0]
            features_dim = sum(observation_space[key].shape)
        else:
            features_dim = sum(
                [sum(observation_space[key].shape) for key in observation_space.keys()]
            )

        super(MultiAgentFeaturesExtractor, self).__init__(
            observation_space, features_dim=features_dim
        )
        self.keep_dict = keep_dict
        self.use_sequence = use_sequence
        self.observation_space = observation_space

    def forward(self, observations: spaces.Dict) -> th.Tensor:
        if not self.keep_dict:
            observations = th.stack(
                list(observations.values()),
                dim=1,
            ).to(self.device)
        if self.use_sequence:
            observations = {
                agent: (
                    observations[agent].unsqueeze(0)
                    if len(observations[agent].shape) < 3
                    else observations[agent]
                )
                for agent in observations.keys()
            }
        return observations


def create_multi_agent_mlp_network(
    layers_dims: list,
    last_layer_dim: int,
    agents: list,
    activation_fn: Type[nn.Module],
    shared_parameters: bool,
    add_final_linear: bool = False,
):
    if shared_parameters:
        net: List[nn.Module] = []
        # Iterate through the policy layers and build the policy net
        for curr_layer_dim in layers_dims:
            net.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net.append(activation_fn())
            last_layer_dim = curr_layer_dim

        if add_final_linear:
            net.append(nn.Linear(last_layer_dim, 1))
            last_layer_dim = 1
        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        return (
            nn.Sequential(*net),
            last_layer_dim,
        )
    else:
        net: Dict[str, nn.Sequential] = {agent: [] for agent in agents}
        first_layer_dim = last_layer_dim
        for agent in agents:
            last_layer_dim = first_layer_dim

            # Iterate through the policy layers and build the policy net
            for curr_layer_dim in layers_dims:
                net[agent].append(nn.Linear(last_layer_dim, curr_layer_dim))
                net[agent].append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net[agent].append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

        # aggregate in the dict all the networks
        return (
            nn.ModuleDict({agent: nn.Sequential(*net[agent]) for agent in agents}),
            last_layer_dim,
        )


class MultiAgentMLPNetwork(nn.Module):

    type = ["mlp"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        device: Union[th.device, str] = "auto",
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameters = shared_parameters

        self.net, last_layer_dim = create_multi_agent_mlp_network(
            net_arch,
            feature_dim * len(agents) if centralized else feature_dim,
            agents,
            activation_fn,
            shared_parameters,
            add_final_linear,
        )
        self.net = self.net.to(device)

        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        centralized: bool = False,
    ) -> Union[TensorDict, th.Tensor]:
        if centralized:
            fxx = th.cat(list(features.values()), dim=1)
            fx = {key: fxx for key in self.agents}
        else:
            fx = features

        if self.shared_parameters:
            fxx = th.stack(list(fx.values()), dim=-2)
            fx = self.net(fxx)
            fx = {agent: fx[:, i, :] for i, agent in enumerate(self.agents)}
        else:
            fx = {agent: self.net[agent](fx[agent]) for agent in self.agents}
            # fx = th.cat(list(fx.values()), dim=0)
        return fx


class MultiAgentMlpExtractor(nn.Module):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["mlp"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameter_actor = shared_parameter_actor
        self.shared_parameter_critic = shared_parameter_critic
        self.centralized_critics = centralized_critics

        last_layer_dim_pi = feature_dim

        # save dimensions of layers in policy and value nets
        if isinstance(net_arch, dict):
            # Note: if key is not specified, assume linear network
            pi_layers_dims = net_arch.get("pi", [])  # Layer sizes of the policy network
            vf_layers_dims = net_arch.get("vf", [])  # Layer sizes of the value network
        else:
            pi_layers_dims = vf_layers_dims = net_arch

        # create policy network
        self.policy_net = MultiAgentMLPNetwork(
            feature_dim=last_layer_dim_pi,
            agents=agents,
            net_arch=pi_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_actor,
            device=device,
        )

        # create policy network
        self.value_net = MultiAgentMLPNetwork(
            feature_dim=feature_dim,
            agents=agents,
            net_arch=vf_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_critic,
            device=device,
            centralized=centralized_critics,
        )

        # Save dim, used to create the distributions
        self.latent_dim_pi = self.policy_net.last_layer_dim
        self.latent_dim_vf = self.value_net.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy_net.agents = agents
        self.value_net.agents = agents

    def forward(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(features), self.forward_critic(features)

    def forward_actor(self, features: Union[TensorDict, th.Tensor]) -> Union[TensorDict, th.Tensor]:
        return self.policy_net(features)

    def forward_critic(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        return self.value_net(features, self.centralized_critics)


class MultiAgentLSTMNetwork(nn.Module):

    type = ["recurrent"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        shared_parameters: bool = False,
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        device: Union[th.device, str] = "auto",
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameters = shared_parameters

        last_layer_dim = feature_dim * len(agents) if centralized else feature_dim

        if shared_parameters:
            net: nn.ModuleList = []
            # Iterate through the policy layers and build the policy net
            for curr_layer_dim in net_arch:
                net.append(nn.LSTM(last_layer_dim, curr_layer_dim))
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1

            # Create policy network
            # If the list of layers is empty, the network will just act as an Identity module
            self.net = nn.ModuleList(net)
        else:
            net: nn.ModuleDict[str, nn.ModuleList] = {agent: [] for agent in agents}
            first_layer_dim = last_layer_dim
            for agent in agents:
                last_layer_dim = first_layer_dim

                # Iterate through the policy layers and build the policy net
                for curr_layer_dim in net_arch:
                    net[agent].append(nn.LSTM(last_layer_dim, curr_layer_dim))
                    last_layer_dim = curr_layer_dim

                if add_final_linear:
                    net[agent].append(nn.Linear(last_layer_dim, 1))
                    last_layer_dim = 1

            self.net = nn.ModuleDict({agent: nn.ModuleList(net[agent]) for agent in agents})

        self.net = self.net.to(device)

        self.recurrent_layers = tuple((2, na) for na in net_arch)
        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
        centralized: bool = False,
    ) -> Union[TensorDict, th.Tensor]:
        if centralized:
            fxx = th.cat(list(features.values()), dim=1)
            fx = {key: fxx for key in self.agents}
        else:
            fx = features

        fx_c = dict(fx)
        out = {}
        states = tuple(th.zeros_like(state[i]) for i in range(len(self.recurrent_layers)))
        if len(episode_start.shape) < 3:
            episode_start = episode_start.reshape(1, -1, 1)
        sequence_size = episode_start.shape[0]
        batch_size = episode_start.shape[1]
        if self.shared_parameters:
            for i, agent in enumerate(self.agents):
                o = []
                for k in range(sequence_size):
                    ot = fx_c[agent][k : k + 1]
                    for j in range(len(self.recurrent_layers)):
                        ot, (hx, cx) = self.net[j](
                            ot,
                            (
                                (1.0 - episode_start[k : k + 1, :, :]).reshape(1, -1, 1)
                                * state[j][:, i, 0, :].unsqueeze(0),
                                (1.0 - episode_start[k : k + 1, :, :]).reshape(1, -1, 1)
                                * state[j][:, i, 1, :].unsqueeze(0),
                            ),
                        )
                        states[j][:, i, 0, :], states[j][:, i, 1, :] = (
                            hx.squeeze(0),
                            cx.squeeze(0),
                        )

                    o.append(ot)

                out[agent] = th.cat(o).reshape(batch_size * sequence_size, -1)

                if len(self.recurrent_layers) < len(self.net):
                    # optional final linear layer
                    out[agent] = self.net[-1](out[agent])

        else:
            for i, agent in enumerate(self.agents):
                o = []
                for k in range(sequence_size):
                    ot = fx_c[agent][k : k + 1]
                    for j in range(len(self.recurrent_layers)):
                        out[agent], (hx, cx) = self.net[agent][j](
                            ot,
                            (
                                (1.0 - episode_start[k : k + 1, :, :]).reshape(1, -1, 1)
                                * state[j][:, i, 0, :].unsqueeze(0),
                                (1.0 - episode_start[k : k + 1, :, :]).reshape(1, -1, 1)
                                * state[j][:, i, 1, :].unsqueeze(0),
                            ),
                        )
                        states[j][:, i, 0, :], states[j][:, i, 1, :] = (
                            hx.squeeze(0),
                            cx.squeeze(0),
                        )

                out[agent] = th.cat(o).reshape(batch_size * sequence_size, -1)
                if len(self.recurrent_layers) < len(self.net):
                    # optional final linear layer
                    out[agent] = self.net[agent][-1](out[agent])

        return out, states


class MultiAgentLSTMExtractor(nn.Module):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["recurrent"]

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__()
        device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents
        self.shared_parameter_actor = shared_parameter_actor
        self.shared_parameter_critic = shared_parameter_critic
        self.centralized_critics = centralized_critics

        # save dimensions of layers in policy and value nets
        if isinstance(net_arch, dict):
            # Note: if key is not specified, assume linear network
            pi_layers_dims = net_arch.get("pi", [])  # Layer sizes of the policy network
            vf_layers_dims = net_arch.get("vf", [])  # Layer sizes of the value network
        else:
            pi_layers_dims = vf_layers_dims = net_arch

        # create policy network
        self.policy_net = MultiAgentLSTMNetwork(
            feature_dim=feature_dim,
            agents=agents,
            net_arch=pi_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_actor,
            device=device,
        )

        # create policy network
        self.value_net = MultiAgentLSTMNetwork(
            feature_dim=feature_dim,
            agents=agents,
            net_arch=vf_layers_dims,
            activation_fn=activation_fn,
            shared_parameters=shared_parameter_critic,
            device=device,
        )

        # save dim of recurrent layers
        self.recurrent_pi_layers = self.policy_net.recurrent_layers
        self.recurrent_vf_layers = self.value_net.recurrent_layers

        # Save dim, used to create the distributions
        self.latent_dim_pi = self.policy_net.last_layer_dim
        self.latent_dim_vf = self.value_net.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy_net.agents = agents
        self.value_net.agents = agents

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(features, state[0], episode_start), self.forward_critic(
            features, state[1], episode_start
        )

    def forward_actor(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.policy_net(features, state, episode_start)

    def forward_critic(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.value_net(features, state, episode_start, self.centralized_critics)


def create_graph_nn(
    feature_dims_pre: list,
    feature_dims_post: list,
    agents: List[str],
    last_layer_dim: int,
    activation_fn: Type[nn.Module],
    gnn_feature_dim: int = 128,
    add_final_linear: bool = False,
    centralized: bool = False,
):
    net_pre: List[nn.Module] = []
    for curr_layer_dim in feature_dims_pre:
        net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
        net_pre.append(activation_fn())
        last_layer_dim = curr_layer_dim

    gnn1 = GraphLayer(
        2,
        last_layer_dim,
        gnn_feature_dim,
        bias=True,
        direct_input=True,
        attention=False,
        autoencoder=False,
    )
    last_layer_dim = gnn_feature_dim

    net_post: List[nn.Module] = []

    if centralized:
        for curr_layer_dim in feature_dims_post:
            net_post.append(nn.Linear(last_layer_dim * len(agents), curr_layer_dim))
            net_post.append(activation_fn())
            last_layer_dim = curr_layer_dim

        if add_final_linear:
            net_post.append(nn.Linear(last_layer_dim, 1))
            last_layer_dim = 1
    else:
        for curr_layer_dim in feature_dims_post:
            net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_post.append(activation_fn())
            last_layer_dim = curr_layer_dim

        if add_final_linear:
            net_post.append(nn.Linear(last_layer_dim, 1))
            last_layer_dim = 1
    # Create policy network
    # If the list of layers is empty, the network will just act as an Identity module
    net_pre = nn.Sequential(*net_pre)
    net_post = nn.Sequential(*net_post)

    return net_pre, gnn1, net_post, last_layer_dim


class GNNNetwork(nn.Module):

    type = ["gnn"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super(GNNNetwork, self).__init__()

        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents

        pre_dims = []
        post_dims = [512, 512]

        self.net_pre, self.gnn, self.net_post, last_layer_dim = create_graph_nn(
            pre_dims,
            post_dims,
            agents,
            feature_dim,
            activation_fn,
            128,
            add_final_linear,
            centralized=centralized,
        )
        self.net_pre = self.net_pre.to(self.device)
        self.net_post = self.net_post.to(self.device)
        self.gnn = self.gnn.to(self.device)

        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def make_graph(
        self,
        pos: th.Tensor,
    ):
        batch_size = pos.shape[0]
        b = torch.arange(batch_size, device=self.device)
        pos = pos.reshape(-1, pos.shape[-1])
        graphs = torch_geometric.data.Batch()
        graphs.ptr = torch.arange(0, (batch_size + 1) * len(self.agents), len(self.agents))
        graphs.batch = torch.repeat_interleave(b, len(self.agents))
        graphs.pos = pos
        graphs.vel = None
        graphs.edge_attr = None

        graphs.edge_index = torch_geometric.nn.pool.radius_graph(
            graphs.pos, batch=graphs.batch, r=self.edge_radius, loop=False
        )

        graphs = graphs.to(self.device)

        A = to_dense_adj(graphs.edge_index, graphs.batch)
        D = torch.sum(A, axis=-1)
        A = A / (D.unsqueeze(-1) + 1e-4)

        return A

    def forward(self, features: th.Tensor, centralized: bool = False) -> th.Tensor:

        pos = features.pop(self.edge_variable)

        # build Graph
        A = self.make_graph(pos)

        # dimensions batch x n_agents x feature_dim
        fxx = th.stack(list(features.values()), dim=-2)
        batch_size = fxx.shape[0]
        fxx = self.net_pre(fxx)

        fxx = torch.tanh(self.gnn(fxx, A))

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fx = fxx.reshape(batch_size, len(self.agents), -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx


class GNNExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["gnn"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = GNNNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
        )
        self.value = GNNNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.value.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius
        self.value.edge_radius = self.edge_radius

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return self.forward_actor(deepcopy(features)), self.forward_critic(deepcopy(features))

    def forward_actor(self, features: Union[TensorDict, th.Tensor]) -> Union[TensorDict, th.Tensor]:
        fx = self.policy(features)
        return fx

    def forward_critic(
        self, features: Union[TensorDict, th.Tensor]
    ) -> Union[TensorDict, th.Tensor]:
        fx = self.value(features, centralized=self.centralized_critics)
        return fx


class GGNNNetwork(nn.Module):

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameters: bool = False,
        add_final_linear: bool = False,
        centralized: bool = False,
    ) -> None:
        super(GGNNNetwork, self).__init__()

        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")
        self.agents = agents

        pre_dims = [64, 128]
        post_dims = [128, 64]
        gnn_feature_dim = 64

        last_layer_dim = feature_dim
        net_pre: List[nn.Module] = []
        for curr_layer_dim in pre_dims:
            net_pre.append(nn.Linear(last_layer_dim, curr_layer_dim))
            net_pre.append(activation_fn())
            last_layer_dim = curr_layer_dim

        self.gnn = GGL(
            H=2,
            G=last_layer_dim,
            F=gnn_feature_dim,
            attention=False,
            autoencoder=False,
        )
        last_layer_dim = gnn_feature_dim

        net_post: List[nn.Module] = []

        if centralized:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim * len(self.agents), curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        else:
            for curr_layer_dim in post_dims:
                net_post.append(nn.Linear(last_layer_dim, curr_layer_dim))
                net_post.append(activation_fn())
                last_layer_dim = curr_layer_dim

            if add_final_linear:
                net_post.append(nn.Linear(last_layer_dim, 1))
                last_layer_dim = 1
        # Create policy network
        # If the list of layers is empty, the network will just act as an Identity module
        self.net_pre = nn.Sequential(*net_pre)
        self.net_post = nn.Sequential(*net_post)

        self.net_pre = self.net_pre.to(self.device)
        self.net_post = self.net_post.to(self.device)
        self.gnn = self.gnn.to(self.device)

        self.recurrent_layers = tuple([(gnn_feature_dim,)])
        self.last_layer_dim = last_layer_dim
        self.feature_dim = feature_dim

    def make_graph(
        self,
        pos: th.Tensor,
    ):
        batch_size = pos.shape[0]
        b = torch.arange(batch_size, device=self.device)
        pos = pos.reshape(-1, pos.shape[-1])
        graphs = torch_geometric.data.Batch()
        graphs.ptr = torch.arange(0, (batch_size + 1) * len(self.agents), len(self.agents))
        graphs.batch = torch.repeat_interleave(b, len(self.agents))
        graphs.pos = pos
        graphs.vel = None
        graphs.edge_attr = None

        graphs.edge_index = torch_geometric.nn.pool.radius_graph(
            graphs.pos, batch=graphs.batch, r=self.edge_radius, loop=True
        )

        graphs = graphs.to(self.device)

        A = to_dense_adj(graphs.edge_index, graphs.batch)
        D = torch.sum(A, axis=-1)
        A = A / (D.unsqueeze(-1) + 1e-4)

        return A

    def forward(
        self,
        features: th.Tensor,
        state: Tuple = None,
        episode_start: th.Tensor = None,
        centralized: bool = False,
    ) -> th.Tensor:

        pos = features.pop(self.edge_variable)

        # reshape to stack pos sequences in batch
        pos = pos.reshape(-1, pos.shape[-2], pos.shape[-1])

        # build Graph
        A: th.Tensor = self.make_graph(pos)

        # dimensions batch x n_agents x feature_dim
        fxx: th.Tensor = th.stack(list(features.values()), dim=-2)
        N = fxx.size(2)
        batch_size = fxx.size(1)
        sequence_size = fxx.size(0)

        hidden = []

        fxx = self.net_pre(fxx)
        fxx = fxx.transpose(0, 1)
        A = A.reshape(sequence_size, batch_size, N, N)
        A = A.transpose(0, 1)
        if episode_start is None:
            episode_start = th.zeros(sequence_size, 1, 1, device=fxx.device)

        if len(episode_start.shape) < 3:
            episode_start = episode_start.unsqueeze(0)

        for j in range(len(self.recurrent_layers)):
            if not state:
                hd = torch.zeros((batch_size, N, self.gnn.F), device=fxx.device)
            else:
                hd = state[j]
            hs = fxx = self.gnn(fxx, A, hd, episode_start)
            hidden.append(hs.squeeze(0))

        fxx = fxx.squeeze(0)

        if centralized:
            fxx = fxx.reshape(-1, fxx.shape[-1] * fxx.shape[-2])
            fxx = self.net_post(fxx)
            fx = {agent: fxx for agent in self.agents}
        else:
            fxx = self.net_post(fxx)
            fxx = fxx.reshape(batch_size * sequence_size, N, -1)
            fx = {agent: fxx[:, i, :] for i, agent in enumerate(self.agents)}

        return fx, tuple(hidden)


class GGNNExtractor(GraphExtractor):
    """
    Constructs an MLP that receives the output from a previous features extractor (i.e. a CNN) or directly
    the observations (if no features extractor is applied) as an input and outputs a latent representation
    for the policy and a value network.

    The ``net_arch`` parameter allows to specify the amount and size of the hidden layers.
    It can be in either of the following forms:
    1. ``dict(vf=[<list of layer sizes>], pi=[<list of layer sizes>])``: to specify the amount and size of the layers in the
        policy and value nets individually. If it is missing any of the keys (pi or vf),
        zero layers will be considered for that key.
    2. ``[<list of layer sizes>]``: "shortcut" in case the amount and size of the layers
        in the policy and value nets are the same. Same as ``dict(vf=int_list, pi=int_list)``
        where int_list is the same for the actor and critic.

    .. note::
        If a key is not specified or an empty list is passed ``[]``, a linear network will be used.

    :param feature_dim: Dimension of the feature vector (can be the output of a CNN)
    :param net_arch: The specification of the policy and value networks.
        See above for details on its formatting.
    :param activation_fn: The activation function to use for the networks.
    :param device: PyTorch device.
    """

    type = ["gnn", "recurrent"]
    edge_variable = "pos"
    edge_radius = 2.0

    def __init__(
        self,
        feature_dim: int,
        agents: List[str],
        activation_fn: Type[nn.Module],
        net_arch: Union[List[int], Dict[str, List[int]]] = None,
        shared_parameter_critic: bool = False,
        shared_parameter_actor: bool = False,
        centralized_critics: bool = False,
        device: Union[th.device, str] = "auto",
    ) -> None:
        super().__init__(feature_dim, agents, centralized_critics)
        self.device = th.device("cuda" if th.cuda.is_available() else "cpu")

        self.policy = GGNNNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
        )
        self.value = GGNNNetwork(
            feature_dim=feature_dim,
            agents=agents,
            activation_fn=activation_fn,
            centralized=centralized_critics,
        )

        self.policy.edge_variable = self.edge_variable
        self.value.edge_variable = self.edge_variable
        self.policy.edge_radius = self.edge_radius
        self.value.edge_radius = self.edge_radius

        # save dim of recurrent layers
        self.recurrent_pi_layers = self.policy.recurrent_layers
        self.recurrent_vf_layers = self.value.recurrent_layers

        self.latent_dim_pi = self.policy.last_layer_dim
        self.latent_dim_vf = self.value.last_layer_dim

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.policy.agents = agents
        self.value.agents = agents

    def forward(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Tuple[Union[TensorDict, th.Tensor], Union[TensorDict, th.Tensor]]:
        """
        :return: latent_policy, latent_value of the specified network.
            If all layers are shared, then ``latent_policy == latent_value``
        """
        return (
            self.forward_actor(
                dict(features),
                state[0],
                episode_start,
            ),
            self.forward_critic(
                dict(features),
                state[1],
                episode_start,
            ),
        )

    def forward_actor(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.policy(features, state, episode_start)

    def forward_critic(
        self,
        features: Union[TensorDict, th.Tensor],
        state: Tuple,
        episode_start: th.Tensor,
    ) -> Union[TensorDict, th.Tensor]:
        return self.value(
            features,
            state=state,
            episode_start=episode_start,
            centralized=self.centralized_critics,
        )
