def check_graph_nn(net):
    return "gnn" in net.type


def check_recurrent_nn(net):
    return "recurrent" in net.type


def check_mo_nn(net):
    return "multi-objective" in net.type
