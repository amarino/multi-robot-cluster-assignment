"""Policies: abstract base class and concrete implementations."""

import collections
from copy import deepcopy
import warnings
from abc import ABC, abstractmethod
from functools import partial
from typing import Any, Dict, List, Optional, Tuple, Type, TypeVar, Union, Callable

import numpy as np
import torch as th
from gymnasium import spaces
from torch import nn

from stable_baselines3.common.preprocessing import (
    get_action_dim,
)
from stable_baselines3.common.policies import BasePolicy, BaseModel
from stable_baselines3.common.distributions import (
    BernoulliDistribution,
    CategoricalDistribution,
    DiagGaussianDistribution,
    Distribution,
    MultiCategoricalDistribution,
    StateDependentNoiseDistribution,
    make_proba_distribution,
)

from multi_agent_stable_baselines3.common.torch_layers import (
    MultiAgentFeaturesExtractor,
    MultiAgentMlpExtractor,
    MultiAgentMLPNetwork,
    GNNNetwork,
)
from stable_baselines3.common.torch_layers import BaseFeaturesExtractor
from stable_baselines3.common.type_aliases import PyTorchObs, Schedule
from stable_baselines3.common.utils import (
    get_device,
    is_vectorized_observation,
    obs_as_tensor,
)

from multi_agent_stable_baselines3.common.utils import (
    check_graph_nn,
    check_recurrent_nn,
)

SelfBaseModel = TypeVar("SelfBaseModel", bound="BaseModel")


class MultiAgentActorCriticPolicy(BasePolicy):

    def __init__(
        self,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        lr_schedule: Callable[[float], float],
        centralized_critics: bool = False,
        net_arch: Optional[Union[List[int], Dict[str, List[int]]]] = None,
        net_model: Optional[Dict[str, Type[nn.Module]]] = None,
        activation_fn: Type[nn.Module] = nn.Tanh,
        net_model_kwargs: Optional[Dict[str, Any]] = None,
        ortho_init: bool = True,
        use_sde: bool = False,
        log_std_init: float = 0.0,
        full_std: bool = True,
        use_expln: bool = False,
        squash_output: bool = False,
        features_extractor_class: Type[BaseFeaturesExtractor] = MultiAgentFeaturesExtractor,
        features_extractor_kwargs: Optional[Dict[str, Any]] = None,
        share_features_extractor: bool = True,
        share_parameters_actor: bool = False,
        share_parameters_critic: bool = False,
        normalize_images: bool = True,
        optimizer_class: Type[th.optim.Optimizer] = th.optim.Adam,
        optimizer_kwargs: Optional[Dict[str, Any]] = None,
    ):
        # Disable orthogonal initialization
        features_extractor_class = MultiAgentFeaturesExtractor
        features_extractor_kwargs = {
            "keep_dict": True,
            "use_mlp": True,
            "use_sequence": check_recurrent_nn(net_model),
        }
        # TODO: different shared features extractor when we are dealing with state value function
        self.na = len(action_space.keys())
        self.agents = list(action_space.keys())
        action_space = action_space[self.agents[0]]
        self.share_parameters_actor = share_parameters_actor
        self.share_parameters_critic = share_parameters_critic
        self.centralized_critics = centralized_critics
        self.net_model_kwargs = net_model_kwargs or {}

        if optimizer_kwargs is None:
            optimizer_kwargs = {}
            # Small values to avoid NaN in Adam optimizer
            if optimizer_class == th.optim.Adam:
                optimizer_kwargs["eps"] = 1e-5

        super().__init__(
            observation_space,
            action_space,
            features_extractor_class,
            features_extractor_kwargs,
            optimizer_class=optimizer_class,
            optimizer_kwargs=optimizer_kwargs,
            squash_output=squash_output,
            normalize_images=normalize_images,
        )

        if isinstance(net_arch, list) and len(net_arch) > 0 and isinstance(net_arch[0], dict):
            warnings.warn(
                (
                    "As shared layers in the mlp_extractor are removed since SB3 v1.8.0, "
                    "you should now pass directly a dictionary and not a list "
                    "(net_arch=dict(pi=..., vf=...) instead of net_arch=[dict(pi=..., vf=...)])"
                ),
            )
            net_arch = net_arch[0]

        self.model_class = net_model

        self.net_arch = net_arch
        self.activation_fn = activation_fn
        self.ortho_init = ortho_init

        self.share_features_extractor = share_features_extractor
        self.features_extractor = self.make_features_extractor()
        self.features_dim = self.features_extractor.features_dim
        if self.share_features_extractor:
            self.pi_features_extractor = self.features_extractor
            self.vf_features_extractor = self.features_extractor
        else:
            self.pi_features_extractor = self.features_extractor
            self.vf_features_extractor = self.make_features_extractor()

        self.log_std_init = log_std_init
        dist_kwargs = None

        assert not (
            squash_output and not use_sde
        ), "squash_output=True is only available when using gSDE (use_sde=True)"
        # Keyword arguments for gSDE distribution
        if use_sde:
            dist_kwargs = {
                "full_std": full_std,
                "squash_output": squash_output,
                "use_expln": use_expln,
                "learn_features": False,
            }

        self.use_sde = use_sde
        self.dist_kwargs = dist_kwargs

        # Action distribution
        if self.share_parameters_actor:
            self.action_dist = make_proba_distribution(
                action_space, use_sde=use_sde, dist_kwargs=dist_kwargs
            )
        else:
            self.action_dist = {
                agent: make_proba_distribution(
                    action_space, use_sde=use_sde, dist_kwargs=dist_kwargs
                )
                for agent in self.agents
            }

        self._build(lr_schedule)

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.na = len(agents)
        self.mlp_extractor.reset_agents(agents)

    def _get_constructor_parameters(self) -> Dict[str, Any]:
        data = super()._get_constructor_parameters()

        default_none_kwargs = self.dist_kwargs or collections.defaultdict(lambda: None)  # type: ignore[arg-type, return-value]

        data.update(
            dict(
                net_arch=self.net_arch,
                activation_fn=self.activation_fn,
                use_sde=self.use_sde,
                log_std_init=self.log_std_init,
                squash_output=default_none_kwargs["squash_output"],
                full_std=default_none_kwargs["full_std"],
                use_expln=default_none_kwargs["use_expln"],
                lr_schedule=self._dummy_schedule,  # dummy lr schedule, not needed for loading policy alone
                ortho_init=self.ortho_init,
                optimizer_class=self.optimizer_class,
                optimizer_kwargs=self.optimizer_kwargs,
                features_extractor_class=self.features_extractor_class,
                features_extractor_kwargs=self.features_extractor_kwargs,
            )
        )
        return data

    def reset_noise(self, n_envs: int = 1) -> None:
        """
        Sample new weights for the exploration matrix.

        :param n_envs:
        """
        assert isinstance(
            self.action_dist, StateDependentNoiseDistribution
        ), "reset_noise() is only available when using gSDE"
        self.action_dist.sample_weights(self.log_std, batch_size=n_envs)

    def _build(self, lr_schedule: Schedule) -> None:
        """
        Create the networks and the optimizer.

        :param lr_schedule: Learning rate schedule
            lr_schedule(1) is the initial learning rate
        """
        self._build_mlp_extractor()

        latent_dim_pi = self.mlp_extractor.latent_dim_pi

        if not self.share_parameters_critic:
            value_net = {}
        if not self.share_parameters_actor:
            action_net, log_std = {}, {}

        for agent in self.agents:

            if not self.share_parameters_actor:
                if isinstance(self.action_dist[agent], DiagGaussianDistribution):
                    action_net[agent], log_std[agent] = self.action_dist[
                        agent
                    ].proba_distribution_net(
                        latent_dim=latent_dim_pi, log_std_init=self.log_std_init
                    )
                elif isinstance(self.action_dist[agent], StateDependentNoiseDistribution):
                    action_net[agent], log_std[agent] = self.action_dist[
                        agent
                    ].proba_distribution_net(
                        latent_dim=latent_dim_pi,
                        latent_sde_dim=latent_dim_pi,
                        log_std_init=self.log_std_init,
                    )
                elif isinstance(
                    self.action_dist[agent],
                    (
                        CategoricalDistribution,
                        MultiCategoricalDistribution,
                        BernoulliDistribution,
                    ),
                ):
                    action_net[agent] = self.action_dist[agent].proba_distribution_net(
                        latent_dim=latent_dim_pi
                    )
                else:
                    raise NotImplementedError(
                        f"Unsupported distribution '{self.action_dist[agent]}'."
                    )

            if not self.share_parameters_critic:
                value_net[agent] = nn.Linear(self.mlp_extractor.latent_dim_vf, 1)

        if self.share_parameters_actor:
            if isinstance(self.action_dist, DiagGaussianDistribution):
                action_net, log_std = self.action_dist.proba_distribution_net(
                    latent_dim=latent_dim_pi, log_std_init=self.log_std_init
                )
            elif isinstance(self.action_dist, StateDependentNoiseDistribution):
                action_net, log_std = self.action_dist.proba_distribution_net(
                    latent_dim=latent_dim_pi,
                    latent_sde_dim=latent_dim_pi,
                    log_std_init=self.log_std_init,
                )
            elif isinstance(
                self.action_dist,
                (
                    CategoricalDistribution,
                    MultiCategoricalDistribution,
                    BernoulliDistribution,
                ),
            ):
                action_net = self.action_dist.proba_distribution_net(latent_dim=latent_dim_pi)
            else:
                raise NotImplementedError(f"Unsupported distribution '{self.action_dist}'.")

        if self.share_parameters_actor:
            self.action_net = action_net
            self.log_std = log_std
        else:
            self.action_net = nn.ModuleDict({agent: action_net[agent] for agent in self.agents})
            self.log_std = nn.ParameterDict({agent: log_std[agent] for agent in self.agents})

        if self.share_parameters_critic:
            self.value_net = nn.Linear(self.mlp_extractor.latent_dim_vf, 1)
        else:
            self.value_net = nn.ModuleDict({agent: value_net[agent] for agent in self.agents})

        # Init weights: use orthogonal initialization
        # with small initial weight for the output
        if self.ortho_init:
            # TODO: check for features_extractor
            # Values from stable-baselines.
            # features_extractor/mlp values are
            # originally from openai/baselines (default gains/init_scales).
            module_gains = {
                self.features_extractor: np.sqrt(2),
                self.mlp_extractor: np.sqrt(2),
                self.action_net: 0.01,
                self.value_net: 1,
            }
            if not self.share_features_extractor:
                # Note(antonin): this is to keep SB3 results
                # consistent, see GH#1148
                del module_gains[self.features_extractor]
                module_gains[self.pi_features_extractor] = np.sqrt(2)
                module_gains[self.vf_features_extractor] = np.sqrt(2)

            for module, gain in module_gains.items():
                module.apply(partial(self.init_weights, gain=gain))

        # Setup optimizer with initial learning rate
        self.optimizer = self.optimizer_class(self.parameters(), lr=lr_schedule(1), **self.optimizer_kwargs)  # type: ignore[call-arg]

    def _build_mlp_extractor(self) -> None:
        # this can also be with a state
        self.mlp_extractor = self.model_class(
            self.features_dim,
            agents=self.agents,
            shared_parameter_actor=self.share_parameters_actor,
            shared_parameter_critic=self.share_parameters_critic,
            centralized_critics=self.centralized_critics,
            net_arch=self.net_arch,
            activation_fn=self.activation_fn,
            device=self.device,
            **self.net_model_kwargs,
        )

    def extract_features(  # type: ignore[override]
        self,
        obs: PyTorchObs,
        features_extractor: Optional[BaseFeaturesExtractor] = None,
    ) -> Union[th.Tensor, Tuple[th.Tensor, th.Tensor]]:
        """
        Preprocess the observation if needed and extract features.

        :param obs: Observation
        :param features_extractor: The features extractor to use. If None, then ``self.features_extractor`` is used.
        :return: The extracted features. If features extractor is not shared, returns a tuple with the
            features for the actor and the features for the critic.
        """
        if self.share_features_extractor:
            return (
                self.features_extractor(obs)
                if features_extractor is None
                else features_extractor(obs)
            )
        else:
            if features_extractor is not None:
                warnings.warn(
                    "Provided features_extractor will be ignored because the features extractor is not shared.",
                    UserWarning,
                )

            pi_features = self.pi_features_extractor(obs)
            vf_features = self.vf_features_extractor(obs)
            return pi_features, vf_features

    def _get_action_dist_from_latent(self, latent_pi: th.Tensor) -> Distribution:
        """
        Retrieve action distribution given the latent codes.

        :param latent_pi: Latent code for the actor
        :return: Action distribution
        """
        mean_actions = {}
        for i, agent in enumerate(self.agents):
            if self.share_parameters_actor:
                ma = self.action_net(latent_pi[agent])

                if isinstance(self.action_dist, DiagGaussianDistribution):
                    action_dist = DiagGaussianDistribution(self.action_space.shape[0])
                    mean_actions[agent] = action_dist.proba_distribution(ma, self.log_std)
                elif isinstance(self.action_dist, CategoricalDistribution):
                    # Here mean_actions are the logits before the softmax
                    mean_actions[agent] = self.action_dist.proba_distribution(action_logits=ma)
                elif isinstance(self.action_dist, MultiCategoricalDistribution):
                    # Here mean_actions are the flattened logits
                    mean_actions[agent] = self.action_dist.proba_distribution(action_logits=ma)
                elif isinstance(self.action_dist, BernoulliDistribution):
                    # Here mean_actions are the logits (before rounding to get the binary actions)
                    mean_actions[agent] = self.action_dist.proba_distribution(action_logits=ma)
                elif isinstance(self.action_dist, StateDependentNoiseDistribution):
                    mean_actions[agent] = self.action_dist.proba_distribution(
                        ma, self.log_std, latent_pi[i, :]
                    )
                else:
                    raise ValueError("Invalid action distribution")

            else:
                ma = self.action_net[agent](latent_pi[agent])

                if isinstance(self.action_dist[agent], DiagGaussianDistribution):
                    mean_actions[agent] = self.action_dist[agent].proba_distribution(
                        ma, self.log_std[agent]
                    )
                elif isinstance(self.action_dist[agent], CategoricalDistribution):
                    # Here mean_actions are the logits before the softmax
                    mean_actions[agent] = self.action_dist[agent].proba_distribution(
                        action_logits=ma
                    )
                elif isinstance(self.action_dist[agent], MultiCategoricalDistribution):
                    # Here mean_actions are the flattened logits
                    mean_actions[agent] = self.action_dist[agent].proba_distribution(
                        action_logits=ma
                    )
                elif isinstance(self.action_dist[agent], BernoulliDistribution):
                    # Here mean_actions are the logits (before rounding to get the binary actions)
                    mean_actions[agent] = self.action_dist[agent].proba_distribution(
                        action_logits=ma
                    )
                elif isinstance(self.action_dist[agent], StateDependentNoiseDistribution):
                    mean_actions[agent] = self.action_dist[agent].proba_distribution(
                        ma, self.log_std[agent], latent_pi[i, :]
                    )
                else:
                    raise ValueError("Invalid action distribution")

        return mean_actions

    def get_distribution(
        self,
        obs: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
    ) -> Distribution:
        """
        Get the current policy distribution given the observations.

        :param obs:
        :return: the action distribution.
        """
        features = self.pi_features_extractor(obs)

        if check_recurrent_nn(self.model_class):
            latent_pi, state = self.mlp_extractor.forward_actor(
                features,
                state,
                episode_start=episode_starts,
            )
        else:
            latent_pi = self.mlp_extractor.forward_actor(features)
        return self._get_action_dist_from_latent(latent_pi), state

    def predict(
        self,
        observation: Union[np.ndarray, Dict[str, np.ndarray]],
        state: Optional[Tuple[np.ndarray, ...]] = None,
        episode_start: Optional[np.ndarray] = None,
        deterministic: bool = False,
        infos: Optional[List[Dict[str, Any]]] = None,
    ) -> Tuple[Dict[str, np.ndarray], Optional[Tuple[np.ndarray, ...]]]:
        """
        Get the policy action from an observation (and optional hidden state).
        Includes sugar-coating to handle different observations (e.g. normalizing images).

        :param observation: the input observation
        :param state: The last hidden states (can be None, used in recurrent policies)
        :param episode_start: The last masks (can be None, used in recurrent policies)
            this correspond to beginning of episodes,
            where the hidden states of the RNN must be reset.
        :param deterministic: Whether or not to return deterministic actions.
        :return: the model's action and the next hidden state
            (used in recurrent policies)
        """
        # Switch to eval mode (this affects batch norm / dropout)
        self.set_training_mode(False)

        # Check for common mistake that the user does not mix Gym/VecEnv API
        # Tuple obs are not supported by SB3, so we can safely do that check
        if not isinstance(observation, dict):
            raise ValueError(
                "You have passed a tuple to the predict() function instead of a Numpy array or a Dict. "
                "You are probably mixing Gym API with SB3 VecEnv API: `obs, info = env.reset()` (Gym) "
                "vs `obs = vec_env.reset()` (SB3 VecEnv). "
                "See related issue https://github.com/DLR-RM/stable-baselines3/issues/1694 "
                "and documentation for more information: https://stable-baselines3.readthedocs.io/en/master/guide/vec_envs.html#vecenv-api-vs-gym-api"
            )

        obs_tensor, vectorized_env = self.obs_to_tensor(observation)

        if check_graph_nn(self.model_class):
            # Graph NNs need to add position information to form the graph
            if infos is None or not infos or any([info.get("pos") is None for info in infos]):
                raise ValueError(
                    "Graph Neural Networks need to receive the position information in the info dict. "
                )
            pos = []
            for info in infos:
                pos = pos + [info["pos"]]

            pos = np.stack(pos, axis=0)
            obs_tensor["pos"] = th.from_numpy(pos).to(obs_tensor[self.agents[0]].device)

        with th.no_grad():

            states = None

            if check_recurrent_nn(self.model_class):
                if state is None:
                    # Initialize hidden states to zeros
                    state = self.get_initial_states(obs_tensor[self.agents[0]].shape[0])[0]
                states = tuple(th.tensor(s, dtype=th.float32, device=self.device) for s in state)

            episode_starts = th.tensor(episode_start, dtype=th.float32, device=self.device)
            actions, states = self._predict(
                obs_tensor, states, episode_starts, deterministic=deterministic
            )
            if check_recurrent_nn(self.model_class):
                states = tuple(s.cpu().numpy() for s in states)

        # Convert to numpy, and reshape to the original action shape
        actions = actions.cpu().numpy().reshape((-1, *self.action_space.shape))  # type: ignore[misc, assignment]

        if isinstance(self.action_space, spaces.Box):
            if self.squash_output:
                # Rescale to proper domain when using squashing
                actions = self.unscale_action(actions)  # type: ignore[assignment, arg-type]
            else:
                # Actions could be on arbitrary scale, so clip the actions to avoid
                # out of bound error (e.g. if sampling from a Gaussian distribution)
                actions = np.clip(actions, self.action_space.low, self.action_space.high)  # type: ignore[assignment, arg-type]

        actions = actions.reshape(-1, len(self.agents), *self.action_space.shape)
        actions = {agent: actions[:, i, :] for i, agent in enumerate(self.agents)}
        # Remove batch dimension if needed
        if not vectorized_env:
            for action in actions:
                assert isinstance(actions[action], np.ndarray)
                actions[action] = actions[action].squeeze(axis=0)

        return actions, states  # type: ignore[return-value]

    def _predict(
        self,
        observation: PyTorchObs,
        state: PyTorchObs = None,
        episode_starts: th.Tensor = None,
        deterministic: bool = False,
    ) -> th.Tensor:
        """
        Get the action according to the policy for a given observation.

        :param observation:
        :param deterministic: Whether to use stochastic or deterministic actions
        :return: Taken action according to the policy
        """
        distribution, state = self.get_distribution(observation, state, episode_starts)
        actions = {
            agent: distribution[agent].get_actions(deterministic=deterministic)
            for agent in self.agents
        }
        actions = th.stack(list(actions.values()), dim=1)
        return actions, state

    def predict_values(
        self,
        obs: PyTorchObs,
        states: Tuple = None,
        episode_starts: th.Tensor = None,
    ) -> th.Tensor:
        """
        Get the estimated values according to the current policy given the observations.

        :param obs: Observation
        :return: the estimated values.
        """
        vf_features = self.vf_features_extractor(obs)

        if check_recurrent_nn(self.model_class):
            latent_vf, states = self.mlp_extractor.forward_critic(
                vf_features, states[1], episode_starts
            )
        else:
            latent_vf = self.mlp_extractor.forward_critic(vf_features)
        if self.share_parameters_critic:
            vf = {agent: self.value_net(latent_vf[agent]) for agent in self.agents}
        else:
            vf = {agent: self.value_net[agent](latent_vf[agent]) for agent in self.agents}
        return th.stack(list(vf.values()), dim=1), states

    def get_initial_states(self, n_envs: int) -> Tuple:

        # Tuple ( [ Batch x agents x layer_states x features ] x num_layers )
        init_state_p = tuple(
            np.zeros((n_envs, self.na, *pi_r), dtype=np.float32)
            for pi_r in self.mlp_extractor.recurrent_pi_layers
        )
        init_state_v = tuple(
            np.zeros((n_envs, self.na, *pi_r), dtype=np.float32)
            for pi_r in self.mlp_extractor.recurrent_vf_layers
        )
        return (init_state_p, init_state_v)

    def forward(
        self,
        obs: th.Tensor,
        states: Tuple = None,
        episode_starts: th.Tensor = None,
        deterministic: bool = False,
    ) -> Tuple[th.Tensor, th.Tensor, th.Tensor]:
        """
        Forward pass in all the networks (actor and critic)

        :param obs: Observation
        :param state: Tuple state_pi and state_vf
        :param episode_starts: Episode starts
        :param deterministic: Whether to sample or use deterministic actions
        :return: action, value and log probability of the action
        """

        # Preprocess the observation if needed
        features = self.extract_features(obs)
        if self.share_features_extractor:

            if check_recurrent_nn(self.model_class):
                latent_pi, latent_vf = self.mlp_extractor(features, states, episode_starts)
                states = (latent_pi[1], latent_vf[1])
                latent_pi = latent_pi[0]
                latent_vf = latent_vf[0]
            else:
                latent_pi, latent_vf = self.mlp_extractor(features)
        else:
            pi_features, vf_features = features
            if check_recurrent_nn(self.model_class):
                latent_pi, states[0] = self.mlp_extractor.forward_actor(
                    pi_features, states[0], episode_starts
                )
                latent_vf, states[1] = self.mlp_extractor.forward_critic(
                    vf_features, states[1], episode_starts
                )
            else:
                latent_pi = self.mlp_extractor.forward_actor(pi_features)
                latent_vf = self.mlp_extractor.forward_critic(vf_features)

        # Evaluate the values for the given observations
        if self.share_parameters_critic:
            values = {agent: self.value_net(latent_vf[agent]) for agent in self.agents}
        else:
            values = {agent: self.value_net[agent](latent_vf[agent]) for agent in self.agents}
        values = th.stack(list(values.values()), dim=1)

        distribution = self._get_action_dist_from_latent(latent_pi)
        actions = {
            agent: distribution[agent].get_actions(deterministic=deterministic)
            for agent in self.agents
        }
        log_prob = {agent: distribution[agent].log_prob(actions[agent]) for agent in self.agents}
        actions = th.stack(list(actions.values()), dim=1)
        log_prob = th.stack(list(log_prob.values()), dim=1)
        log_prob = log_prob.unsqueeze(-1)
        return actions, values, log_prob, states

    def evaluate_actions(
        self,
        obs: PyTorchObs,
        actions: PyTorchObs,
        states: th.Tensor = None,
        episode_starts: th.Tensor = None,
    ) -> Tuple[th.Tensor, th.Tensor, Optional[th.Tensor]]:
        # Preprocess the observation if needed
        features = self.extract_features(obs)
        if self.share_features_extractor:
            if check_recurrent_nn(self.model_class):
                latent_pi, latent_vf = self.mlp_extractor(features, states, episode_starts)
                states = (latent_pi[1], latent_vf[1])
                latent_pi = latent_pi[0]
                latent_vf = latent_vf[0]
            else:
                latent_pi, latent_vf = self.mlp_extractor(features)
        else:
            pi_features, vf_features = features
            if check_recurrent_nn(self.model_class):
                latent_pi, states[0] = self.mlp_extractor.forward_actor(
                    pi_features, states[0], episode_starts
                )
                latent_vf, states[1] = self.mlp_extractor.forward_critic(
                    vf_features, states[1], episode_starts
                )
            else:
                latent_pi = self.mlp_extractor.forward_actor(pi_features)
                latent_vf = self.mlp_extractor.forward_critic(vf_features)
        distribution = self._get_action_dist_from_latent(latent_pi)
        log_prob = {agent: distribution[agent].log_prob(actions[agent]) for agent in self.agents}
        log_prob = th.stack(list(log_prob.values()), dim=1)

        if self.share_parameters_critic:
            values = {agent: self.value_net(latent_vf[agent]) for agent in self.agents}
        else:
            values = {agent: self.value_net[agent](latent_vf[agent]) for agent in self.agents}

        values = th.stack(list(values.values()), dim=1)

        entropy = {agent: distribution[agent].entropy() for agent in self.agents}
        entropy = th.stack(list(entropy.values()), dim=1)
        entropy = entropy.unsqueeze(-1)
        log_prob = log_prob.unsqueeze(-1)
        return values, log_prob, entropy


class ContinuousCritic(BaseModel):
    """
    Critic network(s) for DDPG/SAC/TD3.
    It represents the action-state value function (Q-value function).
    Compared to A2C/PPO critics, this one represents the Q-value
    and takes the continuous action as input. It is concatenated with the state
    and then fed to the network which outputs a single value: Q(s, a).
    For more recent algorithms like SAC/TD3, multiple networks
    are created to give different estimates.

    By default, it creates two critic networks used to reduce overestimation
    thanks to clipped Q-learning (cf TD3 paper).

    :param observation_space: Observation space
    :param action_space: Action space
    :param net_arch: Network architecture
    :param features_extractor: Network to extract features
        (a CNN when using images, a nn.Flatten() layer otherwise)
    :param features_dim: Number of features
    :param activation_fn: Activation function
    :param normalize_images: Whether to normalize images or not,
         dividing by 255.0 (True by default)
    :param n_critics: Number of critic networks to create.
    :param share_features_extractor: Whether the features extractor is shared or not
        between the actor and the critic (this saves computation time)
    """

    features_extractor: MultiAgentFeaturesExtractor

    def __init__(
        self,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        net_model: Type[nn.Module],
        net_model_kwargs: Optional[Dict[str, Any]],
        features_extractor: MultiAgentFeaturesExtractor,
        features_dim: int,
        activation_fn: Type[nn.Module] = nn.ReLU,
        normalize_images: bool = True,
        n_critics: int = 2,
        share_features_extractor: bool = True,
        shared_parameters: bool = False,
        centralized_critics: bool = False,
    ):
        self.na = len(action_space.keys())
        self.agents = list(action_space.keys())
        action_space = action_space[self.agents[0]]
        self.shared_parameters = shared_parameters
        self.centralized_critics = centralized_critics

        super().__init__(
            observation_space,
            action_space,
            features_extractor=features_extractor,
            normalize_images=normalize_images,
        )

        self.features_extractor.use_sequence = check_recurrent_nn(net_model)

        action_dim = get_action_dim(self.action_space)
        self.edge_variables = "pos"
        self.share_features_extractor = share_features_extractor
        self.n_critics = n_critics
        self.q_networks: List[nn.Module] = []
        self.net_model = net_model
        net_model_kwargs["feature_dim"] = features_dim
        net_model_kwargs["activation_fn"] = activation_fn
        net_model_kwargs["agents"] = self.agents
        net_model_kwargs["shared_parameters"] = shared_parameters
        net_model_kwargs["add_final_linear"] = False
        net_model_kwargs["centralized"] = centralized_critics
        self.q_networks_final: List[nn.Module] = []

        for idx in range(n_critics):
            q_net = net_model(**net_model_kwargs)
            self.add_module(f"qf{idx}", q_net)
            self.q_networks.append(q_net)

        final_model_kwargs = {}
        final_model_kwargs["add_final_linear"] = True
        final_model_kwargs["net_arch"] = [128]
        final_model_kwargs["activation_fn"] = activation_fn
        final_model_kwargs["agents"] = self.agents
        final_model_kwargs["shared_parameters"] = shared_parameters
        final_model_kwargs["feature_dim"] = self.q_networks[0].last_layer_dim + action_dim
        for idx in range(n_critics):
            q_net = MultiAgentMLPNetwork(**final_model_kwargs)
            self.add_module(f"qf{idx}_final", q_net)
            self.q_networks_final.append(q_net)

    def extract_features(  # type: ignore[override]
        self,
        obs: PyTorchObs,
        features_extractor: Optional[BaseFeaturesExtractor] = None,
    ) -> Union[th.Tensor, Tuple[th.Tensor, th.Tensor]]:
        """
        Preprocess the observation if needed and extract features.

        :param obs: Observation
        :param features_extractor: The features extractor to use. If None, then ``self.features_extractor`` is used.
        :return: The extracted features. If features extractor is not shared, returns a tuple with the
            features for the actor and the features for the critic.
        """
        return (
            self.features_extractor(obs) if features_extractor is None else features_extractor(obs)
        )

    def reset_agents(self, agents: List[str]) -> None:
        self.agents = agents
        self.na = len(agents)
        for q_net in self.q_networks:
            q_net.agents = agents

    def forward(
        self,
        obs: PyTorchObs,
        actions: PyTorchObs,
        sequence_start: th.Tensor = None,
    ) -> Tuple[th.Tensor, ...]:

        with th.set_grad_enabled(not self.share_features_extractor):
            obs = self.extract_features(obs, self.features_extractor)

        if check_recurrent_nn(self.net_model):
            q_out = []
            for j, q_net in enumerate(self.q_networks):
                q_p, _ = q_net(
                    dict(obs),
                    centralized=self.centralized_critics,
                    episode_start=sequence_start,
                )
                q_p_final, _ = self.q_networks_final[j](
                    {
                        agent: th.cat([actions[:, :, i, :], q_p[agent]], dim=-1)
                        for i, agent in enumerate(self.agents)
                    },
                    centralized=self.centralized_critics,
                    episode_start=sequence_start,
                )
                q_out.append(
                    th.stack(
                        list(q_p_final.values()),
                        dim=-2,
                    )
                )
            q_out = tuple(q_out)
        else:
            q_out = []
            for j, q_net in enumerate(self.q_networks):
                q_p = q_net(
                    dict(obs),
                    centralized=self.centralized_critics,
                )
                q_p_final = self.q_networks_final[j](
                    {
                        agent: th.cat([actions[:, i, :], q_p[agent]], dim=-1)
                        for i, agent in enumerate(self.agents)
                    },
                    centralized=self.centralized_critics,
                )
                q_out.append(
                    th.stack(
                        list(q_p_final.values()),
                        dim=-2,
                    )
                )
            q_out = tuple(q_out)

        return q_out

    def q1_forward(
        self,
        obs: th.Tensor,
        actions: th.Tensor,
        sequence_start: th.Tensor = None,
    ) -> th.Tensor:
        """
        Only predict the Q-value using the first network.
        This allows to reduce computation when all the estimates are not needed
        (e.g. when updating the policy in TD3).
        """

        with th.no_grad():
            obs = self.extract_features(obs, self.features_extractor)

        if check_recurrent_nn(self.net_model):
            q_p, _ = self.q_networks[0](
                dict(obs),
                centralized=self.centralized_critics,
                episode_start=sequence_start,
            )
            q_p_final, _ = self.q_networks_final[0](
                {
                    agent: th.cat([actions[:, :, i, :], q_p[agent]], dim=-1)
                    for i, agent in enumerate(self.agents)
                },
                centralized=self.centralized_critics,
                episode_start=sequence_start,
            )
            q_out = th.stack(
                list(q_p_final.values()),
                dim=-2,
            )
        else:
            q_p = self.q_networks[0](
                dict(obs),
                centralized=self.centralized_critics,
            )
            q_p_final = self.q_networks_final[0](
                {
                    agent: th.cat([actions[:, i, :], q_p[agent]], dim=-1)
                    for i, agent in enumerate(self.agents)
                },
                centralized=self.centralized_critics,
            )
            q_out = th.stack(
                list(q_p_final.values()),
                dim=-2,
            )

        return q_out
