"""Common aliases for type hints"""

from typing import (
    TYPE_CHECKING,
    Any,
    Dict,
    NamedTuple,
    Tuple,
    Union,
)
from torch import Tensor

import pettingzoo

# Avoid circular imports, we use type hint as string to avoid it too
if TYPE_CHECKING:
    from stable_baselines3.common.vec_env import VecEnv
from stable_baselines3.common.type_aliases import TensorDict

PettingzooEnv = Union[pettingzoo.ParallelEnv, "VecEnv"]
PettingzooObs = Dict[str, Any]
PettingzooResetReturn = Tuple[PettingzooObs, Dict]
PettingzooStepReturn = Tuple[PettingzooObs, Dict[str, Any], bool, bool, Dict]


class MaRolloutBufferSamples(NamedTuple):
    observations: TensorDict
    actions: Tensor
    old_values: Tensor
    old_log_prob: Tensor
    advantages: Tensor
    returns: Tensor
    edges_info: Tensor
    state_pi: Tensor
    state_vf: Tensor
    episode_starts: Tensor


class MaReplayBufferSamples(NamedTuple):
    observations: TensorDict
    actions: Tensor
    next_observations: Tensor
    dones: Tensor
    dones_next: Tensor
    rewards: Tensor
    edges_info: Tensor
    next_edges_info: Tensor
    state_pi: Tensor
    state_pi_next: Tensor
