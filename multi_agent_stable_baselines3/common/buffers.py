import warnings
from abc import ABC, abstractmethod
from typing import Any, Dict, Generator, List, Optional, Tuple, Union

import numpy as np
import torch as th
from gymnasium import spaces

from stable_baselines3.common.preprocessing import get_obs_shape
from stable_baselines3.common.buffers import BaseBuffer
from multi_agent_stable_baselines3.common.type_aliases import (
    MaReplayBufferSamples,
    MaRolloutBufferSamples,
)
from stable_baselines3.common.type_aliases import TensorDict
from stable_baselines3.common.utils import get_device
from stable_baselines3.common.vec_env import VecNormalize

try:
    # Check memory used by replay buffer when possible
    import psutil
except ImportError:
    psutil = None


def to_batch_dict_torch(
    dict: dict,
    device,
    batch: int = 1,
    copy: bool = True,
    sequence: list = [],
) -> TensorDict:
    """
    Convert a numpy array to a PyTorch tensor.
    Note: it copies the data by default

    :param array:
    :param copy: Whether to copy or not the data (may be useful to avoid changing things
        by reference). This argument is inoperative if the device is not the CPU.
    :return:
    """
    out_dict = {}
    for key, value in dict.items():
        if sequence:
            if copy:
                out_dict[key] = (
                    th.tensor(value[batch], device=device)
                    .reshape(sequence[0], sequence[1], *value.shape[1:])
                    .transpose(1, 0)
                    .reshape(sequence[1], -1, *value.shape[1:])
                )
            else:
                out_dict[key] = (
                    th.as_tensor(value[batch], device=device)
                    .reshape(sequence[0], sequence[1], *value.shape[1:])
                    .transpose(1, 0)
                    .reshape(sequence[1], -1, *value.shape[1:])
                )
        else:
            if copy:
                out_dict[key] = th.tensor(value[batch], device=device)
            else:
                out_dict[key] = th.as_tensor(value[batch], device=device)
    return out_dict


def pad(
    seq_start_indices: np.ndarray,
    seq_end_indices: np.ndarray,
    device: th.device,
    tensor: np.ndarray,
    padding_value: float = 0.0,
) -> th.Tensor:
    """
    Chunk sequences and pad them to have constant dimensions.

    :param seq_start_indices: Indices of the transitions that start a sequence
    :param seq_end_indices: Indices of the transitions that end a sequence
    :param device: PyTorch device
    :param tensor: Tensor of shape (batch_size, *tensor_shape)
    :param padding_value: Value used to pad sequence to the same length
        (zero padding by default)
    :return: (n_seq, max_length, *tensor_shape)
    """
    # Create sequences given start and end
    seq = [
        th.tensor(tensor[start : end + 1], device=device)
        for start, end in zip(seq_start_indices, seq_end_indices)
    ]
    return th.nn.utils.rnn.pad_sequence(seq, batch_first=True, padding_value=padding_value)


def pad_and_flatten(
    seq_start_indices: np.ndarray,
    seq_end_indices: np.ndarray,
    device: th.device,
    tensor: np.ndarray,
    padding_value: float = 0.0,
) -> th.Tensor:
    """
    Pad and flatten the sequences of scalar values,
    while keeping the sequence order.
    From (batch_size, 1) to (n_seq, max_length, 1) -> (n_seq * max_length,)

    :param seq_start_indices: Indices of the transitions that start a sequence
    :param seq_end_indices: Indices of the transitions that end a sequence
    :param device: PyTorch device (cpu, gpu, ...)
    :param tensor: Tensor of shape (max_length, n_seq, 1)
    :param padding_value: Value used to pad sequence to the same length
        (zero padding by default)
    :return: (n_seq * max_length,) aka (padded_batch_size,)
    """
    return pad(seq_start_indices, seq_end_indices, device, tensor, padding_value).flatten()


class MaRolloutBuffer(BaseBuffer):
    """
    Rollout buffer used in on-policy algorithms like A2C/PPO.
    It corresponds to ``buffer_size`` transitions collected
    using the current policy.
    This experience will be discarded after the policy update.
    In order to use PPO objective, we also store the current value of each state
    and the log probability of each taken action.

    The term rollout here refers to the model-free notion and should not
    be used with the concept of rollout used in model-based RL or planning.
    Hence, it is only involved in policy and value function training but not action selection.

    :param buffer_size: Max number of element in the buffer
    :param observation_space: Observation space
    :param action_space: Action space
    :param device: PyTorch device
    :param gae_lambda: Factor for trade-off of bias vs variance for Generalized Advantage Estimator
        Equivalent to classic advantage when set to 1.
    :param gamma: Discount factor
    :param n_envs: Number of parallel environments
    """

    observation_space: spaces.Dict
    actions: np.ndarray
    rewards: np.ndarray
    advantages: np.ndarray
    returns: np.ndarray
    episode_starts: np.ndarray
    log_probs: np.ndarray
    values: np.ndarray
    obs_shape: Dict[str, Tuple[int, ...]]  # type: ignore[assignment]
    observations: Dict[str, np.ndarray]  # type: ignore[assignment]
    hidden_state_vf: np.ndarray
    hidden_state_pi: np.ndarray

    def __init__(
        self,
        buffer_size: int,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        device: Union[th.device, str] = "auto",
        hidden_state_space: list = [[], []],
        gae_lambda: float = 1,
        gamma: float = 0.99,
        n_envs: int = 1,
    ):
        self.buffer_size = buffer_size
        self.observation_space = observation_space
        self.action_space = action_space
        self.obs_shape = get_obs_shape(observation_space)  # type: ignore[assignment]

        # hidden state dimensions, empty list for feedforward networks
        self.hidden_state_space = hidden_state_space

        self.seq_start_indices, self.seq_end_indices = None, None

        self.agents = list(observation_space.spaces.keys())
        self.action_dim = get_obs_shape(action_space)
        self.pos = 0
        self.full = False
        self.device = get_device(device)
        self.n_envs = n_envs

        self.sequence_length = 16

        self.gae_lambda = gae_lambda
        self.gamma = gamma
        self.generator_ready = False
        self.reset()

    def compute_returns_and_advantage(self, last_values: th.Tensor, dones: np.ndarray) -> None:
        """
        Post-processing step: compute the lambda-return (TD(lambda) estimate)
        and GAE(lambda) advantage.

        Uses Generalized Advantage Estimation (https://arxiv.org/abs/1506.02438)
        to compute the advantage. To obtain Monte-Carlo advantage estimate (A(s) = R - V(S))
        where R is the sum of discounted reward with value bootstrap
        (because we don't always have full episode), set ``gae_lambda=1.0`` during initialization.

        The TD(lambda) estimator has also two special cases:
        - TD(1) is Monte-Carlo estimate (sum of discounted rewards)
        - TD(0) is one-step estimate with bootstrapping (r_t + gamma * v(s_{t+1}))

        For more information, see discussion in https://github.com/DLR-RM/stable-baselines3/pull/375.

        :param last_values: state value estimation for the last step (one for each env)
        :param dones: if the last step was a terminal step (one bool for each env).
        """
        # Convert to numpy
        last_values = last_values.clone().cpu().numpy()  # type: ignore[assignment]

        last_gae_lam = 0
        for step in reversed(range(self.buffer_size)):
            if step == self.buffer_size - 1:
                next_non_terminal = 1.0 - dones.astype(np.float32).reshape(-1, 1)
                next_values = last_values
            else:
                next_non_terminal = 1.0 - self.episode_starts[step + 1]
                next_values = self.values[step + 1]
            next_non_terminal = next_non_terminal.reshape(-1, 1, 1)
            delta = (
                self.rewards[step]
                + self.gamma * next_values * next_non_terminal
                - self.values[step]
            )
            last_gae_lam = delta + self.gamma * self.gae_lambda * next_non_terminal * last_gae_lam
            self.advantages[step] = last_gae_lam
        # TD(lambda) estimator, see Github PR #375 or "Telescoping in TD(lambda)"
        # in David Silver Lecture 4: https://www.youtube.com/watch?v=PnHCvfgC_ZA
        self.returns = self.advantages + self.values

    def reset(self) -> None:
        self.observations = {}
        self.actions = {}
        self.rewards = {}
        self.advantages = {}
        self.returns = {}
        self.values = {}
        self.log_probs = {}
        for key, obs_input_shape in self.obs_shape.items():
            self.observations[key] = np.zeros(
                (self.buffer_size, self.n_envs, *obs_input_shape), dtype=np.float32
            )
        self.actions = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), *self.action_dim[key]),
            dtype=np.float32,
        )
        self.rewards = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.returns = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.values = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.log_probs = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.advantages = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.edges_info = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 2), dtype=np.float32
        )
        self.episode_starts = np.zeros((self.buffer_size, self.n_envs, 1), dtype=np.float32)

        self.hidden_state_vf = tuple(
            np.zeros((self.buffer_size, self.n_envs, len(self.agents), *hs), dtype=np.float32)
            for hs in self.hidden_state_space[1]
        )
        self.hidden_state_pi = tuple(
            np.zeros((self.buffer_size, self.n_envs, len(self.agents), *hs), dtype=np.float32)
            for hs in self.hidden_state_space[0]
        )

        self.generator_ready = False
        self.pos = 0
        self.full = False

    def add(  # type: ignore[override]
        self,
        obs: Dict[str, np.ndarray],
        action: np.ndarray,
        reward: Dict[str, np.ndarray],
        episode_start: Dict[str, np.ndarray],
        value: Dict[str, th.Tensor],
        log_prob: Dict[str, th.Tensor],
        state_pi: Tuple[th.Tensor],
        state_vf: Tuple[th.Tensor],
        edges_info: np.ndarray,
    ) -> None:
        """
        :param obs: Observation
        :param action: Action
        :param reward:
        :param episode_start: Start of episode signal.
        :param value: estimated value of the current state
            following the current policy.
        :param log_prob: log probability of the action
            following the current policy.
        """
        if len(log_prob.shape) == 0:
            # Reshape 0-d tensor to avoid error
            log_prob = log_prob.reshape(-1, 1)

        for key in self.observations.keys():

            obs_ = np.array(obs[key])
            # Reshape needed when using multiple envs with discrete observations
            # as numpy cannot broadcast (n_discrete,) to (n_discrete, 1)
            if isinstance(self.observation_space.spaces[key], spaces.Discrete):
                obs_ = obs_.reshape((self.n_envs,) + self.obs_shape[key])
            self.observations[key][self.pos] = obs_

        # Reshape to handle multi-dim and discrete action spaces, see GH #970 #1392
        self.actions[self.pos] = action.reshape(
            (self.n_envs, len(self.agents), *self.action_dim[key])
        )
        self.rewards[self.pos] = np.stack(list(reward.values()), axis=1).reshape(
            (-1, len(self.agents), 1)
        )
        self.values[self.pos] = value.clone().cpu().numpy()
        self.log_probs[self.pos] = log_prob.clone().cpu().numpy()
        self.episode_starts[self.pos, :, :] = np.array(episode_start).reshape(-1, 1)
        self.edges_info[self.pos] = np.array(edges_info)
        for i in range(len(self.hidden_state_space[0])):
            self.hidden_state_pi[i][self.pos] = state_pi[i].cpu().numpy()
        for i in range(len(self.hidden_state_space[1])):
            self.hidden_state_vf[i][self.pos] = state_vf[i].cpu().numpy()
        self.pos += 1
        if self.pos == self.buffer_size:
            self.full = True

    def get(  # type: ignore[override]
        self,
        batch_size: Optional[int] = None,
    ) -> Generator[MaRolloutBufferSamples, None, None]:
        assert self.full, ""

        if len(self.hidden_state_space[0]) or len(self.hidden_state_space[1]):
            """
            buffer = [
                i
                for i in range(
                    0,
                    self.buffer_size * self.n_envs - (self.sequence_length - 1),
                    self.sequence_length,
                )
            ]
            """
            indices = np.random.permutation(
                self.buffer_size * self.n_envs - (self.sequence_length - 1)
            )
        else:
            indices = np.random.permutation(self.buffer_size * self.n_envs)
        reshape_dim = (self.buffer_size, self.n_envs)

        # Prepare the data
        if not self.generator_ready:
            _tensor_names = [
                "actions",
                "values",
                "log_probs",
                "advantages",
                "returns",
            ]

            for key in self.observations.keys():
                self.observations[key] = self.swap_and_flatten(
                    self.observations[key].reshape(*reshape_dim, -1)
                )

            for tensor in _tensor_names:
                self.__dict__[tensor] = self.swap_and_flatten(
                    self.__dict__[tensor].reshape(*reshape_dim, len(self.agents), -1)
                )

            self.episode_starts = self.swap_and_flatten(
                self.episode_starts.reshape(*reshape_dim, -1)
            )

            self.edges_info = self.swap_and_flatten(
                self.edges_info.reshape(*reshape_dim, len(self.agents), -1)
            )
            self.hidden_state_pi = tuple(
                self.swap_and_flatten(
                    self.hidden_state_pi[i].reshape(*reshape_dim, len(self.agents), *s_pi)
                )
                for i, s_pi in enumerate(self.hidden_state_space[0])
            )
            self.hidden_state_vf = tuple(
                self.swap_and_flatten(
                    self.hidden_state_vf[i].reshape(*reshape_dim, len(self.agents), *s_vf)
                )
                for i, s_vf in enumerate(self.hidden_state_space[1])
            )
            self.generator_ready = True

        # Return everything, don't create minibatches
        if batch_size is None:
            batch_size = self.buffer_size * self.n_envs

        start_idx = 0
        while start_idx < len(indices):
            yield self._get_samples(indices[start_idx : start_idx + batch_size])
            start_idx += batch_size

    def _get_samples(  # type: ignore[override]
        self,
        batch_inds: np.ndarray,
        env: Optional[VecNormalize] = None,
    ) -> MaRolloutBufferSamples:

        if len(self.hidden_state_space[0]) or len(self.hidden_state_space[1]):
            batch_size = len(batch_inds)
            batch_inds_s = np.linspace(
                batch_inds,
                batch_inds + self.sequence_length - 1,
                self.sequence_length,
                dtype=np.int16,
            )
            batch_inds_s = batch_inds_s.transpose(1, 0).reshape(-1)
            actions = (
                self.to_torch(self.actions[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.actions.shape[1:])
                .reshape(-1, *self.actions.shape[1:])
            )
            old_values = (
                self.to_torch(self.values[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.values.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.values.shape[1:])
            )[-1]
            old_log_prob = (
                self.to_torch(self.log_probs[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.log_probs.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.log_probs.shape[1:])
            )[-1]
            advantages = (
                self.to_torch(self.advantages[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.advantages.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.advantages.shape[1:])
            )[-1]
            returns = (
                self.to_torch(self.returns[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.returns.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.returns.shape[1:])
            )[-1]
            edges_info = (
                self.to_torch(self.edges_info[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.edges_info.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.edges_info.shape[1:])
            )
            state_vf = tuple(
                self.to_torch(self.hidden_state_vf[i][batch_inds]).reshape(
                    -1, *self.hidden_state_vf[i].shape[1:]
                )
                for i in range(len(self.hidden_state_space[1]))
            )
            state_pi = tuple(
                self.to_torch(self.hidden_state_pi[i][batch_inds]).reshape(
                    -1, *self.hidden_state_pi[i].shape[1:]
                )
                for i in range(len(self.hidden_state_space[0]))
            )
            episode_starts = (
                self.to_torch(self.episode_starts[batch_inds_s])
                .reshape(batch_size, self.sequence_length, *self.episode_starts.shape[1:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.episode_starts.shape[1:])
            )
            return MaRolloutBufferSamples(
                observations=to_batch_dict_torch(
                    self.observations,
                    self.device,
                    batch_inds_s,
                    sequence=[batch_size, self.sequence_length],
                ),
                actions=actions,
                old_values=old_values,
                old_log_prob=old_log_prob,
                advantages=advantages,
                returns=returns,
                edges_info=edges_info,
                state_vf=state_vf,
                state_pi=state_pi,
                episode_starts=episode_starts,
            )
        else:
            return MaRolloutBufferSamples(
                observations=to_batch_dict_torch(self.observations, self.device, batch_inds),
                actions=self.to_torch(self.actions[batch_inds]),
                old_values=self.to_torch(self.values[batch_inds]),
                old_log_prob=self.to_torch(self.log_probs[batch_inds]),
                advantages=self.to_torch(self.advantages[batch_inds]),
                returns=self.to_torch(self.returns[batch_inds]),
                edges_info=self.to_torch(self.edges_info[batch_inds]),
                state_vf=tuple(
                    self.to_torch(self.hidden_state_vf[i][batch_inds])
                    for i in range(len(self.hidden_state_space[1]))
                ),
                state_pi=tuple(
                    self.to_torch(self.hidden_state_pi[i][batch_inds])
                    for i in range(len(self.hidden_state_space[0]))
                ),
                episode_starts=self.to_torch(self.episode_starts[batch_inds]),
            )


class MaReplayBuffer(BaseBuffer):
    """
    Dict Replay buffer used in off-policy algorithms like SAC/TD3.
    Extends the ReplayBuffer to use dictionary observations

    :param buffer_size: Max number of element in the buffer
    :param observation_space: Observation space
    :param action_space: Action space
    :param device: PyTorch device
    :param n_envs: Number of parallel environments
    :param optimize_memory_usage: Enable a memory efficient variant
        Disabled for now (see https://github.com/DLR-RM/stable-baselines3/pull/243#discussion_r531535702)
    :param handle_timeout_termination: Handle timeout termination (due to timelimit)
        separately and treat the task as infinite horizon task.
        https://github.com/DLR-RM/stable-baselines3/issues/284
    """

    observations: Dict[str, np.ndarray]  # type: ignore[assignment]
    next_observations: Dict[str, np.ndarray]
    actions: Dict[str, np.ndarray]
    rewards: Dict[str, np.ndarray]
    episode_starts: np.ndarray
    dones: Dict[str, bool]
    timeouts: np.ndarray
    observation_space: spaces.Dict
    obs_shape: Dict[str, Tuple[int, ...]]  # type: ignore[assignment]
    hidden_state_pi_next: np.ndarray
    hidden_state_pi: np.ndarray

    def __init__(
        self,
        buffer_size: int,
        observation_space: spaces.Dict,
        action_space: spaces.Dict,
        device: Union[th.device, str] = "auto",
        n_envs: int = 1,
        hidden_state_space: list = [],
        optimize_memory_usage: bool = False,
        handle_timeout_termination: bool = True,
    ):
        self.buffer_size = buffer_size
        self.observation_space = observation_space
        self.action_space = action_space
        self.obs_shape = get_obs_shape(observation_space)  # type: ignore[assignment]

        # hidden state dimensions, empty list for feedforward networks
        self.hidden_state_space = hidden_state_space

        self.action_dim = get_obs_shape(action_space)
        self.pos = 0
        self.full = False
        self.device = get_device(device)
        self.n_envs = n_envs
        self.agents = list(observation_space.spaces.keys())

        self.sequence_length = 32

        self.generator_ready = False
        self.reset()

        assert isinstance(
            self.obs_shape, dict
        ), "DictReplayBuffer must be used with Dict obs space only"
        self.buffer_size = max(buffer_size // n_envs, 1)

        # Check that the replay buffer can fit into the memory
        if psutil is not None:
            mem_available = psutil.virtual_memory().available

        assert not optimize_memory_usage, "DictReplayBuffer does not support optimize_memory_usage"
        # disabling as this adds quite a bit of complexity
        # https://github.com/DLR-RM/stable-baselines3/pull/243#discussion_r531535702
        self.optimize_memory_usage = optimize_memory_usage

        self.observations = {
            key: np.zeros(
                (self.buffer_size, self.n_envs, *self.obs_shape[key]),
                dtype=observation_space[key].dtype,
            )
            for key in self.agents
        }
        self.next_observations = {
            key: np.zeros(
                (self.buffer_size, self.n_envs, *self.obs_shape[key]),
                dtype=observation_space[key].dtype,
            )
            for key in self.agents
        }

        self.actions = np.zeros(
            (
                self.buffer_size,
                self.n_envs,
                len(self.agents),
                *self.action_dim[self.agents[0]],
            ),
            dtype=self._maybe_cast_dtype(action_space[self.agents[0]].dtype),
        )
        self.rewards = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.dones = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.dones_next = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )
        self.edges_info = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 2), dtype=np.float32
        )
        self.next_edges_info = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 2), dtype=np.float32
        )

        self.hidden_state_pi_next = tuple(
            np.zeros((self.buffer_size, self.n_envs, len(self.agents), *hs), dtype=np.float32)
            for hs in self.hidden_state_space
        )
        self.hidden_state_pi = tuple(
            np.zeros((self.buffer_size, self.n_envs, len(self.agents), *hs), dtype=np.float32)
            for hs in self.hidden_state_space
        )

        # Handle timeouts termination properly if needed
        # see https://github.com/DLR-RM/stable-baselines3/issues/284
        self.handle_timeout_termination = handle_timeout_termination
        self.timeouts = np.zeros(
            (self.buffer_size, self.n_envs, len(self.agents), 1), dtype=np.float32
        )

        if psutil is not None:
            obs_nbytes = 0
            for key in self.observations.keys():
                obs_nbytes += self.observations[key].nbytes
            acts_nbytes = self.actions.nbytes
            rews_nbytes = self.rewards.nbytes
            dones_nbytes = self.dones.nbytes
            dones_next_nbytes = self.dones_next.nbytes
            edge_info_nbytes = self.edges_info.nbytes
            next_edge_info_nbytes = self.next_edges_info.nbytes
            state_pi_next_nbytes = sum(hvf.nbytes for hvf in self.hidden_state_pi_next)
            state_pi_nbytes = sum(hpi.nbytes for hpi in self.hidden_state_pi)

            total_memory_usage: float = (
                obs_nbytes
                + acts_nbytes
                + rews_nbytes
                + dones_nbytes
                + edge_info_nbytes
                + next_edge_info_nbytes
                + state_pi_next_nbytes
                + state_pi_nbytes
                + dones_next_nbytes
            )
            if not optimize_memory_usage:
                next_obs_nbytes = 0
                for _, obs in self.observations.items():
                    next_obs_nbytes += obs.nbytes
                total_memory_usage += next_obs_nbytes

            if total_memory_usage > mem_available:
                # Convert to GB
                total_memory_usage /= 1e9
                mem_available /= 1e9
                warnings.warn(
                    "This system does not have apparently enough memory to store the complete "
                    f"replay buffer {total_memory_usage:.2f}GB > {mem_available:.2f}GB"
                )

    def add(  # type: ignore[override]
        self,
        obs: Dict[str, np.ndarray],
        next_obs: Dict[str, np.ndarray],
        action: np.ndarray,
        reward: Dict[str, np.ndarray],
        done: np.ndarray,
        done_next: np.ndarray,
        infos: List[Dict[str, Any]],
        last_edges_info: np.ndarray,
        state_pi: Tuple[th.Tensor],
        state_pi_next: Tuple[th.Tensor],
        edges_info: np.ndarray,
    ) -> None:
        # Copy to avoid modification by reference
        for key in self.observations.keys():
            # Reshape needed when using multiple envs with discrete observations
            # as numpy cannot broadcast (n_discrete,) to (n_discrete, 1)
            if isinstance(self.observation_space.spaces[key], spaces.Discrete):
                obs[key] = obs[key].reshape((self.n_envs,) + self.obs_shape[key])
            self.observations[key][self.pos] = np.array(obs[key])

            if isinstance(self.observation_space.spaces[key], spaces.Discrete):
                next_obs[key] = next_obs[key].reshape((self.n_envs,) + self.obs_shape[key])
            self.next_observations[key][self.pos] = np.array(next_obs[key])

        # Reshape to handle multi-dim and discrete action spaces, see GH #970 #1392
        action = action.reshape((self.n_envs, len(self.agents), *self.action_dim[key]))

        self.actions[self.pos] = np.array(action)
        self.rewards[self.pos] = np.stack(list(reward.values()), axis=1).reshape(
            (-1, len(self.agents), 1)
        )
        self.dones[self.pos, :, :, :] = np.array(done).reshape(-1, 1, 1)
        self.dones_next[self.pos, :, :, :] = np.array(done_next).reshape(-1, 1, 1)
        self.edges_info[self.pos] = np.array(last_edges_info)
        self.next_edges_info[self.pos] = np.array(edges_info)

        for i in range(len(self.hidden_state_space)):
            self.hidden_state_pi[i][self.pos] = state_pi[i].cpu().numpy()
        for i in range(len(self.hidden_state_space)):
            self.hidden_state_pi_next[i][self.pos] = state_pi_next[i].cpu().numpy()

        if self.handle_timeout_termination:
            self.timeouts[self.pos, :, :, :] = np.array(
                [info.get("TimeLimit.truncated", False) for info in infos]
            ).reshape(-1, 1, 1)

        self.pos += 1
        if self.pos == self.buffer_size:
            self.full = True
            self.pos = 0

    def sample(self, batch_size: int, env: Optional[VecNormalize] = None) -> MaReplayBufferSamples:
        """
        Sample elements from the replay buffer.
        Custom sampling when using memory efficient variant,
        as we should not sample the element with index `self.pos`
        See https://github.com/DLR-RM/stable-baselines3/pull/28#issuecomment-637559274

        :param batch_size: Number of element to sample
        :param env: associated gym VecEnv
            to normalize the observations/rewards when sampling
        :return:
        """

        if not self.optimize_memory_usage:
            return super().sample(batch_size=batch_size, env=env)
            """
            if self.full:
                if len(self.hidden_state_space):
                    batch_inds = np.random.randint(
                        self.sequence_length, self.buffer_size, size=batch_size
                    )
                else:
                    batch_inds = np.random.randint(1, self.buffer_size, size=batch_size)
            else:
                if len(self.hidden_state_space):
                    batch_inds = np.random.randint(self.sequence_length, self.pos, size=batch_size)
                else:
                    batch_inds = np.random.randint(0, self.pos, size=batch_size)
            return self._get_samples(batch_inds, env=env)
            """
        # Do not sample the element with index `self.pos` as the transitions is invalid
        # (we use only one array to store `obs` and `next_obs`)
        if self.full:
            if len(self.hidden_state_space):
                batch_inds = (
                    np.random.randint(self.sequence_length, self.buffer_size, size=batch_size)
                    + self.pos
                ) % (self.buffer_size)
            else:
                batch_inds = (
                    np.random.randint(1, self.buffer_size, size=batch_size) + self.pos
                ) % (self.buffer_size)
        else:
            if len(self.hidden_state_space):
                batch_inds = np.random.randint(self.sequence_length, self.pos, size=batch_size)
            else:
                batch_inds = np.random.randint(0, self.pos, size=batch_size)
        return self._get_samples(batch_inds, env=env)

    def _get_samples(  # type: ignore[override]
        self,
        batch_inds: np.ndarray,
        env: Optional[VecNormalize] = None,
    ) -> MaReplayBufferSamples:
        # Sample randomly the env idx
        env_indices = np.random.randint(0, high=self.n_envs, size=(len(batch_inds),))

        if len(self.hidden_state_space):
            batch_size = len(batch_inds)
            batch_inds_s = np.linspace(
                batch_inds - self.sequence_length,
                batch_inds,
                self.sequence_length,
                dtype=np.int16,
            )
            batch_inds_s = batch_inds_s.transpose(1, 0).reshape(-1)
            env_indices_s = env_indices.repeat(self.sequence_length)
            # Normalize if needed and remove extra dimension (we are using only one env for now)
            obs_ = self._normalize_obs(
                {
                    key: obs[batch_inds_s, env_indices_s, :]
                    .reshape(batch_size, self.sequence_length, *obs.shape[2:])
                    .transpose(1, 0, 2)
                    .reshape(self.sequence_length, -1, *obs.shape[2:])
                    for key, obs in self.observations.items()
                },
                env,
            )
            next_obs_ = self._normalize_obs(
                {
                    key: obs[batch_inds_s, env_indices_s, :]
                    .reshape(batch_size, self.sequence_length, *obs.shape[2:])
                    .transpose(1, 0, 2)
                    .reshape(self.sequence_length, -1, *obs.shape[2:])
                    for key, obs in self.next_observations.items()
                },
                env,
            )
        else:
            # Normalize if needed and remove extra dimension (we are using only one env for now)
            obs_ = self._normalize_obs(
                {key: obs[batch_inds, env_indices, :] for key, obs in self.observations.items()},
                env,
            )
            next_obs_ = self._normalize_obs(
                {
                    key: obs[batch_inds, env_indices, :]
                    for key, obs in self.next_observations.items()
                },
                env,
            )

        assert isinstance(obs_, dict)
        assert isinstance(next_obs_, dict)
        # Convert to torch tensor
        observations = {key: self.to_torch(obs) for key, obs in obs_.items()}
        next_observations = {key: self.to_torch(obs) for key, obs in next_obs_.items()}

        if len(self.hidden_state_space):
            actions = (
                self.to_torch(self.actions[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.actions.shape[2:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.actions.shape[2:])
            )
            rewards = (
                self.to_torch(self.rewards[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.rewards.shape[2:])
                .transpose(1, 0)
                .reshape(-1, *self.rewards.shape[2:])
            )
            edges_info = (
                self.to_torch(self.edges_info[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.edges_info.shape[2:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.edges_info.shape[2:])
            )
            next_edges_info = (
                self.to_torch(self.next_edges_info[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.edges_info.shape[2:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.edges_info.shape[2:])
            )
            state_pi_next = tuple(
                self.to_torch(
                    self.hidden_state_pi_next[i][batch_inds - self.sequence_length, env_indices]
                ).reshape(-1, *self.hidden_state_pi_next[i].shape[2:])
                for i in range(len(self.hidden_state_space))
            )
            state_pi = tuple(
                self.to_torch(
                    self.hidden_state_pi[i][batch_inds - self.sequence_length, env_indices]
                ).reshape(-1, *self.hidden_state_pi[i].shape[2:])
                for i in range(len(self.hidden_state_space))
            )
            episode_starts = (
                self.to_torch(self.dones[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.dones.shape[2:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.dones.shape[2:])
            )
            episode_starts_next = (
                self.to_torch(self.dones_next[batch_inds_s, env_indices_s])
                .reshape(batch_size, self.sequence_length, *self.dones_next.shape[2:])
                .transpose(1, 0)
                .reshape(self.sequence_length, -1, *self.dones_next.shape[2:])
            )
            return MaReplayBufferSamples(
                observations=observations,
                next_observations=next_observations,
                actions=actions,
                edges_info=edges_info,
                rewards=rewards,
                next_edges_info=next_edges_info,
                state_pi_next=state_pi_next,
                state_pi=state_pi,
                dones=episode_starts,
                dones_next=episode_starts_next,
            )
        else:
            return MaReplayBufferSamples(
                observations=observations,
                actions=self.to_torch(self.actions[batch_inds, env_indices]),
                next_observations=next_observations,
                # Only use dones that are not due to timeouts
                # deactivated by default (timeouts is initialized as an array of False)
                dones=self.to_torch(
                    self.dones[batch_inds, env_indices]
                    * (1 - self.timeouts[batch_inds, env_indices])
                ),
                dones_next=self.to_torch(
                    self.dones_next[batch_inds, env_indices]
                    * (1 - self.timeouts[batch_inds, env_indices])
                ),
                rewards=self.to_torch(
                    self._normalize_reward(self.rewards[batch_inds, env_indices], env)
                ),
                edges_info=self.to_torch(self.edges_info[batch_inds, env_indices]),
                next_edges_info=self.to_torch(self.next_edges_info[batch_inds, env_indices]),
                state_pi_next=tuple(
                    self.to_torch(self.hidden_state_pi_next[i][batch_inds])
                    for i in range(len(self.hidden_state_space))
                ),
                state_pi=tuple(
                    self.to_torch(self.hidden_state_pi[i][batch_inds])
                    for i in range(len(self.hidden_state_space))
                ),
            )

    @staticmethod
    def _maybe_cast_dtype(dtype: np.typing.DTypeLike) -> np.typing.DTypeLike:
        """
        Cast `np.float64` action datatype to `np.float32`,
        keep the others dtype unchanged.
        See GH#1572 for more information.

        :param dtype: The original action space dtype
        :return: ``np.float32`` if the dtype was float64,
            the original dtype otherwise.
        """
        if dtype == np.float64:
            return np.float32
        return dtype
