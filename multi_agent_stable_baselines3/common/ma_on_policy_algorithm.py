import sys
import time
from typing import Any, Dict, List, Optional, Tuple, Type, TypeVar, Union

from collections import OrderedDict

import numpy as np
import torch as th
from gymnasium import spaces

from copy import deepcopy

from stable_baselines3.common.on_policy_algorithm import OnPolicyAlgorithm
from stable_baselines3.common.callbacks import BaseCallback
from multi_agent_stable_baselines3.common.policies import MultiAgentActorCriticPolicy
from stable_baselines3.common.type_aliases import GymEnv, MaybeCallback, Schedule
from multi_agent_stable_baselines3.common.type_aliases import PettingzooEnv
from stable_baselines3.common.utils import obs_as_tensor, safe_mean
from stable_baselines3.common.vec_env import VecEnv
from multi_agent_stable_baselines3.common.buffers import MaRolloutBuffer
from multi_agent_stable_baselines3.common.utils import (
    check_graph_nn,
    check_recurrent_nn,
)


SelfMaOnPolicyAlgorithm = TypeVar("SelfMaOnPolicyAlgorithm", bound="MaOnPolicyAlgorithm")


class MaOnPolicyAlgorithm(OnPolicyAlgorithm):
    """
    The base for a multi-agent On-Policy algorithms (ex: A2C/PPO).

    :param policy: The policy model to use (MlpPolicy, CnnPolicy, ...)
    :param env: The environment to learn from (if registered in Gym, can be str)
    :param learning_rate: The learning rate, it can be a function
        of the current progress remaining (from 1 to 0)
    :param n_steps: The number of steps to run for each environment per update
        (i.e. batch size is n_steps * n_env where n_env is number of environment copies running in parallel)
    :param gamma: Discount factor
    :param gae_lambda: Factor for trade-off of bias vs variance for Generalized Advantage Estimator.
        Equivalent to classic advantage when set to 1.
    :param ent_coef: Entropy coefficient for the loss calculation
    :param vf_coef: Value function coefficient for the loss calculation
    :param max_grad_norm: The maximum value for the gradient clipping
    :param use_sde: Whether to use generalized State Dependent Exploration (gSDE)
        instead of action noise exploration (default: False)
    :param sde_sample_freq: Sample a new noise matrix every n steps when using gSDE
        Default: -1 (only sample at the beginning of the rollout)
    :param rollout_buffer_class: Rollout buffer class to use. If ``None``, it will be automatically selected.
    :param rollout_buffer_kwargs: Keyword arguments to pass to the rollout buffer on creation.
    :param stats_window_size: Window size for the rollout logging, specifying the number of episodes to average
        the reported success rate, mean episode length, and mean reward over
    :param tensorboard_log: the log location for tensorboard (if None, no logging)
    :param monitor_wrapper: When creating an environment, whether to wrap it
        or not in a Monitor wrapper.
    :param policy_kwargs: additional arguments to be passed to the policy on creation
    :param verbose: Verbosity level: 0 for no output, 1 for info messages (such as device or wrappers used), 2 for
        debug messages
    :param seed: Seed for the pseudo random generators
    :param device: Device (cpu, cuda, ...) on which the code should be run.
        Setting it to auto, the code will be run on the GPU if possible.
    :param _init_setup_model: Whether or not to build the network at the creation of the instance
    :param supported_action_spaces: The action spaces supported by the algorithm.
    """

    rollout_buffer: MaRolloutBuffer
    policy: MultiAgentActorCriticPolicy

    def __init__(
        self,
        policy: Union[str, Type[MultiAgentActorCriticPolicy]],
        env: Union[PettingzooEnv, str],
        learning_rate: Union[float, Schedule],
        centralized_critics: bool,
        share_parameters_actor: bool,
        share_parameters_critic: bool,
        n_steps: int,
        gamma: float,
        gae_lambda: float,
        ent_coef: float,
        vf_coef: float,
        max_grad_norm: float,
        use_sde: bool,
        sde_sample_freq: int,
        rollout_buffer_class: Optional[Type[MaRolloutBuffer]] = MaRolloutBuffer,
        rollout_buffer_kwargs: Optional[Dict[str, Any]] = None,
        stats_window_size: int = 100,
        tensorboard_log: Optional[str] = None,
        monitor_wrapper: bool = True,
        policy_kwargs: Optional[Dict[str, Any]] = None,
        verbose: int = 0,
        seed: Optional[int] = None,
        device: Union[th.device, str] = "auto",
        _init_setup_model: bool = True,
        supported_action_spaces: Optional[Tuple[Type[spaces.Space], ...]] = None,
    ):
        self.agents = env.agents
        self.centralized_critics = centralized_critics
        self.shared_parameters_actor = share_parameters_actor
        self.shared_parameters_critic = share_parameters_critic
        policy_kwargs["centralized_critics"] = centralized_critics
        policy_kwargs["share_parameters_actor"] = share_parameters_actor
        policy_kwargs["share_parameters_critic"] = share_parameters_critic
        try:
            self.net_model = policy_kwargs["net_model"]
        except:
            raise ValueError(
                "net_model must be specified in policy_kwargs if net_arch is not specified"
            )

        super().__init__(
            policy=policy,
            env=env,
            learning_rate=learning_rate,
            policy_kwargs=policy_kwargs,
            n_steps=n_steps,
            gamma=gamma,
            gae_lambda=gae_lambda,
            ent_coef=ent_coef,
            vf_coef=vf_coef,
            max_grad_norm=max_grad_norm,
            verbose=verbose,
            rollout_buffer_class=rollout_buffer_class,
            rollout_buffer_kwargs=rollout_buffer_kwargs,
            device=device,
            use_sde=use_sde,
            sde_sample_freq=sde_sample_freq,
            monitor_wrapper=monitor_wrapper,
            seed=seed,
            stats_window_size=stats_window_size,
            tensorboard_log=tensorboard_log,
            _init_setup_model=_init_setup_model,
            supported_action_spaces=supported_action_spaces,
        )

    def reset_agent_num(self, agents: list) -> None:
        self.agents = agents
        self.policy.reset_agents(agents)

    def _setup_model(self) -> None:
        self._setup_lr_schedule()
        self.set_random_seed(self.seed)

        self.policy = self.policy_class(  # type: ignore[assignment]
            self.observation_space,
            self.action_space,
            self.lr_schedule,
            use_sde=self.use_sde,
            **self.policy_kwargs,
        )
        self.policy = self.policy.to(self.device)

        if self.rollout_buffer_class is None:
            self.rollout_buffer_class = MaRolloutBuffer

        if check_recurrent_nn(self.net_model):
            self.rollout_buffer_kwargs["hidden_state_space"] = tuple(
                [
                    self.policy.mlp_extractor.recurrent_pi_layers,
                    self.policy.mlp_extractor.recurrent_vf_layers,
                ]
            )

        self.rollout_buffer = self.rollout_buffer_class(
            self.n_steps,
            self.observation_space,  # type: ignore[arg-type]
            self.action_space,
            device=self.device,
            gamma=self.gamma,
            gae_lambda=self.gae_lambda,
            n_envs=self.n_envs,
            **self.rollout_buffer_kwargs,
        )

        self._last_states = None

    def evaluate_action_value(
        self,
        env: VecEnv,
        obs: Dict[str, np.ndarray],
        states: Tuple[th.Tensor, th.Tensor],
        episode_starts: th.Tensor,
        infos: tuple,
    ) -> Tuple[np.ndarray, np.ndarray]:

        with th.no_grad():
            if check_graph_nn(self.net_model):
                # Graph NNs need to add position information to form the graph
                if any([info.get("pos") is None for info in infos]):
                    raise ValueError(
                        "Graph Neural Networks need to receive the position information in the info dict. "
                    )
                pos = []
                for info in infos:
                    pos = pos + [info["pos"]]

                pos = np.stack(pos, axis=0)

            if check_recurrent_nn(self.net_model):
                if not any(states):
                    states = self.policy.get_initial_states(env.num_envs)
                    states = tuple(
                        [
                            tuple(th.tensor(state, device=self.device) for state in states[0]),
                            tuple(th.tensor(state, device=self.device) for state in states[1]),
                        ]
                    )
            if check_graph_nn(self.net_model):
                obs_tensor = obs_as_tensor(obs, self.device)
                try:
                    obs_tensor["pos"] = th.from_numpy(pos).to(self.device)
                except:
                    obs_tensor["pos"] = th.from_numpy(
                        np.zeros((env.num_envs, len(obs_tensor.keys()), 2))
                    ).to(self.device)
            else:
                obs_tensor = obs_as_tensor(self._last_obs, self.device)
            actions, values, _, states = self.policy(
                obs_tensor,
                states,
                th.tensor(episode_starts, device=self.device).float(),
                deterministic=True,
            )
        actions = actions.cpu().numpy()

        if isinstance(self.action_space[list(self.action_space.keys())[0]], spaces.Box):
            if self.policy.squash_output:
                # Rescale to proper domain when using squashing
                actions = self.unscale_action(actions)  # type: ignore[assignment, arg-type]
            else:
                # Actions could be on arbitrary scale, so clip the actions to avoid
                # out of bound error (e.g. if sampling from a Gaussian distribution)
                actions = np.clip(actions, self.policy.action_space.low, self.policy.action_space.high)  # type: ignore[assignment, arg-type]

        actions = actions.reshape(-1, len(self.agents), *self.policy.action_space.shape)
        actions = {agent: actions[:, i, :] for i, agent in enumerate(self.agents)}

        return actions, values, states

    def collect_rollouts(
        self,
        env: VecEnv,
        callback: BaseCallback,
        rollout_buffer: MaRolloutBuffer,
        n_rollout_steps: int,
    ) -> bool:
        """
        Collect experiences using the current policy and fill a ``RolloutBuffer``.
        The term rollout here refers to the model-free notion and should not
        be used with the concept of rollout used in model-based RL or planning.

        :param env: The training environment
        :param callback: Callback that will be called at each step
            (and at the beginning and end of the rollout)
        :param rollout_buffer: Buffer to fill with rollouts
        :param n_rollout_steps: Number of experiences to collect per environment
        :return: True if function returned with at least `n_rollout_steps`
            collected, False if callback terminated rollout prematurely.
        """
        assert self._last_obs is not None, "No previous observation was provided"
        # Switch to eval mode (this affects batch norm / dropout)
        self.policy.set_training_mode(False)

        n_steps = 0
        rollout_buffer.reset()
        # Sample new weights for the state dependent exploration
        if self.use_sde:
            self.policy.reset_noise(env.num_envs)

        callback.on_rollout_start()

        if isinstance(self._last_obs, Tuple):
            infos = self._last_obs[1]
            self._last_obs = self._last_obs[0]
            if check_graph_nn(self.net_model):
                # Graph NNs need to add position information to form the graph
                if any([info.get("pos") is None for info in infos]):
                    raise ValueError(
                        "Graph Neural Networks need to receive the position information in the info dict. "
                    )
                pos = []
                for info in infos:
                    pos = pos + [info["pos"]]

                pos = np.stack(pos, axis=0)

        if check_recurrent_nn(self.net_model):
            if not self._last_states:
                self._last_states = self.policy.get_initial_states(env.num_envs)
                self._last_states = tuple(
                    [
                        tuple(
                            th.tensor(state, device=self.device) for state in self._last_states[0]
                        ),
                        tuple(
                            th.tensor(state, device=self.device) for state in self._last_states[1]
                        ),
                    ]
                )

        while n_steps < n_rollout_steps:
            if self.use_sde and self.sde_sample_freq > 0 and n_steps % self.sde_sample_freq == 0:
                # Sample a new noise matrix
                self.policy.reset_noise(env.num_envs)

            with th.no_grad():
                # Convert to pytorch tensor or to TensorDict
                if check_graph_nn(self.net_model):
                    obs_tensor = obs_as_tensor(self._last_obs, self.device)
                    try:
                        obs_tensor["pos"] = th.from_numpy(pos).to(self.device)
                    except:
                        obs_tensor["pos"] = th.from_numpy(
                            np.zeros((env.num_envs, len(obs_tensor.keys()), 2))
                        ).to(self.device)
                else:
                    obs_tensor = obs_as_tensor(self._last_obs, self.device)
                actions, values, log_probs, states = self.policy(
                    obs_tensor,
                    self._last_states,
                    th.tensor(self._last_episode_starts, device=self.device).float(),
                )
            # state is wrong
            actions = actions.cpu().numpy()

            # Rescale and perform action
            actions_dict = OrderedDict()
            for i, key in enumerate(env.agents):
                actions_dict[key] = actions[:, i, :]
            clipped_actions = deepcopy(actions_dict)

            for key in env.agents:
                if isinstance(self.action_space[key], spaces.Box):
                    if self.policy.squash_output:
                        # Unscale the actions to match env bounds
                        # if they were previously squashed (scaled in [-1, 1])
                        clipped_actions[key] = self.policy.unscale_action(clipped_actions[key])
                    else:
                        # Otherwise, clip the actions to avoid out of bound error
                        # as we are sampling from an unbounded Gaussian distribution
                        clipped_actions[key] = np.clip(
                            actions_dict[key],
                            self.action_space[key].low,
                            self.action_space[key].high,
                        )

            new_obs, rewards, dones, infos = env.step(clipped_actions)

            if check_graph_nn(self.net_model):
                # Graph NNs need to add position information to form the graph
                if infos is None or not infos or any([info.get("pos") is None for info in infos]):
                    raise ValueError(
                        "Graph Neural Networks need to receive the position information in the info dict. "
                    )
                pos_new = []
                for info in infos:
                    pos_new = pos_new + [info["pos"]]

                pos_new = np.stack(pos_new, axis=0)

            self.num_timesteps += env.num_envs

            # Give access to local variables
            callback.update_locals(locals())
            if not callback.on_step():
                return False

            self._update_info_buffer(infos, dones)
            n_steps += 1

            for key in env.agents:
                if isinstance(self.action_space[key], spaces.Discrete):
                    # Reshape in case of discrete action
                    actions_dict[key] = actions_dict[key].reshape(-1, 1)

            # Handle timeout by bootstrapping with value function
            # see GitHub issue #633
            for idx, done in enumerate(dones):
                if (
                    done
                    and infos[idx].get("terminal_observation") is not None
                    and infos[idx].get("TimeLimit.truncated", False)
                ):
                    terminal_obs = infos[idx]["terminal_observation"]
                    if check_graph_nn(self.net_model):
                        terminal_obs = obs_as_tensor(terminal_obs, self.device)
                        terminal_obs["pos"] = th.from_numpy(pos_new).to(self.device)
                    else:
                        terminal_obs = obs_as_tensor(new_obs, self.device)
                    with th.no_grad():
                        terminal_value, _ = self.policy.predict_values(
                            terminal_obs,
                            states,
                            th.tensor(dones, device=self.device).float(),
                        )  # type: ignore[arg-type]
                    for i, key in enumerate(env.agents):
                        rewards[key][idx] += self.gamma * terminal_value[i, :]

            rollout_buffer.add(
                self._last_obs,  # type: ignore[arg-type]
                actions,
                deepcopy(rewards),
                self._last_episode_starts,  # type: ignore[arg-type]
                values,
                log_probs,
                edges_info=pos if check_graph_nn(self.net_model) else None,
                state_pi=(self._last_states[0] if check_recurrent_nn(self.net_model) else None),
                state_vf=(self._last_states[1] if check_recurrent_nn(self.net_model) else None),
            )
            self._last_obs = new_obs  # type: ignore[assignment]
            self._last_episode_starts = dones
            if check_graph_nn(self.net_model):
                pos = pos_new
            self._last_states = states

        with th.no_grad():
            # Compute value for the last timestep
            if check_graph_nn(self.net_model):
                obs_tensor = obs_as_tensor(new_obs, self.device)
                obs_tensor["pos"] = th.from_numpy(pos).to(self.device)
            else:
                obs_tensor = obs_as_tensor(new_obs, self.device)
            values, _ = self.policy.predict_values(obs_tensor, self._last_states, th.tensor(self._last_episode_starts, device=self.device).float())  # type: ignore[arg-type]
            values = values.reshape(-1, len(env.agents), 1)

        rollout_buffer.compute_returns_and_advantage(last_values=values, dones=dones)

        self._last_obs = (new_obs, infos) if check_graph_nn(self.net_model) else new_obs

        callback.update_locals(locals())

        callback.on_rollout_end()

        return True

    def predict(
        self,
        observation: Union[np.ndarray, Dict[str, np.ndarray]],
        state: Optional[Tuple[np.ndarray, ...]] = None,
        episode_start: Optional[np.ndarray] = None,
        deterministic: bool = False,
        infos: Optional[List[Dict[str, Any]]] = None,
    ) -> Tuple[np.ndarray, Optional[Tuple[np.ndarray, ...]]]:
        """
        Get the policy action from an observation (and optional hidden state).
        Includes sugar-coating to handle different observations (e.g. normalizing images).

        :param observation: the input observation
        :param state: The last hidden states (can be None, used in recurrent policies)
        :param episode_start: The last masks (can be None, used in recurrent policies)
            this correspond to beginning of episodes,
            where the hidden states of the RNN must be reset.
        :param deterministic: Whether or not to return deterministic actions.
        :param infos: infos dict list.
        :return: the model's action and the next hidden state
            (used in recurrent policies)
        """
        return self.policy.predict(observation, state, episode_start, deterministic, infos)

    def _dump_logs(self, iteration: int) -> None:
        """
        Write log.

        :param iteration: Current logging iteration
        """
        assert self.ep_info_buffer is not None
        assert self.ep_success_buffer is not None

        time_elapsed = max((time.time_ns() - self.start_time) / 1e9, sys.float_info.epsilon)
        fps = int((self.num_timesteps - self._num_timesteps_at_start) / time_elapsed)
        self.logger.record("time/iterations", iteration, exclude="tensorboard")
        if len(self.ep_info_buffer) > 0 and len(self.ep_info_buffer[0]) > 0:
            self.logger.record(
                "rollout/ep_rew_mean",
                safe_mean([ep_info["r"] for ep_info in self.ep_info_buffer]),
            )
            for key in self.agents:
                self.logger.record(
                    "rollout/ep_rew_mean/" + str(key),
                    safe_mean([ep_info["ra"][key] for ep_info in self.ep_info_buffer]),
                )
            self.logger.record(
                "rollout/ep_len_mean",
                safe_mean([ep_info["l"] for ep_info in self.ep_info_buffer]),
            )
        self.logger.record("time/fps", fps)
        self.logger.record("time/time_elapsed", int(time_elapsed), exclude="tensorboard")
        self.logger.record("time/total_timesteps", self.num_timesteps, exclude="tensorboard")
        if len(self.ep_success_buffer) > 0:
            self.logger.record("rollout/success_rate", safe_mean(self.ep_success_buffer))
        self.logger.dump(step=self.num_timesteps)

    def learn(
        self: SelfMaOnPolicyAlgorithm,
        total_timesteps: int,
        callback: MaybeCallback = None,
        log_interval: int = 1,
        tb_log_name: str = "OnPolicyAlgorithm",
        reset_num_timesteps: bool = True,
        progress_bar: bool = False,
    ) -> SelfMaOnPolicyAlgorithm:
        iteration = 0

        total_timesteps, callback = self._setup_learn(
            total_timesteps,
            callback,
            reset_num_timesteps,
            tb_log_name,
            progress_bar,
        )

        callback.on_training_start(locals(), globals())

        assert self.env is not None

        while self.num_timesteps < total_timesteps:
            continue_training = self.collect_rollouts(
                self.env, callback, self.rollout_buffer, n_rollout_steps=self.n_steps
            )

            if not continue_training:
                break

            iteration += 1
            self._update_current_progress_remaining(self.num_timesteps, total_timesteps)

            # Display training infos
            if log_interval is not None and iteration % log_interval == 0:
                assert self.ep_info_buffer is not None
                self._dump_logs(iteration)

            self.train()

        callback.on_training_end()

        return self

    def _get_torch_save_params(self) -> Tuple[List[str], List[str]]:
        state_dicts = ["policy", "policy.optimizer"]

        return state_dicts, []
