import numpy as np

from multi_agent_stable_baselines3 import MAPPO, IPPO, ISAC, MASAC
from stable_baselines3.common.vec_env import VecVideoRecorder
from stable_baselines3.common.utils import set_random_seed


# from stable_baselines3.common.evaluation import evaluate_policy

from multi_agent_stable_baselines3.common.evaluation import evaluate_policy
from multi_agent_stable_baselines3.common.env_util import make_ma_vec_env

from multi_agent_stable_baselines3.common.callbacks import EvalCallback
from multi_agent_stable_baselines3.common.policies import MultiAgentActorCriticPolicy
from multi_agent_stable_baselines3.sac.policies import SACPolicy
import coverage_environment.coverage_environment as ce
import torch as th

from utils import MRLConfig, TrialEvalCallback

import os
import random
import optuna
from optuna.pruners import MedianPruner
from optuna.samplers import TPESampler


def linear_schedule(initial_value, final_value):
    def fun(progress_remaining: float):

        return max(progress_remaining * initial_value, final_value)

    return fun


seed = 0

random.seed(seed)
os.environ["PYTHONHASHSEED"] = str(seed)
np.random.seed(seed)
th.manual_seed(seed)
th.cuda.manual_seed(seed)

config = MRLConfig.from_yaml("config.yaml")

eval_env = ce.SimpleCEEnv(na=config.Na, destinations=config.dest, max_cycles=250, resources=2)
eval_env.reset()
vec_env = make_ma_vec_env(
    ce.SimpleCEEnv,
    n_envs=4,
    env_kwargs={"na": config.Na, "destinations": config.dest, "max_cycles": 250, "resources": 2},
)


def objective(trial: optuna.Trial) -> float:
    config = MRLConfig.sample_parameters(yaml_path="config.yaml", trial=trial)

    if config.algorithm == MAPPO or config.algorithm == IPPO:

        algorithm_class = config.algorithm
        model = algorithm_class(
            MultiAgentActorCriticPolicy,
            vec_env,
            verbose=1,
            n_steps=config.n_steps,
            batch_size=config.batch_size,
            n_epochs=config.n_epochs,
            clip_range=config.clip_range,
            clip_range_vf=config.clip_range,
            ent_coef=config.ent_coef,
            gamma=config.gamma,
            gae_lambda=config.gae_lambda,
            learning_rate=config.learning_rate,  # linear_schedule(config.learning_rate, 1e-6),
            sde_sample_freq=4,
            vf_coef=0.5,
            share_parameters_actor=config.share_parameters_actor,
            share_parameters_critic=config.share_parameters_critic,
            policy_kwargs=config.policy_kwargs,
            use_sde=False,
            tensorboard_log="./resources_assignment_logs/"
            + str(config.Na)
            + "_"
            + str(config.dest),
        )
    elif config.algorithm == ISAC or config.algorithm == MASAC:
        algorithm_class = config.algorithm
        model = algorithm_class(
            SACPolicy,
            vec_env,
            verbose=1,
            ent_coef=config.ent_coef,
            batch_size=config.batch_size,
            gamma=config.gamma,
            learning_rate=linear_schedule(config.learning_rate, 1e-6),
            gradient_steps=config.n_epochs,
            buffer_size=config.buffer_size,
            share_parameters_actor=config.share_parameters_actor,
            share_parameters_critic=config.share_parameters_critic,
            policy_kwargs=config.policy_kwargs,
            tensorboard_log="./resources_assignment_logs/"
            + str(config.Na)
            + "_"
            + str(config.dest),
        )

    # Create the callback that will periodically evaluate and report the performance.
    eval_callback = TrialEvalCallback(
        eval_env,
        trial,
        best_model_save_path="./logs/" + str(config.Na) + "_" + str(config.dest),
        log_path="./logs/" + str(config.Na) + "_" + str(config.dest),
        eval_freq=10000,
        deterministic=True,
        render=False,
    )

    nan_encountered = False
    try:
        model.learn(config.n_timestamps, progress_bar=True, callback=eval_callback)
    except AssertionError as e:
        # Sometimes, random hyperparams can generate NaN.
        print(e)
        nan_encountered = True
    finally:
        # Free memory.
        model.env.close()
        eval_env.close()

    # Tell the optimizer that the trial failed.
    if nan_encountered:
        return float("nan")

    if eval_callback.is_pruned:
        raise optuna.exceptions.TrialPruned()

    return eval_callback.last_mean_reward


if __name__ == "__main__":

    # Set pytorch num threads to 1 for faster training.
    th.set_num_threads(1)

    sampler = TPESampler(n_startup_trials=5)

    # Do not prune before 1/3 of the max budget is used.
    pruner = MedianPruner(n_startup_trials=5, n_warmup_steps=20, interval_steps=5)

    study = optuna.create_study(sampler=sampler, pruner=pruner, direction="maximize")
    try:
        study.optimize(objective, n_trials=60)
    except KeyboardInterrupt:
        pass

    print("Number of finished trials: ", len(study.trials))

    print("Best trial:")
    trial = study.best_trial

    print("  Value: ", trial.value)

    print("  Params: ")
    for key, value in trial.params.items():
        print("    {}: {}".format(key, value))

    print("  User attrs:")
    for key, value in trial.user_attrs.items():
        print("    {}: {}".format(key, value))
