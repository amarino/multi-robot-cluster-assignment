from dataclasses import dataclass
import yaml
from pathlib import Path
import sys
from torch import nn
import optuna

import pettingzoo

from coverage_environment.coverage_environment.models import *
from multi_agent_stable_baselines3 import (
    MAPPO,
    IPPO,
    ISAC,
    MASAC,
    VDN,
    MAMOPPO,
    IMOPPO,
    QMIX,
)
from multi_agent_stable_baselines3.common.torch_layers import *
from multi_agent_stable_baselines3.common.utils import check_graph_nn
from multi_agent_stable_baselines3.common.callbacks import EvalCallback
from stable_baselines3.common.logger import HParam

from coverage_environment.coverage_environment.multi_resources_ppo import *


@dataclass
class MRLConfig:
    # Network Architecture
    Na: int
    dest: int
    algorithm: str

    # Training Parameters
    learning_rate: float
    n_steps: int
    n_epochs: int
    gamma: float
    ent_coef: float
    batch_size: int
    clip_range: float
    n_timestamps: int
    gae_lambda: float
    buffer_size: int
    max_grad_norm: float
    train_freq: int
    resources: int

    # Model Architecture Parameters
    share_parameters_actor: bool
    share_parameters_critic: bool
    net_model: str

    policy_kwargs: dict

    @classmethod
    def from_yaml(cls, yaml_path: str | Path) -> "MRLConfig":
        """
        Create MRLConfig instance from YAML file.

        Args:
            yaml_path: Path to YAML configuration file

        Returns:
            MRLConfig object with parsed configuration

        Raises:
            FileNotFoundError: If config file doesn't exist
            ValueError: If required fields are missing or invalid
        """
        yaml_path = Path(yaml_path)

        if not yaml_path.exists():
            raise FileNotFoundError(f"Configuration file not found: {yaml_path}")

        with open(yaml_path) as f:
            config_dict = yaml.safe_load(f)

        net_model = getattr(
            sys.modules[__name__], config_dict.get("net_model", "GNNExtractor")
        )

        if check_graph_nn(net_model):
            edge_radius = float(config_dict.get("edge_radius", 0.9))
            edge_variable = str(config_dict.get("edge_variable", "pos"))
            net_model.edge_radius = edge_radius
            net_model.edge_variable = edge_variable

        pk = config_dict.get("policy_kwargs", {})
        pk["activation_fn"] = getattr(nn, pk["activation_fn"])
        pk["net_model"] = net_model
        pk["net_model_kwargs"] = (
            pk["net_model_kwargs"] if pk["net_model_kwargs"] else {}
        )
        return cls(
            Na=config_dict.get("Na", 2),
            dest=config_dict.get("dest", 2),
            algorithm=getattr(
                sys.modules[__name__], config_dict.get("algorithm", "IPPO")
            ),
            learning_rate=float(config_dict.get("learning_rate", 1e-3)),
            n_steps=int(config_dict.get("n_steps", 2048)),
            n_epochs=int(config_dict.get("n_epochs", 10)),
            gamma=float(config_dict.get("gamma", 0.9)),
            gae_lambda=float(config_dict.get("gae_lambda", 0.9)),
            n_timestamps=float(config_dict.get("n_timesteps", 1e6)),
            clip_range=float(config_dict.get("clip_range", 0.2)),
            ent_coef=config_dict.get("ent_coef", 0.01),
            max_grad_norm=config_dict.get("max_grad_norm", 0.5),
            batch_size=int(config_dict.get("batch_size", 64)),
            buffer_size=int(config_dict.get("buffer_size", 1_000_000)),
            share_parameters_actor=bool(
                config_dict.get("share_parameters_actor", True)
            ),
            share_parameters_critic=bool(
                config_dict.get("share_parameters_critic", True)
            ),
            train_freq=int(config_dict.get("train_freq", 1)),
            policy_kwargs=pk,
            resources=config_dict.get("resources", 1),
            net_model=net_model,
        )

    def validate(self) -> None:
        """
        Validate configuration values.

        Raises:
            ValueError: If any configuration values are invalid
        """
        # Validate network architecture parameters
        if self.Na <= 0:
            raise ValueError("Na must be positive")
        if self.dest <= 0:
            raise ValueError("dest must be positive")

        # Validate algorithm
        valid_algorithms = ["ISAC", "MASAC", "MAPPO", "IPPO"]  # Add more as needed
        if self.algorithm not in valid_algorithms:
            raise ValueError(f"Algorithm must be one of: {valid_algorithms}")

        # Validate training parameters
        if self.learning_rate <= 0:
            raise ValueError("Learning rate must be positive")
        if self.n_steps <= 0:
            raise ValueError("n_steps must be positive")
        if self.n_epochs <= 0:
            raise ValueError("n_epochs must be positive")
        if not 0 <= self.gamma <= 1:
            raise ValueError("gamma must be between 0 and 1")
        if self.ent_coef < 0:
            raise ValueError("ent_coef must be non-negative")
        if self.batch_size <= 0:
            raise ValueError("batch_size must be positive")

    def save(self, save_path: str | Path) -> None:
        """
        Save configuration to YAML file.

        Args:
            save_path: Path where to save the configuration
        """
        save_path = Path(save_path)

        config_dict = {
            "Na": self.Na,
            "dest": self.dest,
            "algorithm": self.algorithm,
            "learning_rate": self.learning_rate,
            "n_steps": self.n_steps,
            "n_epochs": self.n_epochs,
            "gamma": self.gamma,
            "ent_coef": self.ent_coef,
            "batch_size": self.batch_size,
            "share_parameters_actor": self.share_parameters_actor,
            "share_parameters_critic": self.share_parameters_critic,
        }

        with open(save_path, "w") as f:
            yaml.safe_dump(config_dict, f, default_flow_style=False)

    @classmethod
    def sample_parameters(
        cls, yaml_path: str | Path, trial: optuna.Trial
    ) -> "MRLConfig":

        yaml_path = Path(yaml_path)

        if not yaml_path.exists():
            raise FileNotFoundError(f"Configuration file not found: {yaml_path}")

        with open(yaml_path) as f:
            config_dict = yaml.safe_load(f)

        net_model = getattr(
            sys.modules[__name__], config_dict.get("net_model", "GNNExtractor")
        )

        if check_graph_nn(net_model):
            edge_radius = float(config_dict.get("edge_radius", 0.9))
            edge_variable = str(config_dict.get("edge_variable", "pos"))
            net_model.edge_radius = edge_radius
            net_model.edge_variable = edge_variable

        pk = config_dict.get("policy_kwargs", {})
        pk["activation_fn"] = getattr(nn, pk["activation_fn"])
        pk["net_model"] = net_model

        algorithm = config_dict.get("algorithm", "IPPO")
        if algorithm in [
            "MAPPO",
            "IPPO",
            "MultiResourcesIPPO",
            "MultiResourcesMAPPO",
            "MAMOPPO",
            "IMOPPO",
        ]:
            n_steps = trial.suggest_categorical("n_steps", [4048, 6048, 8048])
            buffer_size = config_dict.get("buffer_size", 1_000_000)
            ent_coef = trial.suggest_float("ent_coef", 1e-8, 0.1, log=True)
            clip_range = trial.suggest_float("clip_range", 0.05, 0.3, log=True)
            n_epochs = trial.suggest_int("n_epochs", 1, 10, log=True)
            gae_lambda = config_dict.get("gae_lambda", 0.2)
            train_freq = int(config_dict.get("train_freq", 1))
        elif algorithm == "ISAC" or algorithm == "MASAC" or algorithm == "VDN":
            n_steps = config_dict.get("n_steps", 4048)
            ent_coef = config_dict.get("ent_coef", 0.001)
            clip_range = config_dict.get("clip_range", 0.2)
            n_epochs = config_dict.get("n_epochs", 1)
            buffer_size = trial.suggest_categorical(
                "buffer_size", [50_000, 100_000, 500_000, 1_000_000]
            )
            gae_lambda = config_dict.get("gae_lambda", 0.2)
            train_freq = trial.suggest_int("train_freq", 1, 10)

        learning_rate = trial.suggest_float("learning_rate", 5e-5, 5e-4, log=True)
        max_grad_norm = trial.suggest_float("max_grad_norm", 0.3, 5.0, log=True)
        gamma = 1.0 - trial.suggest_float("gamma", 0.001, 0.15, log=True)
        batch_size = trial.suggest_categorical("batch_size", [32, 64])

        algorithm = getattr(sys.modules[__name__], algorithm)
        return cls(
            Na=config_dict.get("Na", 2),
            dest=config_dict.get("dest", 2),
            algorithm=algorithm,
            learning_rate=learning_rate,
            n_steps=n_steps,
            n_epochs=n_epochs,
            gamma=gamma,
            gae_lambda=gae_lambda,
            n_timestamps=float(config_dict.get("n_timesteps", 1e6)),
            clip_range=clip_range,
            ent_coef=ent_coef,
            batch_size=batch_size,
            buffer_size=buffer_size,
            max_grad_norm=max_grad_norm,
            share_parameters_actor=bool(
                config_dict.get("share_parameters_actor", True)
            ),
            share_parameters_critic=bool(
                config_dict.get("share_parameters_critic", True)
            ),
            train_freq=train_freq,
            policy_kwargs=pk,
            resources=config_dict.get("resources", 1),
            net_model=net_model,
        )


class TrialEvalCallback(EvalCallback):
    """Callback used for evaluating and reporting a trial."""

    def __init__(
        self,
        eval_env: pettingzoo.ParallelEnv,
        trial: optuna.Trial,
        n_eval_episodes: int = 5,
        eval_freq: int = 10000,
        deterministic: bool = True,
        verbose: int = 0,
        best_model_save_path: str = "./logs/",
        log_path: str = "./logs/",
        render: bool = False,
    ):
        super().__init__(
            eval_env=eval_env,
            n_eval_episodes=n_eval_episodes,
            eval_freq=eval_freq,
            deterministic=deterministic,
            verbose=verbose,
            best_model_save_path=best_model_save_path,
            log_path=log_path,
            render=render,
        )
        self.trial = trial
        self.eval_idx = 0
        self.is_pruned = False

    def _on_training_start(self) -> None:

        hparam_dict = {
            "algorithm": self.model.__class__.__name__,
            "learning rate": self.trial.params["learning_rate"],
            "gamma": self.trial.params["gamma"],
            "batch size": self.trial.params["batch_size"],
            "max_grad_norm": self.trial.params["max_grad_norm"],
        }

        if self.trial.params.get("ent_coef", None):
            hparam_dict.update({"ent_coef": self.trial.params["ent_coef"]})
        if self.trial.params.get("clip_range", None):
            hparam_dict.update({"clip_range": self.trial.params["clip_range"]})
        if self.trial.params.get("n_epochs", None):
            hparam_dict.update({"n_epochs": self.trial.params["n_epochs"]})
        if self.trial.params.get("n_steps", None):
            hparam_dict.update({"n_steps": self.trial.params["n_steps"]})
        if self.trial.params.get("buffer_size", None):
            hparam_dict.update({"buffer_size": self.trial.params["buffer_size"]})
        if self.trial.params.get("gae_lambda", None):
            hparam_dict.update({"gae_lambda": self.trial.params["gae_lambda"]})
        if self.trial.params.get("train_freq", None):
            hparam_dict.update({"train_freq": self.trial.params["train_freq"]})

        # define the metrics that will appear in the `HPARAMS` Tensorboard tab by referencing their tag
        # Tensorbaord will find & display metrics from the `SCALARS` tab
        metric_dict = {
            "rollout/ep_len_mean": 0,
            "train/value_loss": 0.0,
            "eval/mean_reward": 0.0,
            "train/loss": 0.0,
            "train/entropy_loss": 0.0,
        }
        self.logger.record(
            "hparams",
            HParam(hparam_dict, metric_dict),
            exclude=("stdout", "log", "json", "csv"),
        )

    def _on_step(self) -> bool:
        if self.eval_freq > 0 and self.n_calls % self.eval_freq == 0:
            super()._on_step()
            self.eval_idx += 1
            self.trial.report(self.last_mean_reward, self.eval_idx)
            # Prune trial if need.
            if self.trial.should_prune():
                self.is_pruned = True
                return False
        return True
