import numpy as np

from multi_agent_stable_baselines3 import (
    MAPPO,
    IPPO,
    ISAC,
    MASAC,
    VDN,
    MAMOPPO,
    IMOPPO,
    QMIX,
)
from stable_baselines3.common.vec_env import VecVideoRecorder
from stable_baselines3.common.utils import set_random_seed

from matplotlib import pyplot as plt

# from stable_baselines3.common.evaluation import evaluate_policy

from multi_agent_stable_baselines3.common.evaluation import evaluate_policy
from multi_agent_stable_baselines3.common.env_util import make_ma_vec_env
from multi_agent_stable_baselines3.common.vec_env.vec_normalize import VecNormalize

from multi_agent_stable_baselines3.common.callbacks import EvalCallback
from multi_agent_stable_baselines3.common.policies import MultiAgentActorCriticPolicy
from multi_agent_stable_baselines3.sac.policies import SACPolicy
from multi_agent_stable_baselines3.vdn.policies import VDNPolicy
from multi_agent_stable_baselines3.qmix.policies import QMIXPolicy
import coverage_environment.coverage_environment as ce
from coverage_environment.coverage_environment.multi_resources_ppo import (
    MultiResourcesMAPPO,
    MultiResourcesIPPO,
)
import torch as th
from multi_agent_stable_baselines3.common.utils import (
    check_graph_nn,
    check_recurrent_nn,
)

from utils import MRLConfig

import os
import random


def linear_schedule(initial_value, final_value):
    def fun(progress_remaining: float):

        return max(progress_remaining * initial_value, final_value)

    return fun


seed = 7

random.seed(seed)
os.environ["PYTHONHASHSEED"] = str(seed)
np.random.seed(seed)
th.manual_seed(seed)
th.cuda.manual_seed(seed)

config = MRLConfig.from_yaml("config.yaml")

eval_env = ce.SimpleCEEnv(
    na=config.Na, destinations=config.dest, resources=config.resources, max_cycles=250
)
eval_env.reset()
vec_env = make_ma_vec_env(
    ce.SimpleCEEnv,
    n_envs=4,
    env_kwargs={
        "na": config.Na,
        "destinations": config.dest,
        "resources": config.resources,
        "max_cycles": 250,
    },
)


vec_env = VecNormalize(
    vec_env,
    norm_obs=False,
    norm_reward=True,
    clip_reward=5.0,
    gamma=config.gamma,
)


if config.algorithm in [
    MAPPO,
    IPPO,
    MultiResourcesIPPO,
    MultiResourcesMAPPO,
    MAMOPPO,
    IMOPPO,
]:

    algorithm_class = config.algorithm
    model = algorithm_class(
        MultiAgentActorCriticPolicy,
        vec_env,
        verbose=1,
        n_steps=config.n_steps,
        batch_size=config.batch_size,
        n_epochs=config.n_epochs,
        clip_range=config.clip_range,
        # clip_range_vf=config.clip_range,
        ent_coef=config.ent_coef,
        gamma=config.gamma,
        gae_lambda=config.gae_lambda,
        learning_rate=linear_schedule(config.learning_rate, 5e-7),
        sde_sample_freq=4,
        vf_coef=0.5,
        max_grad_norm=config.max_grad_norm,
        share_parameters_actor=config.share_parameters_actor,
        share_parameters_critic=config.share_parameters_critic,
        policy_kwargs=config.policy_kwargs,
        use_sde=False,
        tensorboard_log="./resources_assignment_logs/"
        + str(config.Na)
        + "_"
        + str(config.dest),
    )
elif config.algorithm == ISAC or config.algorithm == MASAC:
    algorithm_class = config.algorithm
    model = algorithm_class(
        SACPolicy,
        vec_env,
        verbose=1,
        ent_coef=config.ent_coef,
        batch_size=config.batch_size,
        gamma=config.gamma,
        learning_rate=linear_schedule(config.learning_rate, 5e-7),
        gradient_steps=config.n_epochs,
        buffer_size=config.buffer_size,
        train_freq=config.train_freq,
        share_parameters_actor=config.share_parameters_actor,
        share_parameters_critic=config.share_parameters_critic,
        policy_kwargs=config.policy_kwargs,
        tensorboard_log="./resources_assignment_logs/"
        + str(config.Na)
        + "_"
        + str(config.dest),
    )
elif config.algorithm == VDN:
    algorithm_class = config.algorithm
    model = algorithm_class(
        VDNPolicy,
        vec_env,
        verbose=1,
        ent_coef=config.ent_coef,
        batch_size=config.batch_size,
        gamma=config.gamma,
        learning_rate=linear_schedule(config.learning_rate, 5e-7),
        gradient_steps=config.n_epochs,
        buffer_size=config.buffer_size,
        train_freq=config.train_freq,
        share_parameters_actor=config.share_parameters_actor,
        share_parameters_critic=config.share_parameters_critic,
        policy_kwargs=config.policy_kwargs,
        tensorboard_log="./resources_assignment_logs/"
        + str(config.Na)
        + "_"
        + str(config.dest),
    )
elif config.algorithm == QMIX:
    algorithm_class = config.algorithm
    model = algorithm_class(
        QMIXPolicy,
        vec_env,
        verbose=1,
        ent_coef=config.ent_coef,
        batch_size=config.batch_size,
        gamma=config.gamma,
        learning_rate=linear_schedule(config.learning_rate, 5e-7),
        gradient_steps=config.n_epochs,
        buffer_size=config.buffer_size,
        train_freq=config.train_freq,
        share_parameters_actor=config.share_parameters_actor,
        share_parameters_critic=config.share_parameters_critic,
        policy_kwargs=config.policy_kwargs,
        tensorboard_log="./resources_assignment_logs/"
        + str(config.Na)
        + "_"
        + str(config.dest),
    )

eval_callback = EvalCallback(
    eval_env=vec_env,
    best_model_save_path="./logs/"
    + config.net_model.__name__
    + "-"
    + config.algorithm.__name__,
    n_eval_episodes=10,
    log_path="./logs/",
    eval_freq=10000,
    deterministic=True,
    render=False,
)

# Random Agent, before training
# mean_reward, std_reward, mstd_agents = evaluate_policy(model, eval_env, n_eval_episodes=10)
# print(f"Mean reward: {mean_reward} +/- {std_reward:.2f}")
# for t in mstd_agents:
#    print(f"agent: {t[0]}, Mean reward: {t[1]} +/- {t[2]:.2f}")

# Multiprocessed RL Training
model.learn(config.n_timestamps, progress_bar=True, callback=eval_callback)
# model.set_parameters("logs/LGTCRaExtractor-MultiResourcesIPPO/best_model.zip")

# Random Agent, after training
# mean_reward, std_reward, mstd_agents = evaluate_policy(
#    model, eval_env, n_eval_episodes=10, deterministic=True
# )
# print(f"Mean reward: {mean_reward} +/- {std_reward:.2f}")
# for t in mstd_agents:
#    print(f"agent: {t[0]}, Mean reward: {t[1]} +/- {t[2]:.2f}")

config.Na = 5
vec_env = make_ma_vec_env(
    ce.SimpleCEEnv,
    n_envs=1,
    env_kwargs={
        "na": config.Na,
        "destinations": config.dest,
        "resources": config.resources,
        "max_cycles": 250,
    },
)
# vec_env = model.get_env()
obs = vec_env.reset()
info = {}
if isinstance(obs, tuple):
    info = obs[1]
    obs = obs[0]

vec_env = VecVideoRecorder(
    vec_env,
    "videos",
    record_video_trigger=lambda x: x % 1 == 0,
    video_length=250,
    name_prefix="resources_assigment_"
    + str(config.Na)
    + "_"
    + str(config.dest)
    + "_"
    + str(config.resources),
)
vec_env.start_video_recorder()
_states = (None, None)
episode_starts = np.ones((4,), dtype=bool)
rw = np.zeros((config.Na, 4))

save_state_value = []
save_state_action = []

model.reset_agent_num(list(obs.keys()))

with th.no_grad():
    for i in range(250):

        action, values, _states = model.evaluate_action_value(
            vec_env,
            obs,
            states=_states,
            episode_starts=episode_starts,
            infos=info,
        )

        obs, rewards, dones, info = vec_env.step(action)
        episode_starts = dones

        if i > 100:
            vec_env.env.envs[0].env.agents_level[1, 0] = 0

        if _states[1]:
            save_state_value.append(
                (
                    th.matmul(
                        _states[1][0][0],
                        th.ones(
                            (1, _states[1][0][0].shape[-1]), device=_states[1][0].device
                        ).mT,
                    )
                    / th.linalg.norm(_states[1][0][0], dim=-1, keepdim=True)
                ).reshape(-1)
            )

        if _states[0]:
            save_state_action.append(
                (
                    th.matmul(
                        _states[0][0][0],
                        th.ones(
                            (1, _states[0][0][0].shape[-1]), device=_states[0][0].device
                        ).mT,
                    )
                    / th.linalg.norm(_states[0][0][0], dim=-1, keepdim=True)
                ).reshape(-1)
            )

        rw += np.array(list(rewards.values()))
        if any(dones):
            print(rw)
        rw = (1 - dones.reshape(1, -1)) * rw
        vec_env.render("rgb_array")

    if _states[1]:
        save_state = th.stack(save_state_value).cpu().numpy()

        plt.plot(save_state)
        plt.grid(True)
        plt.savefig("test_value.png")

    if _states[0]:
        save_state = th.stack(save_state_action).cpu().numpy()

        plt.plot(save_state)
        plt.grid(True)
        plt.savefig("test_action.png")

    vec_env.close_video_recorder()
    vec_env.close()
